# Green Party Campaigns

This is software to be used by the Green Party of England and Wales when canvassing, and for logging casework.

## Installation
```
git clone https://gitlab.com/andach/green-campaigns
cd ./green-campaigns
composer install
php artisan serve --port=47336
php artisan migrate
```

Obviously use whatever port you prefer, or artisan defaults to port 80. The .env file should be set up with whatever your preferred database is (mySQL and MariaDB have been used throughout testing). 

The system will create a default user - admin@demo.com with a password of "password". You'll need to create councils and wards manually by editing the mySQL database. 

Your local PHP service will need to have the following extensions enabled:

* extension=curl
* extension=fileinfo
* extension=mbstring
* extension=mysqli
* extension=openssl
* extension=pdo_mysql
* extension=pdo_sqlite
* extension=sockets

## Docker
```
docker-compose up
docker-compose exec green-campaigns php artisan key:generate
docker-compose exec green-campaigns php artisan config:cache
docker-compose exec green-campaigns php artisan migrate --seed
```

Access the website through http://localhost:47336

Note that, on Windows, you might need to disable the "Use the WSL 2 based engine" option in the Docker settings, to improve I/O performance. You will also need to add your local folder to the Settings->Resources->File Sharing page in Docker settings if using Windows. 

Alternatively, install Ubuntu from the Microsoft Store, then type "wsl" at the command line to get into a ubuntu machine, and 

## Testing

```
./vendor/bin/phpunit
./vendor/bin/phpcs -sp --standard=phpcs.xml
./vendor/bin/phpmd app/Http/Controllers,app/Models,app/Traits text phpmd.xml
```
