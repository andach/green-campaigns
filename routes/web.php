<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes([
    'register' => false
]);

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function ()
{
    Route::get('/ajax/address-to-voters/{id}', 'App\Http\Controllers\AjaxController@addressToVoters');
    Route::get('/ajax/road-to-addresses/{id}', 'App\Http\Controllers\AjaxController@roadToAddresses');
    Route::get('/ajax/ward-to-roads/{id}', 'App\Http\Controllers\AjaxController@wardToRoads');

    Route::post('/activist/contact-delete', 'App\Http\Controllers\ActivistController@contactDelete')->name('activist.contact-delete');
    Route::post('/activist/upload', 'App\Http\Controllers\ActivistController@upload')->name('activist.upload');
    Route::get('/activist/{id}/export', 'App\Http\Controllers\ActivistController@export')->name('activist.export');
    Route::get('/activist/{id}', 'App\Http\Controllers\ActivistController@show')->name('activist.show');
    Route::post('/activist/{id}', 'App\Http\Controllers\ActivistController@showPost')->name('activist.show-post');
    Route::get('/activist', 'App\Http\Controllers\ActivistController@index')->name('activist.index');

    Route::get('/address/create', 'App\Http\Controllers\AddressController@create')->name('address.create');
    Route::post('/address/create', 'App\Http\Controllers\AddressController@createPost')->name('address.create-post');
    Route::get('/address/{id}/edit', 'App\Http\Controllers\AddressController@edit')->name('address.edit');
    Route::post('/address/{id}/edit', 'App\Http\Controllers\AddressController@editPost')->name('address.edit-post');

    Route::get('/canvass/address/{id}/absent-marked', 'App\Http\Controllers\CanvassController@addressAbsentMarked')->name('canvass.address-absent-marked');
    Route::get('/canvass/address/{id}', 'App\Http\Controllers\CanvassController@address')->name('canvass.address');
    Route::post('/canvass/address/{id}', 'App\Http\Controllers\CanvassController@addressPost')->name('canvass.address-post');
    Route::get('/canvass/print/{id}/absent-marked', 'App\Http\Controllers\CanvassController@printAbsentMarked')->name('canvass.print-absent-marked');
    Route::get('/canvass/print/{id}', 'App\Http\Controllers\CanvassController@print')->name('canvass.print');
    Route::get('/canvass/road/{id}/absent-marked', 'App\Http\Controllers\CanvassController@roadAbsentMarked')->name('canvass.road-absent-marked');
    Route::get('/canvass/road/{id}', 'App\Http\Controllers\CanvassController@road')->name('canvass.road');
    Route::post('/canvass/road/{id}', 'App\Http\Controllers\CanvassController@roadPost')->name('canvass.road-post');

    Route::get('/casework/create/{userID}', 'App\Http\Controllers\CaseworkController@create')->name('casework.create');
    Route::post('/casework/create/{userID}', 'App\Http\Controllers\CaseworkController@createPost')->name('casework.create-post');
    Route::get('/casework/{id}', 'App\Http\Controllers\CaseworkController@show')->name('casework.show');
    Route::post('/casework/{id}', 'App\Http\Controllers\CaseworkController@showPost')->name('casework.show-post');
    Route::get('/casework', 'App\Http\Controllers\CaseworkController@index')->name('casework.index');

    Route::get('/event/create', 'App\Http\Controllers\EventController@create')->name('event.create');
    Route::post('/event/create', 'App\Http\Controllers\EventController@createPost')->name('event.create-post');
    Route::get('/event/{id}/contact/{activistID}', 'App\Http\Controllers\EventController@contact')->name('event.contact');
    Route::post('/event/{id}/contact/{activistID}', 'App\Http\Controllers\EventController@contactPost')->name('event.contact-post');
    Route::post('/event/{id}/contact/{activistID}/not-coming', 'App\Http\Controllers\EventController@contactNotComing')->name('event.contact-not-coming');
    Route::get('/event/{id}/edit', 'App\Http\Controllers\EventController@edit')->name('event.edit');
    Route::post('/event/{id}/edit', 'App\Http\Controllers\EventController@editPost')->name('event.edit-post');
    Route::get('/event/{id}', 'App\Http\Controllers\EventController@show')->name('event.show');
    Route::get('/event', 'App\Http\Controllers\EventController@index')->name('event.index');

    Route::get('/road/{id}', 'App\Http\Controllers\RoadController@show')->name('road.show');

    Route::get('/survey/create', 'App\Http\Controllers\SurveyController@create')->name('survey.create');
    Route::post('/survey/create', 'App\Http\Controllers\SurveyController@createPost')->name('survey.create-post');
    Route::get('/survey/{id}/edit', 'App\Http\Controllers\SurveyController@edit')->name('survey.edit');
    Route::get('/survey/{id}/export', 'App\Http\Controllers\SurveyController@export')->name('survey.export');
    Route::post('/survey/{id}/edit', 'App\Http\Controllers\SurveyController@editPost')->name('survey.edit-post');
    Route::get('/survey/{id}/response', 'App\Http\Controllers\SurveyController@response')->name('survey.response');
    Route::post('/survey/{id}/response', 'App\Http\Controllers\SurveyController@responsePost')->name('survey.response-post');
    Route::get('/survey/{id}/responses', 'App\Http\Controllers\SurveyController@responses')->name('survey.responses');
    Route::get('/survey/{id}', 'App\Http\Controllers\SurveyController@show')->name('survey.show');
    Route::get('/survey', 'App\Http\Controllers\SurveyController@index')->name('survey.index');

    Route::get('/survey-response/{id}', 'App\Http\Controllers\SurveyResponseController@show')->name('survey-response.show');

    Route::get('/user/my-account', 'App\Http\Controllers\UserController@myAccount')->name('user.my-account');
    Route::post('/user/my-account', 'App\Http\Controllers\UserController@myAccountPost')->name('user.my-account-post');

    Route::get('/ward/{id}/export', 'App\Http\Controllers\WardController@export')->name('ward.export');
    Route::get('/ward/{id}', 'App\Http\Controllers\WardController@show')->name('ward.show');
    Route::get('/ward', 'App\Http\Controllers\WardController@index')->name('ward.index');

    Route::group(['middleware' => ['admin']], function ()
    {
        Route::get('/council', 'App\Http\Controllers\CouncilController@index')->name('council.index');
        Route::post('/council', 'App\Http\Controllers\CouncilController@indexPost')->name('council.index-post');

        Route::get('/upload/absent-register', 'App\Http\Controllers\UploadController@absentRegister')->name('upload.absent-register');
        Route::post('/upload/absent-register', 'App\Http\Controllers\UploadController@absentRegisterPost')->name('upload.absent-register-post');
        Route::get('/upload/electoral-register', 'App\Http\Controllers\UploadController@electoralRegister')->name('upload.electoral-register');
        Route::post('/upload/electoral-register', 'App\Http\Controllers\UploadController@electoralRegisterPost')->name('upload.electoral-register-post');
        Route::get('/upload/marked-register', 'App\Http\Controllers\UploadController@markedRegister')->name('upload.marked-register');
        Route::post('/upload/marked-register', 'App\Http\Controllers\UploadController@markedRegisterPost')->name('upload.marked-register-post');
        Route::get('/upload', 'App\Http\Controllers\UploadController@index')->name('upload.index');

        Route::get('/user/create', 'App\Http\Controllers\UserController@create')->name('user.create');
        Route::post('/user/create', 'App\Http\Controllers\UserController@createPost')->name('user.create-post');
        Route::get('/user/{id}/edit', 'App\Http\Controllers\UserController@edit')->name('user.edit');
        Route::post('/user/{id}/edit', 'App\Http\Controllers\UserController@editPost')->name('user.edit-post');
        Route::get('/user', 'App\Http\Controllers\UserController@index')->name('user.index');
    });

    Route::view('/help', 'help')->name('help');
    Route::view('/help/absent-register', 'help.absent-register')->name('help.absent-register');
    Route::view('/help/elector-markers', 'help.elector-markers')->name('help.elector-markers');
    Route::view('/help/electoral-register', 'help.electoral-register')->name('help.electoral-register');
    Route::view('/help/getting-started', 'help.getting-started')->name('help.getting-started');
    Route::view('/help/marked-register', 'help.marked-register')->name('help.marked-register');

    Route::view('/static/elector-markers', 'static.elector-markers')->name('static.elector-markers');
});
