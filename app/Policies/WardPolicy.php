<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Ward;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class WardPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->is_super_admin) 
        {
            return true;
        }
    }

    public function show(User $user, Ward $ward)
    {
        return in_array($ward->council_id, $user->councils()->pluck('councils.id')->toArray());
    }
}
