<?php

namespace App\Policies;

use App\Models\Casework;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CaseworkPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->is_super_admin)
        {
            return true;
        }
    }

    public function comment(User $user, Casework $casework)
    {
        return $user->id === $casework->user_id;
    }

    public function show(User $user, Casework $casework)
    {
        return $user->id === $casework->user_id;
    }
}
