<?php

namespace App\Policies;

use App\Models\Road;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RoadPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->is_super_admin) 
        {
            return true;
        }
    }

    public function canvass(User $user, Road $road)
    {
        return in_array($road->council_id, $user->councils()->pluck('councils.id')->toArray());
    }

    public function show(User $user, Road $road)
    {
        return in_array($road->council_id, $user->councils()->pluck('councils.id')->toArray());
    }
}
