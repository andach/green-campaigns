<?php

namespace App\Policies;

use App\Models\Council;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CouncilPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->is_super_admin)
        {
            return true;
        }
    }

    public function show(User $user, Council $council)
    {
        return in_array($council->id, $user->councils()->pluck('councils.id')->toArray());
    }
}
