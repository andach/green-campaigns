<?php

namespace App\Http\Controllers;

use App\Enums\ContactTypes;
use App\Models\Activist;
use App\Models\Contact;
use App\Models\Council;
use App\Models\Event;
use Auth;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function create(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::whereIn('id', Auth::user()->canAccessCouncilIDs())->pluck('name', 'id')->toArray();

        return view('main.event.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'council_id'    => 'required',
            'date_of_event' => 'required',
            'name'          => 'required',
        ]);

        Auth::user()->events()->create([
            'council_id'    => $request->council_id,
            'date_of_event' => $request->date_of_event,
            'name'          => $request->name,
            'notes'         => $request->notes ?? '',
        ]);

        session()->flash('success', 'The event has been created.');

        return redirect()->route('event.index');
    }

    public function contact(int $id, string $activistID): \Illuminate\View\View
    {
        $args = [];
        $args['event']        = Event::find($id);
        $args['activist']     = Activist::find($activistID);
        $args['contacts']     = Contact::where('activist_id', $activistID)->where('event_id', $id)->get();
        $args['contactTypes'] = ContactTypes::toArray();
        $args['isAttending']  = $args['activist']->events()->max('is_attending');

        $this->authorize('show', $args['event']->council);

        return view('main.event.contact', $args);
    }

    public function contactNotComing(int $id, string $activistID): \Illuminate\Http\RedirectResponse
    {
        $event  = Event::find($id);
        $event->activists()->updateExistingPivot($activistID, ['is_attending' => 0]);

        session()->flash('success', 'This person has been marked as not attending.');

        return redirect()->route('event.show', $id);
    }

    public function contactPost(Request $request, int $id, string $activistID): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'date_of_contact'   => 'required',
            'enum_contact_type' => 'required'
        ]);

        Contact::create([
            'activist_id'        => $activistID,
            'event_id'           => $id,
            'user_id'            => Auth::id(),
            'date_of_contact'    => $request->date_of_contact,
            'enum_contact_type'  => $request->enum_contact_type,
            'managed_to_contact' => $request->managed_to_contact,
            'notes'              => $request->notes ?? '',
        ]);

        if ($request->managed_to_contact)
        {
            $event = Event::find($id);
            $event->activists()->detach($activistID);
            $event->activists()->attach($activistID, ['is_attending' => $request->is_attending]);
        }

        session()->flash('success', 'Information added successfully.');

        return redirect()->route('event.show', $id);
    }

    public function edit(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['event'] = Event::find($id);

        $this->authorize('show', $args['event']->council);

        return view('main.event.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'date_of_event' => 'required',
            'name'          => 'required',
        ]);

        $event = Event::find($id);
        $event->update($request->all());

        session()->flash('success', 'The event has been edited.');

        return redirect()->route('event.index');
    }

    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::whereIn('id', Auth::user()->canAccessCouncilIDs())->with('events')->get();

        return view('main.event.index', $args);
    }

    public function show(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['event']     = Event::find($id);
        $args['activists'] = Activist::where('council_id', $args['event']->council_id)->where('do_not_phone', '<>', 1)->with('contacts')->get();
        $args['attending'] = $args['event']->activists()->where('is_attending', 1)->pluck('activists.id')->toArray();
        $args['contacted'] = $args['event']->contacts()->where('managed_to_contact', 1)->pluck('activist_id')->toArray();

        $args['activists'] = Activist::where('council_id', $args['event']->council_id)->where('do_not_phone', '<>', 1);

        foreach (['forename', 'surname', 'council_ward', 'local_authority', 'constituency'] as $search)
        {
            if (isset($_GET[$search]))
            {
                if ($_GET[$search])
                {
                    $args['activists'] = $args['activists']->where($search, 'LIKE', '%'.$_GET[$search].'%');
                }
            }
        }

        if (isset($_GET['is_member']))
        {
            if ($_GET['is_member'] !== "")
            {
                $args['activists'] = $args['activists']->where('is_member', $_GET['is_member']);
            }
        }

        $args['activists'] = $args['activists']->with('contacts')->get();

        $this->authorize('show', $args['event']->council);

        return view('main.event.show', $args);
    }
}
