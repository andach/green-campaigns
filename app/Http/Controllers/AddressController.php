<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Voter;
use App\Models\Ward;
use Auth;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function create(): \Illuminate\View\View
    {
        $args = [];
        $args['wards'] = Ward::whereIn('council_id', Auth::user()->canAccessCouncilIDs())->pluck('name', 'id')->toArray();

        return view('main.address.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'road_id' => 'required',
            'ward_id' => 'required',
            'name'    => 'required',
        ]);

        $existingAddress = Address::where('road_id', $request->road_id)
            ->where('ward_id', $request->ward_id)
            ->where('name', $request->name)->first();

        if ($existingAddress)
        {
            session()->flash('danger', 'This address already exists.');

            return redirect()->route('address.create');
        }

        session()->flash('success', 'The address has been created.');

        $createArray = $request->all();
        $createArray['council_id'] = Ward::find($request->ward_id)->council_id;

        Address::create($createArray);

        return redirect()->route('ward.show', $request->ward_id);
    }

    public function edit(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['address']  = Address::find($id);

        return view('main.address.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $address = Address::find($id);

        if (is_array($request->delete_voters))
        {
            foreach ($request->delete_voters as $voterID)
            {
                Voter::destroy($voterID);
            }
        }

        if ($request->forename && $request->surname)
        {
            $address->voters()->create([
                'council_id' => $address->council_id,
                'road_id'    => $address->road_id,
                'ward_id'    => $address->ward_id,
                'forename'   => $request->forename,
                'surname'    => $request->surname,
                'is_postal'  => $request->is_postal ?? 0,
                'notes'      => $request->notes,
            ]);
        }

        session()->flash('success', 'The voters at this address have been updated');

        return redirect()->route('address.edit', $id);
    }
}
