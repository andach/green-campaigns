<?php

namespace App\Http\Controllers;

use App\Enums\GenerallyVotes;
use App\Enums\VoteGreenLikelihood;
use App\Models\Address;
use App\Models\Election;
use App\Models\Canvass;
use App\Models\Road;
use App\Models\Voter;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Collection;

class CanvassController extends Controller
{
    public function address(int $addressID): \Illuminate\View\View
    {
        $args = [];
        $args['enumGenerallyVotes']       = GenerallyVotes::toArray();
        $args['enumVoteGreenLikelihood']  = VoteGreenLikelihood::toArray();
        $args['address']                  = Address::find($addressID);
        $args['mostRecentSurveyResponse'] = $args['address']->surveyResponses()->orderBy('date_of_response', 'desc')->first();
        $args['allAddresses']             = Address::where('road_id', $args['address']->road_id)->pluck('name', 'id')->toArray();

        $this->authorize('canvass', $args['address']->road);

        if ($addressID)
        {
            $args['nextAddresses'] = $args['address']->getNextCanvassHouses();
        }

        return view('main.canvass.address', $args);
    }

    public function addressPost(Request $request, int $addressID): \Illuminate\Http\RedirectResponse
    {
        $address = Address::find($addressID);
        $address->notes = $request->notes;
        $address->save();

        foreach ($request->voter_ids as $voterID)
        {
            $voter = Voter::find($voterID);
            $voter->updateVotingIntention(
                $request->council_enum_generally_votes[$voterID],
                $request->council_enum_vote_green_likelihood[$voterID],
                $request->parl_enum_generally_votes[$voterID],
                $request->parl_enum_vote_green_likelihood[$voterID],
                $request->notes[$voterID] ?? '',
            );
        }

        $nextAddressID = $request->next_address;
        if ($request->next_address_manual)
        {
            $nextAddressID = $request->next_address_manual;
        }

        session()->flash('success', 'Canvass data has been updated');

        return redirect()->route('canvass.address', [$nextAddressID]);
    }

    public function print(int $roadID): \Illuminate\View\View
    {
        $args = [];
        $args['road']                     = Road::find($roadID);
        $args['enumGenerallyVotes']       = GenerallyVotes::toArray();
        $args['enumVoteGreenLikelihood']  = VoteGreenLikelihood::toArray();
        $args['absentMarked']             = 0;
        $args['addresses']                = $args['road']->addresses()->orderByRaw('CONVERT(name, SIGNED) asc')->get();

        $this->authorize('canvass', $args['road']);

        return view('main.canvass.print', $args);
    }

    public function printAbsentMarked(int $roadID): \Illuminate\View\View
    {
        $args = [];
        $args['road']                     = Road::find($roadID);
        $args['enumGenerallyVotes']       = GenerallyVotes::toArray();
        $args['enumVoteGreenLikelihood']  = VoteGreenLikelihood::toArray();
        $args['absentMarked']             = 1;

        $mostRecentElection               = Election::where('council_id', $args['road']->council_id)->first();
        $votersWhoVotedAtThisElection     = collect();
        if (count($mostRecentElection->voters))
        {
            $votersWhoVotedAtThisElection     = $mostRecentElection->voters()->pluck('voters.id');
        }

        $absentAndMarkedVoterAddressIDs   = Voter::where('is_postal', 1)
            ->orWhereIn('id', $votersWhoVotedAtThisElection)
            ->where('road_id', $roadID)
            ->pluck('address_id')
            ->toArray();

        $args['addresses']                = Address::whereIn('id', $absentAndMarkedVoterAddressIDs)->orderByRaw('CONVERT(name, SIGNED) asc')->with('voters')->get();

        $this->authorize('canvass', $args['road']);

        return view('main.canvass.print', $args);
    }

    public function road(int $roadID): \Illuminate\View\View
    {
        $args = [];
        $args['road']                     = Road::find($roadID);
        $args['enumGenerallyVotes']       = GenerallyVotes::toArray();
        $args['enumVoteGreenLikelihood']  = VoteGreenLikelihood::toArray();
        $args['absentMarked']             = 0;
        $args['addresses']                = $args['road']->addresses()->orderByRaw('CONVERT(name, SIGNED) asc')->get();

        $this->authorize('canvass', $args['road']);

        return view('main.canvass.road', $args);
    }

    public function roadAbsentMarked(int $roadID): \Illuminate\View\View
    {
        $args = [];
        $args['road']                     = Road::find($roadID);
        $args['enumGenerallyVotes']       = GenerallyVotes::toArray();
        $args['enumVoteGreenLikelihood']  = VoteGreenLikelihood::toArray();
        $args['absentMarked']             = 1;

        $mostRecentElection               = Election::where('council_id', $args['road']->council_id)->first();

        $votersWhoVotedAtThisElection     = collect();
        if (count($mostRecentElection->voters))
        {
            $votersWhoVotedAtThisElection     = $mostRecentElection->voters()->pluck('voters.id');
        }

        $absentAndMarkedVoterAddressIDs   = Voter::where('is_postal', 1)
            ->orWhereIn('id', $votersWhoVotedAtThisElection)
            ->where('road_id', $roadID)
            ->pluck('address_id')
            ->toArray();

        $args['addresses']                = Address::whereIn('id', $absentAndMarkedVoterAddressIDs)->orderByRaw('CONVERT(name, SIGNED) asc')->with('voters')->get();

        $this->authorize('canvass', $args['road']);

        return view('main.canvass.road', $args);
    }

    public function roadPost(Request $request, int $roadID): \Illuminate\Http\RedirectResponse
    {
        foreach ($request->voter_ids as $voterID)
        {
            $voter = Voter::find($voterID);
            $voter->updateVotingIntention(
                $request->council_enum_generally_votes[$voterID],
                $request->council_enum_vote_green_likelihood[$voterID],
                $request->parl_enum_generally_votes[$voterID],
                $request->parl_enum_vote_green_likelihood[$voterID],
                $request->notes[$voterID] ?? '',
            );
        }

        session()->flash('success', 'The canvassing session has been updated.');

        return redirect()->route('canvass.road', $roadID);
    }
}
