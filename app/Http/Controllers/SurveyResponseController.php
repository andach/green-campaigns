<?php

namespace App\Http\Controllers;

use App\Models\Survey\Response;
use Illuminate\Http\Request;

class SurveyResponseController extends Controller
{
    public function show(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['response'] = Response::find($id);
        $args['issues']   = $args['response']->issuesArray();
        $args['parties']  = $args['response']->partiesArray();

        return view('main.survey-response.show', $args);
    }
}
