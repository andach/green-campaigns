<?php

namespace App\Http\Controllers;

use App\Models\Council;
use App\Models\Ward;
use Illuminate\Http\Request;

class CouncilController extends Controller
{
    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::all();

        return view('main.council.index', $args);
    }

    public function indexPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $responseArr = [];

        if ($request->new_council)
        {
            $council        = new Council();
            $council->name  = $request->new_council;
            $council->save();

            $responseArr[] = 'The new council has been added.';
        }

        if ($request->council_id && $request->new_ward)
        {
            $ward                   = new Ward();
            $ward->name             = $request->new_ward;
            $ward->council_id       = $request->council_id;
            $ward->electoral_prefix = $request->electoral_prefix;
            $ward->save();

            $responseArr[] = 'The new ward has been added.';
        }

        if ($request->delete_council)
        {
            foreach ($request->delete_council as $id)
            {
                Council::destroy($id);
            }

            $responseArr[] = 'The selected council(s) have been deleted.';
        }

        if ($request->delete_ward)
        {
            foreach ($request->delete_ward as $id)
            {
                Ward::destroy($id);
            }

            $responseArr[] = 'The selected ward(s) have been deleted.';
        }

        if ($responseArr)
        {
            session()->flash('success', implode(' ', $responseArr));
        }

        return redirect()->route('council.index');
    }
}
