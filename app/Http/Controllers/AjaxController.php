<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Road;
use App\Models\Voter;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function addressToVoters(int $id): \Illuminate\Support\Collection
    {
        return Voter::where('address_id', $id)->get()->pluck('name', 'id');
    }

    public function roadToAddresses(int $id): \Illuminate\Support\Collection
    {
        return Address::where('road_id', $id)->pluck('name', 'id');
    }

    public function wardToRoads(int $id): \Illuminate\Support\Collection
    {
        return Road::where('ward_id', $id)->pluck('name', 'id');
    }
}
