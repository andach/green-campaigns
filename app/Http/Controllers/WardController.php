<?php

namespace App\Http\Controllers;

use App\Enums\GenerallyVotesColor;
use App\Enums\VoteGreenLikelihoodColor;
use App\Models\Address;
use App\Models\Survey;
use App\Models\Voter;
use App\Models\Ward;
use Auth;
use Illuminate\Http\Request;

class WardController extends Controller
{
    public function export(int $id): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $fileName = 'ward.csv';
        $ward     = Ward::find($id);
        $election = $ward->council->elections()->orderBy('year', 'desc')->first();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = Voter::csvColumns();

        $callback = function () use ($columns, $ward, $election): void {
            $file = fopen('php://output', 'w');

            fputcsv($file, $columns);
            foreach ($ward->voters as $voter) {
                fputcsv($file, $voter->csvRow($election->id));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['wards'] = Ward::whereIn('council_id', Auth::user()->canAccessCouncilIDs())->withCount(['addresses', 'voters'])->with('council')->get();

        return view('main.ward.index', $args);
    }

    public function show(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['ward']   = Ward::find($id);
        $args['roads']  = $args['ward']->roads()->orderBy('name')->get();
        $args['routes'] = $args['ward']->routes()->pluck('name', 'id');
        $args['colorsVotes'] = GenerallyVotesColor::toArray();
        $args['colorsGreen'] = VoteGreenLikelihoodColor::toArray();

        $this->authorize('show', $args['ward']);

        return view('main.ward.show', $args);
    }
}
