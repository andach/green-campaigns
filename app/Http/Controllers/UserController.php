<?php

namespace App\Http\Controllers;

use App\Models\Council;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function create(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::pluck('name', 'id');

        return view('main.user.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $councilIDs = $request->council_ids ?? [];
        $user->councils()->sync(array_filter($councilIDs));

        session()->flash('success', 'The user has been created.');

        return redirect()->route('user.index');
    }

    public function edit(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::pluck('name', 'id');
        $args['user']     = User::find($id);

        return view('main.user.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,'.$id,
            'password' => 'nullable|min:8|confirmed',
        ]);

        $user = User::find($id);
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        $councilIDs = $request->council_ids ?? [];
        $user->councils()->sync(array_filter($councilIDs));

        if ($request->password)
        {
            $user->password = Hash::make($request->password);
            $user->save();
        }

        session()->flash('success', 'The user has been edited.');

        return redirect()->route('user.edit', $user->id);
    }

    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['users'] = User::all();

        return view('main.user.index', $args);
    }

    public function myAccount(): \Illuminate\View\View
    {
        $args = [];
        $args['user'] = Auth::user();

        return view('main.user.my-account', $args);
    }

    public function myAccountPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $user = Auth::user();

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,'.$user->id,
            'password' => 'nullable|min:8|confirmed',
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        if ($request->password)
        {
            $user->password = Hash::make($request->password);
            $user->save();
        }

        session()->flash('success', 'Your account has been updated.');

        return redirect()->route('user.my-account');
    }
}
