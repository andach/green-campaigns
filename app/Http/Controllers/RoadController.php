<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Road;
use Illuminate\Http\Request;

class RoadController extends Controller
{
    public function show(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['road']      = Road::find($id);
        $args['addresses'] = $args['road']->addresses()->orderByRaw('CONVERT(name, SIGNED) asc')->get();
        $args['routes']    = $args['road']->ward->routes()->pluck('name', 'id');

        $this->authorize('show', $args['road']);

        return view('main.road.show', $args);
    }
}
