<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Council;
use App\Models\Election;
use App\Models\Road;
use App\Models\Ward;
use App\Models\Voter;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function absentRegister(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::pluck('name', 'id');
        $args['wards']    = Council::pluck('name', 'id');

        return view('main.upload.absent-register', $args);
    }

    public function absentRegisterPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        activity()->disableLogging();

        $request->validate([
            'file' => 'required',
        ]);

        $path = $request->file('file')->storeAs('uploads', date('Y-m-d his').' absent-register.csv');

        $data = array();
        $filename = storage_path().'/app/'.$path;
        $handle = fopen($filename, 'r');
        if ($handle !== false)
        {
            while (($row = fgetcsv($handle, 1000, ',')) !== false)
            {
                $data[] = $row[0];
            }
            fclose($handle);
        }

        if (isset($request->ward_id))
        {
            Voter::where('ward_id', $request->ward_id)->update(['is_postal' => null]);
        }

        if (isset($request->council_id))
        {
            Voter::where('council_id', $request->council_id)->update(['is_postal' => null]);
        }

        Voter::whereIn('elector_number_full', $data)->update(['is_postal' => 1]);

        session()->flash('success', 'The absent register information has been updated.');

        return redirect()->route('upload.absent-register');
    }

    public function electoralRegister(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::pluck('name', 'id');
        $args['wards']    = Ward::pluck('name', 'id');

        return view('main.upload.electoral-register', $args);
    }

    public function electoralRegisterPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        activity()->disableLogging();

        set_time_limit(300);

        $request->validate([
            'council_id' => 'required',
            'date_of_electoral_register' => 'required',
            'ward_id' => 'required',
            'file' => 'required',
        ]);

        $path = $request->file('file')->storeAs('uploads', date('Y-m-d his').' electoral-register.csv');

        $customerArr = $this->csvToArray(storage_path().'/app/'.$path);

        // Need to check the headers for the provided CSV are correct. No, I don't know why the question mark in the diamond is there.
        $headers = [
            "﻿Elector Number Prefix",
            "Elector Number",
            "Elector Number Suffix",
            "Elector Markers",
            "Elector DOB",
            "Elector Surname",
            "Elector Forename",
            "PostCode",
            "Address1",
            "Address2",
            "Address3",
            "Address4",
            "Address5",
            "Address6",
        ];

        if ($headers != array_keys($customerArr[0]))
        {
            session()->flash('danger', 'The headers of the CSV you provided weren\'t right. Nothing has been uploaded or updated.');
            return redirect()->route('upload.electoral-register');
        }

        // Now we need to check that there's a space in each of the Address1 items.
        // foreach ($customerArr as $line)
        // {
        //     if (!isset(explode(' ', $line['Address1'], 2)[1]))
        //     {
        //         session()->flash('danger', 'There is at least one entry in the "Address1" field that doesn\'t appear fully formatted. There needs to be a space in every entry, like "1 The Street". Check these, and if necessary, manually edit the file.');
        //         return redirect()->route('upload.electoral-register');
        //     }
        // }

        $council = Council::find($request->council_id);
        $council->uploadUsers($customerArr, $request->ward_id);

        session()->flash('success', 'The electoral register has been updated');

        return redirect()->route('upload.electoral-register');
    }

    public function index(): \Illuminate\View\View
    {
        return view('main.upload.index');
    }

    public function markedRegister(): \Illuminate\View\View
    {
        $args = [];
        $args['elections'] = Election::pluck('name', 'id');

        return view('main.upload.marked-register', $args);
    }

    public function markedRegisterPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        activity()->disableLogging();

        $path = $request->file('file')->storeAs('uploads', date('Y-m-d his').' marked-register.csv');

        $data = array();
        $filename = storage_path().'/app/'.$path;
        $handle = fopen($filename, 'r');
        if ($handle !== false)
        {
            while (($row = fgetcsv($handle, 1000, ',')) !== false)
            {
                $data[] = $row[0];
            }
            fclose($handle);
        }

        $ids = Voter::whereIn('elector_number_full', $data)->pluck('id');
        Election::find($request->election_id)->voters()->sync($ids);

        session()->flash('success', 'The marked register information has been updated.');

        return redirect()->route('upload.marked-register');
    }

//    private function addPersonFromElectoralRegister(array $line): void
//    {
//        // dd($line);
//        /*
//          16 => array:14 [▼
//            "Elector Number Prefix" => "SP1"
//            "Elector Number" => "24"
//            "Elector Number Suffix" => "0"
//            "Elector Markers" => ""
//            "Elector DOB" => ""
//            "Elector Surname" => "Smith"
//            "Elector Forename" => "John"
//            "PostCode" => "DE21 7AW"
//            "Address1" => "1 Avondale Road"
//            "Address2" => "Spondon"
//            "Address3" => "Derby"
//            "Address4" => "DE21 7AW"
//            "Address5" => ""
//            "Address6" => ""
//        ]
//        */
//
//        // Could be some issue with UTF-8 encoding?
//        // dd($line['﻿Elector Number Prefix']);
//
//        // Wards are determined by the first two letters of the Electoral Number Prefix. SP is Spondon, for example.
//        $ward = Ward::where('electoral_prefix', substr($line['﻿Elector Number Prefix'], 0, 2))->first();
//
//        if (!$ward)
//        {
//            dd('no ward found');
//        }
//
//        $nameAndNumber = $this->getRoadNameAndNumberFromElectoralRegister($line);
//
//        $road = Road::firstOrCreate([
//                'name'    => $nameAndNumber['name'],
//                'ward_id' => $ward->id,
//            ]);
//
//        $address = Address::firstOrCreate([
//                'name'    => $nameAndNumber['number'],
//                'road_id' => $road->id,
//            ]);
//
//        // // First soft-delete all the existing voters.
//        // Voter::where('address_id', $address->id)->delete();
//
//        $voter = Voter::withTrashed()
//            ->where('forename', $line['Elector Forename'])
//            ->where('surname', $line['Elector Surname'])
//            ->where('address_id', $address->id)
//            ->restore();
//
//        // If $voter isn't full, then they must be a new voter. Enter them into the database.
//        if (!$voter)
//        {
//            Voter::create([
//                'forename'   => $line['Elector Forename'],
//                'surname'    => $line['Elector Surname'],
//                'address_id' => $address->id,
//            ]);
//        }
//    }

    private function csvToArray(string $filename = '', string $delimiter = ','): array
    {
        // dd($filename);
        if (!file_exists($filename))
            dd('file doesnt exist');
            // return false;

        if (!is_readable($filename))
            dd('file not readable');
            // return false;

        $header = null;
        $data = array();
        $handle = fopen($filename, 'r');
        if ($handle !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
