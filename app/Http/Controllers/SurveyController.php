<?php

namespace App\Http\Controllers;

use App\Enums\GenerallyVotes;
use App\Models\Council;
use App\Models\Survey;
use App\Models\Survey\Issue;
use App\Models\Survey\Question;
use App\Models\Survey\QuestionOption;
use App\Models\Ward;
use Auth;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function create(): \Illuminate\View\View
    {
        $args = [];
        $args['councils'] = Council::whereIn('id', Auth::user()->canAccessCouncilIDs())->get()->pluck('name', 'id')->toArray();

        return view('main.survey.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'council_id' => 'required',
        ]);

        $survey = Survey::create([
            'council_id' => $request->council_id,
            'user_id'    => Auth::id(),
            'name'       => $request->name,
        ]);

        session()->flash('success', 'The survey has been created successfully. Now add some questions to it.');

        return redirect()->route('survey.edit', $survey->id);
    }

    public function edit(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['survey'] = Survey::find($id);

        return view('main.survey.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $survey = Survey::find($id);
        $survey->name = $request->name;
        $survey->save();

        if ($survey->canEdit())
        {
            $issuesArray = explode(PHP_EOL, $request->add_issues ?? []);

            foreach ($issuesArray as $text)
            {
                $survey->issues()->create(['name' => $text]);
            }


            if ($request->new_question_name)
            {
                $question = $survey->questions()->create(['name' => $request->new_question_name]);

                if ($request->new_question_options)
                {
                    $optionsArray = explode(PHP_EOL, $request->new_question_options);

                    foreach ($optionsArray as $option)
                    {
                        $question->options()->create(['name' => $option]);
                    }
                }
            }

            QuestionOption::destroy($request->delete_options ?? []);
            Question::destroy($request->delete_questions ?? []);
            Issue::destroy($request->delete_issues ?? []);

            if ($request->add_option_to_question)
            {
                foreach ($request->add_option_to_question as $questionID => $text)
                {
                    if ($text)
                    {
                        $question = Question::find($questionID);
                        $question->options()->create(['name' => $text]);
                    }
                }
            }
        }

        return redirect()->route('survey.edit', $id);
    }

    public function export(int $id): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $fileName = 'survey.csv';
        $survey   = Survey::find($id);

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = $survey->csvHeaders();
        $rows    = $survey->csvRows();

        $callback = function () use ($columns, $rows): void {
            $file = fopen('php://output', 'w');

            fputcsv($file, $columns);
            foreach ($rows as $row) {
                fputcsv($file, $row);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['surveys'] = Survey::whereIn('council_id', Auth::user()->canAccessCouncilIDs())->get();

        return view('main.survey.index', $args);
    }

    public function response(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['survey']  = Survey::find($id);
        $args['parties'] = GenerallyVotes::toArray();
        $args['wards']   = Ward::whereIn('council_id', Auth::user()->canAccessCouncilIDs())->pluck('name', 'id')->toArray();

        return view('main.survey.response', $args);
    }

    public function responsePost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'ward_id' => 'required',
            'date_of_response' => 'required',
        ]);

        // Doing this to keep the foreign key constraints valid.
        $insert = $request->all();
        if (!$insert['address_id'])
        {
            $insert['address_id'] = null;
        }
        if (!$insert['voter_id'])
        {
            $insert['voter_id'] = null;
        }

        $survey   = Survey::find($id);
        $response = $survey->responses()->create($insert);

        if ($request->option_id)
        {
            foreach ($request->option_id as $questionID => $optionID)
            {
                $response->answers()->create([
                    'option_id'       => $optionID,
                    'question_id'     => $questionID,
                    'additional_text' => $request->additional_text[$questionID],
                ]);
            }
        }

        if ($request->issue)
        {
            foreach ($request->issue as $issueID => $answer)
            {
                $response->issues()->create([
                    'issue_id' => $issueID,
                    'answer'   => $answer,
                ]);
            }
        }

        if ($request->party)
        {
            foreach ($request->party as $enumParty => $answer)
            {
                $response->parties()->create([
                    'enum_political_party' => $enumParty,
                    'answer'               => $answer,
                ]);
            }
        }

        session()->flash('success', 'The survey response has been logged.');

        return redirect()->route('survey.response', $id);
    }

    public function show(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['survey'] = Survey::find($id);

        return view('main.survey.show', $args);
    }

    public function responses(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['survey'] = Survey::find($id);

        return view('main.survey.responses', $args);
    }
}
