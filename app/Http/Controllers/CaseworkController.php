<?php

namespace App\Http\Controllers;

use App\Models\Casework;
use App\Models\Voter;
use Auth;
use Illuminate\Http\Request;

class CaseworkController extends Controller
{
    public function create(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['voter'] = Voter::find($id);

        $this->authorize('show', $args['voter']->ward);

        return view('main.casework.create', $args);
    }

    public function createPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'title' => 'required',
            'comment' => 'required',
        ]);

        $voter = Voter::find($id);
        $casework = Casework::create([
            'address_id' => $voter->address_id,
            'council_id' => $voter->council_id,
            'voter_id' => $id,
            'ward_id'  => $voter->ward_id,
            'user_id'  => Auth::id(),
            'name'     => $request->title,
        ]);

        $casework->comments()->create([
            'comment' => $request->comment,
            'user_id' => Auth::id(),
        ]);

        session()->flash('success', 'The new item of casework has been created');

        return redirect()->route('casework.show', $casework->id);
    }

    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['caseworks'] = Casework::mine()->get();

        return view('main.casework.index', $args);
    }

    public function show(int $id): \Illuminate\View\View
    {
        $args = [];
        $args['casework'] = Casework::find($id);

        $this->authorize('show', $args['casework']);

        return view('main.casework.show', $args);
    }

    public function showPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $casework = Casework::find($id);

        $this->authorize('show', $casework);

        $return = '';

        if ($request->comment)
        {
            $casework->comments()->create([
                'comment' => $request->comment,
                'user_id' => Auth::id(),
            ]);

            $return = 'Casework has been updated';
        }

        if ($request->is_closed)
        {
            $casework->is_closed = 1;
            $casework->save();

            $return = 'Casework has been updated';
        }

        if ($request->reopen)
        {
            $casework->is_closed = null;
            $casework->save();

            $return = 'The casework item has been reopened';
        }

        session()->flash('success', $return);

        return redirect()->route('casework.show', $casework->id);
    }
}
