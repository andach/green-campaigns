<?php

namespace App\Http\Controllers;

use App\Enums\ContactTypes;
use App\Models\Activist;
use App\Models\Contact;
use App\Models\Council;
use App\Models\Event;
use Auth;
use Illuminate\Http\Request;

class ActivistController extends Controller
{
    public function contactDelete(Request $request): \Illuminate\Http\RedirectResponse
    {
        $contact = Contact::find($request->contact_id);

        $this->authorize('show', $contact->activist->council);

        $activistID = $contact->activist_id;
        $contact->delete();

        session()->flash('success', 'The contact record has been deleted.');

        return redirect()->route('activist.show', $activistID);
    }

    public function export(int $id): \Illuminate\View\View
    {
//        $council  = Council::find($id);

//        $this->authorize('show', $council);

        $fileName = 'activists.csv';

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns   = Activist::csvHeaders();
        $activists = Activist::where('council_id', $id)->get();
        $rows      = [];

        foreach ($activists as $activist)
        {
            $rows[] = $activist->csvRow();
        }

        $callback = function () use ($columns, $rows): void {
            $file = fopen('php://output', 'w');

            fputcsv($file, $columns);
            foreach ($rows as $row) {
                fputcsv($file, $row);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function index(): \Illuminate\View\View
    {
        $args = [];
        $args['councils']  = Council::whereIn('id', Auth::user()->canAccessCouncilIDs())->with('activists')->get();
        $args['activists'] = Activist::whereIn('council_id', Auth::user()->canAccessCouncilIDs());

        foreach (['forename', 'surname', 'council_ward', 'local_authority', 'constituency'] as $search)
        {
            if (isset($_GET[$search]))
            {
                if ($_GET[$search])
                {
                    $args['activists'] = $args['activists']->where($search, 'LIKE', '%'.$_GET[$search].'%');
                }
            }
        }

        if (isset($_GET['is_member']))
        {
            if ($_GET['is_member'] !== "")
            {
                $args['activists'] = $args['activists']->where('is_member', $_GET['is_member']);
            }
        }

        $args['activists'] = $args['activists']->get();

        return view('main.activist.index', $args);
    }

    public function show(string $id): \Illuminate\View\View
    {
        $args = [];
        $args['activist']     = Activist::with('contacts')->find($id);
        $args['events']       = Event::whereIn('council_id', Auth::user()->canAccessCouncilIDs())->pluck('name', 'id')->toArray();
        $args['contactTypes'] = ContactTypes::toArray();

        $this->authorize('show', $args['activist']->council);

        return view('main.activist.show', $args);
    }

    public function showPost(Request $request, string $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'date_of_contact'   => 'required',
            'enum_contact_type' => 'required'
        ]);

        Contact::create([
            'activist_id'        => $id,
            'event_id'           => $request->event_id,
            'user_id'            => Auth::id(),
            'date_of_contact'    => $request->date_of_contact,
            'enum_contact_type'  => $request->enum_contact_type,
            'managed_to_contact' => $request->managed_to_contact,
            'notes'              => $request->notes ?? '',
        ]);

        if ($request->managed_to_contact && $request->event_id)
        {
            $event = Event::find($request->event_id);
            $event->activists()->detach($id);
            $event->activists()->attach($id, ['is_attending' => $request->is_attending]);
        }

        session()->flash('success', 'Information added successfully.');

        return redirect()->route('activist.show', $id);
    }

    public function upload(Request $request): \Illuminate\Http\RedirectResponse
    {
        activity()->disableLogging();

        set_time_limit(300);

        $request->validate([
            'council_id' => 'required',
            'file'       => 'required',
        ]);

        $path = $request->file('file')->storeAs('uploads', date('Y-m-d his').' activists-and-members.csv');

        $activistArr = $this->csvToArray(storage_path().'/app/'.$path, $request->council_id);

        Activist::where('council_id', $request->council_id)->delete();
        Activist::upsert(
            $activistArr,
            ['id'],
            [
                'council_id', 'forename', 'surname', 'email', 'mobile_number', 'phone_number', 'do_not_phone',
                'address', 'city', 'postcode', 'council_ward', 'local_authority', 'constituency',
                'is_member', 'membership_start_date', 'membership_end_date', 'membership_type', 'deleted_at'
            ]
        );

        session()->flash('success', 'The activist list has been updated');

        return redirect()->route('activist.index');
    }

    private function csvToArray(string $filename, int $councilID, string $delimiter = ','): array
    {
        // dd($filename);
        if (!file_exists($filename))
            dd('file doesnt exist');
        // return false;

        if (!is_readable($filename))
            dd('file not readable');
        // return false;

        $header = null;
        $data = array();
        $handle = fopen($filename, 'r');
        if ($handle !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        $renamedData =[];
        foreach ($data as $row)
        {
            $startDate = $this->formatGivenDate($row['Membership_Join_Date']);
            $endDate   = $this->formatGivenDate($row['Membership_End_Date']);

            $renamedData[] = [
                'id'                    => $row['uuid'],
                'council_id'            => $councilID,
                'forename'              => $row['first_name'],
                'surname'               => $row['last_name'],
                'email'                 => $row['email'],
                'mobile_number'         => Activist::parseMobileNumber($row),
                'phone_number'          => Activist::parseHomeNumber($row),
                'do_not_phone'          => $row['do_not_phone'] ? 1 : 0,
                'address'               => $row['can2_user_address'],
                'city'                  => $row['can2_user_city'],
                'postcode'              => $row['zip_code'],
                'council_ward'          => $row['Ward'],
                'local_authority'       => $row['Local_Authority'],
                'constituency'          => $row['Parliamentary_Constituency'],
                'is_member'             => $row['Member'] ? 1 : 0,
                'membership_start_date' => $startDate,
                'membership_end_date'   => $endDate,
                'membership_type'       => $row['Membership_Type'],
                'deleted_at'            => null,
            ];
        }

        return $renamedData;
    }

    private function formatGivenDate(string $date): ?string
    {
        $dateToReturn = \DateTime::createFromFormat('Y/m/d', $date);
        if ($dateToReturn === false) {
            $dateToReturn = \DateTime::createFromFormat('d/m/Y', $date);

            if ($dateToReturn === false) {
                $dateToReturn = '';
            } else {
                $dateToReturn = date('Y-m-d', $dateToReturn->getTimestamp());
            }
        } else {
            $dateToReturn = date('Y-m-d', $dateToReturn->getTimestamp());
        }

        if (!$dateToReturn)
        {
            $dateToReturn = null;
        }

        if (!$dateToReturn == '0000-00-00')
        {
            $dateToReturn = null;
        }

        return $dateToReturn;
    }
}
