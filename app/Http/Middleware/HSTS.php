<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HSTS
{
    public function handle(Request $request, Closure $next): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\StreamedResponse|\Illuminate\Http\JsonResponse
    {
        $response = $next($request);

        if (!get_class($response) === 'Symfony\Component\HttpFoundation\StreamedResponse')
        {
            $response->header('Strict-Transport-Security', 'max-age=31536000; includeSubdomains');
        }

        return $response;
    }
}
