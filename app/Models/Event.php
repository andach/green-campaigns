<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Event extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['council_id', 'user_id', 'date_of_event', 'name', 'notes'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function activists(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Activist', 'link_activists_events', 'event_id', 'activist_id');
    }

    public function contacts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function council(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Council');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getNumberAttendedAttribute(): int
    {
        return $this->activists()->wherePivot('did_attend', 1)->count();
    }

    public function getNumberAttendingAttribute(): int
    {
        return $this->activists()->wherePivot('is_attending', 1)->count();
    }
}
