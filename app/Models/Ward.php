<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Ward extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function addresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Address');
    }

    public function council(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Council');
    }

    public function roads(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Road');
    }

    public function routes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Route');
    }

    public function voters(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Voter');
    }

    public function canvassNumbersForChart(string $columnName): array
    {
        $canvassData = Voter::whereNotNull($columnName)
            ->groupBy($columnName)
            ->select($columnName, \DB::raw('count(*) as total'))
            ->get()
            ->toArray();

        $jsArray = [];
        foreach ($canvassData as $array)
        {
            $jsArray[$array[$columnName]] = $array['total'];
        }

        return $jsArray;
    }
}
