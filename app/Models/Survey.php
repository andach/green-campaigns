<?php

namespace App\Models;

use App\Enums\GenerallyVotes;
use App\Traits\Chartable;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Survey extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['council_id', 'user_id', 'name'];
    public $table = 'surveys';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function council(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Council');
    }

    public function issues(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\Issue');
    }

    public function questions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\Question');
    }

    public function responses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\Response');
    }

    public function responsesIssues(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough('App\Models\Survey\ResponseIssue', 'App\Models\Survey\Response');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    public function canEdit(): bool
    {
        return $this->responses->count() == 0;
    }

    public function chartJSIssues(): array
    {
        if (!$this->issues->count())
        {
            return [];
        }

        $surveyData = $this->responsesIssues()
            ->select('issue_id', 'answer')
            ->get()
            ->toArray();

        $jsArray  = [];
        $issueIDs = $this->issues()->orderBy('id')->pluck('id')->toArray();
        foreach ([0, 1, 2] as $answerNum)
        {
            foreach ($issueIDs as $issueID)
            {
                $jsArray[$answerNum][$issueID] = 0;
            }
        }

        foreach ($surveyData as $array)
        {
            $jsArray[$array['answer']][$array['issue_id']]++;
        }

        $returnArray = [];
        $returnArray['Not Important']    = $jsArray[0];
        $returnArray['Fairly Important'] = $jsArray[1];
        $returnArray['Very Important']   = $jsArray[2];

        // dd($returnArray);

        return $returnArray;
    }

    public function chartJSIssuesLabels(): array
    {
        $array = $this->issues()->orderBy('id')->pluck('name')->toArray();

        return array_map('trim', $array);
    }

    public function chartJSItemsByMonth(): array
    {
        $surveyData = $this->responses()
            ->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(date_of_response, '%Y-%m') yearmonth"))
            ->groupby('yearmonth')
            ->get()
            ->toArray();

        $jsArray = [];
        foreach ($surveyData as $array)
        {
            $jsArray[$array['yearmonth']] = $array['data'];
        }

        return $jsArray;
    }

    public function csvHeaders(): array
    {
        $array   = ['Response ID', 'Ward', 'Address', 'Voter'];
        $parties = GenerallyVotes::toArray();

        foreach ($this->issues as $issue)
        {
            $array[] = $issue->name;
        }

        foreach ($parties as $party)
        {
            $array[] = $party;
        }

        foreach ($this->questions as $question)
        {
            $array[] = $question->name;
        }

        return $array;
    }

    public function csvRows(): array
    {
        $array = [];

        foreach ($this->responses as $response)
        {
            $array[] = $response->csvRow();
        }

        return $array;
    }
}
