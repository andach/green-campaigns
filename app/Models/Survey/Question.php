<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Question extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['survey_id', 'name'];
    public $table = 'surveys_questions';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function options(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\QuestionOption', 'question_id');
    }

    public function survey(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Survey');
    }
}
