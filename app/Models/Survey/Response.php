<?php

namespace App\Models\Survey;

use App\Enums\GenerallyVotes;
use App\Models\Survey\QuestionOption;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Response extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['address_id', 'survey_id', 'ward_id', 'voter_id', 'date_of_response', 'other_issues',
        'signup_name', 'signup_email', 'signup_phone',
        'volunteer_deliver', 'volunteer_vote', 'volunteer_poster', 'volunteer_other', 'volunteer_text'];
    public $table = 'surveys_responses';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function address(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function answers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\ResponseAnswer');
    }

    public function issues(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\ResponseIssue');
    }

    public function parties(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\ResponseParty');
    }

    public function survey(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Survey');
    }

    public function voter(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Voter');
    }

    public function ward(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Ward');
    }

    public function csvRow(): array
    {
        $wardName    = $this->ward->name;
        $addressName = '';
        $voterName   = $this->voter->name ?? '';
        if ($this->address)
        {
            $addressName = $this->address->name.' '.$this->address->road->name;
        }

        $array          = [$this->id, $wardName, $addressName, $voterName];
        $parties        = GenerallyVotes::toArray();
        $convertIssues  = [
            0 => 'Not Important',
            1 => 'Fairly Important',
            2 => 'Very Important',
        ];
        $convertParties = [
            0 => 'Never',
            1 => 'Possibly',
            2 => 'Definitely',
        ];
        $partyResponses = $this->parties()->pluck('answer', 'enum_political_party')->toArray();

        foreach ($this->survey->issues as $issue)
        {
            $array[] = $convertIssues[$this->issues()->where('issue_id', $issue->id)->firstOrNew()->answer] ?? '';
        }

        foreach (array_keys($parties) as $partyKey)
        {
            $key = $partyResponses[$partyKey] ?? '';

            if ($key == '')
            {
                $array[] = '';
            } else {
                $array[]  = $convertParties[$key] ?? '';
            }
        }

        foreach ($this->survey->questions as $question)
        {
            $option   = $this->answers()->where('question_id', $question->id)->first();
            if ($option)
            {
                $optionID = $option->option_id;
                $array[]  = QuestionOption::find($optionID)->name;
            } else {
                $array[]  = '';
            }
        }

        return $array;
    }

    public function getAddressNameAttribute(): string
    {
        if ($this->address)
        {
            return $this->address->name. ' '.$this->address->road->name;
        }

        return '';
    }

    public function getVoterNameAttribute(): string
    {
        if ($this->voter)
        {
            return $this->voter->name;
        }

        return '';
    }

    public function getWardNameAttribute(): string
    {
        if ($this->ward)
        {
            return $this->ward->name;
        }

        return '';
    }

    public function hasSignedUp(): bool
    {
        return $this->signup_email <> '' || $this->signup_phone <> '';
    }

    public function hasVolunteered(): bool
    {
        return $this->volunteer_deliver + $this->volunteer_poster + $this->volunteer_other > 0;
    }

    public function issuesArray(): array
    {
        $return     = [];
        $convertArr = [
            0 => 'Not Interested',
            1 => 'Fairly Interested',
            2 => 'Very Interested',
        ];

        foreach ($this->survey->issues as $issue)
        {
            $answerText = 'N/A';
            $issueResponse = $this->issues()->where('issue_id', $issue->id)->first();
            if ($issueResponse)
            {
                $answerText = $convertArr[$issueResponse->answer];
            }

            $return[$issue->name] = $answerText;
        }

        return $return;
    }

    public function partiesArray(): array
    {
        $return     = [];
        $partyArr   = GenerallyVotes::toArray();
        $convertArr = [
            0 => 'Not Interested',
            1 => 'Fairly Interested',
            2 => 'Very Interested',
        ];

        foreach ($this->parties as $party)
        {
            $return[$partyArr[$party->enum_political_party]] = $convertArr[$party->answer];
        }

        return $return;
    }
}
