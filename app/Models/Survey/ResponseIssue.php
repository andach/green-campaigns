<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ResponseIssue extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['issue_id', 'response_id', 'answer'];
    public $table = 'surveys_responses_issues';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
