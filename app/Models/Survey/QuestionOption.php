<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class QuestionOption extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['question_id', 'name'];
    public $table = 'surveys_questions_options';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
