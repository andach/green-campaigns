<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ResponseAnswer extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['question_id', 'response_id', 'option_id', 'additional_text'];
    public $table = 'surveys_responses_answers';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function option(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Survey\QuestionOption', 'option_id');
    }

    public function question(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Survey\Question');
    }
}
