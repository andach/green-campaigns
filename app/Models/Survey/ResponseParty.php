<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ResponseParty extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['response_id', 'enum_political_party', 'answer'];
    public $table = 'surveys_responses_parties';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
