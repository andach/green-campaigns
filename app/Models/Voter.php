<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Voter extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['address_id', 'council_id', 'road_id', 'route_id', 'ward_id', 'forename', 'surname', 'is_postal',
        'elector_number_full', 'elector_number_prefix', 'elector_number', 'elector_number_suffix', 'elector_markers',
        'notes'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function address(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function canvasses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Canvass');
    }

    public function council(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Council');
    }

    public function elections(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Election', 'link_elections_voters', 'voter_id', 'election_id');
    }

    public function ward(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Ward');
    }

    public static function csvColumns(): array
    {
        return [
            'ID',
            'Council',
            'Ward',
            'Address',
            'Forename',
            'Surname',
            'Postal Voter?',
            'Voted in last Election?',
            'Party Voted For (Council)',
            'Green Vote Chance (Council)',
            'Party Voted For (MP)',
            'Green Vote Chance (MP)',
            'Notes (Address)',
            'Notes (Voter)',
        ];
    }

    public function csvRow(?int $electionID = null): array
    {
        return [
            $this->id,
            $this->council->name,
            $this->ward->name,
            $this->address->name.' '.$this->address->road->name,
            $this->forename,
            $this->surname,
            $this->is_postal,
            $this->votedInElection($electionID),
            $this->council_enum_generally_votes_latest,
            $this->council_enum_vote_green_likelihood_latest,
            $this->parl_enum_generally_votes_latest,
            $this->parl_enum_vote_green_likelihood_latest,
            $this->address->notes,
            $this->notes,
        ];
    }

    public function getNameAttribute(): string
    {
        return $this->forename.' '.$this->surname;
    }

    public function updateVotingIntention(?string $enumCouncilParty, ?string $enumCouncilGreen, ?string $enumParlParty, ?string $enumParlGreen, ?string $notes): bool
    {
        if (!$enumCouncilParty && !$enumCouncilGreen && !$enumParlParty && !$enumParlGreen)
        {
            return false;
        }

        $this->canvasses()->create([
            'address_id' => $this->address_id,
            'council_id' => $this->council_id,
            'ward_id'    => $this->ward_id,
            'user_id'    => Auth::id(),
            'council_enum_generally_votes'          => $enumCouncilParty,
            'council_enum_vote_green_likelihood'    => $enumCouncilGreen,
            'parl_enum_generally_votes'             => $enumParlParty,
            'parl_enum_vote_green_likelihood'       => $enumParlGreen,
        ]);

        $this->council_enum_generally_votes_latest          = $enumCouncilParty;
        $this->council_enum_vote_green_likelihood_latest    = $enumCouncilGreen;
        $this->parl_enum_generally_votes_latest             = $enumParlParty;
        $this->parl_enum_vote_green_likelihood_latest       = $enumParlGreen;
        $this->notes = $notes;
        $this->save();

        return true;
    }

    public function votedInElection(int $electionID): int
    {
        if ($this->elections()->where('election_id', $electionID)->first())
        {
            return 1;
        }

        return 0;
    }
}
