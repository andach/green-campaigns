<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class CaseworkComment extends Model
{
    use HasFactory;
    use LogsActivity;

    public $fillable = ['casework_id', 'user_id', 'attachment', 'comment'];
    public $table = 'caseworks_comments';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function casework(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Casework');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }
}
