<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Address extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['council_id', 'road_id', 'route_id', 'ward_id', 'name', 'postcode', 'enum_generally_votes_latest', 'enum_vote_green_likelihood_latest', 'notes'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function councils(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Council', 'link_addresses_councils', 'address_id', 'council_id');
    }

    public function road(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Road');
    }

    public function route(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Route');
    }

    public function surveyResponses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey\Response');
    }

    public function voters(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Voter');
    }

    public function ward(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Ward');
    }

    // Function returns the address objects for the (up to) four houses that are next to the current one.
    // This bases off the name, assumes it's a number and goes from there. It won't work for houses that are named.
    // For house 140 on a particular road, this will give the address objects for 138, 139, 141 and 142.
    public function getNextCanvassHouses(): array
    {
        $return = [];

        if (!is_numeric($this->name))
        {
            return $return;
        }

        $return[-4] = Address::where('road_id', $this->road_id)->where('name', $this->name - 4)->first();
        $return[-3] = Address::where('road_id', $this->road_id)->where('name', $this->name - 3)->first();
        $return[-2] = Address::where('road_id', $this->road_id)->where('name', $this->name - 2)->first();
        $return[-1] = Address::where('road_id', $this->road_id)->where('name', $this->name - 1)->first();
        $return[1]  = Address::where('road_id', $this->road_id)->where('name', $this->name + 1)->first();
        $return[2]  = Address::where('road_id', $this->road_id)->where('name', $this->name + 2)->first();
        $return[3]  = Address::where('road_id', $this->road_id)->where('name', $this->name + 3)->first();
        $return[4]  = Address::where('road_id', $this->road_id)->where('name', $this->name + 4)->first();

        return $return;
    }

    public function getNumberOfVotersAttribute(): int
    {
        return $this->voters()->count();
    }
}
