<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Road extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['council_id', 'route_id', 'ward_id', 'name'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function addresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Address');
    }

    public function ward(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Ward');
    }

    // Returns a string of all delivery routes for addresses in this road.
    public function getDeliveryRouteNamesAttribute(): string
    {
        $routeIDs = \DB::select('SELECT route_id FROM addresses WHERE road_id = '.$this->id.' GROUP BY route_id');
        $array = [];

        foreach ($routeIDs as $object)
        {
            if ($object->route_id > 0)
            {
                $array[] = $object->route_id;
            }
        }

        return implode(', ', Route::whereIn('id', $array)->pluck('name')->toArray());
    }

    public function getNumberOfHousesAttribute(): int
    {
        $countHouses = \DB::select('SELECT count(id) as number_of_houses FROM addresses WHERE road_id = "'.$this->id.'"');

        return $countHouses[0]->number_of_houses;
    }

    public function getNumberOfVotersAttribute(): int
    {
        $addressIDs = $this->addresses->pluck('id')->toArray();
        $countVoters = \DB::select('SELECT count(id) as number_of_voters FROM voters WHERE address_id IN ('.implode(',', $addressIDs).')');

        return $countVoters[0]->number_of_voters;
    }
}
