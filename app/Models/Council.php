<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Council extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['name'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function addresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Address', 'link_addresses_councils', 'council_id', 'address_id');
    }

    public function activists(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Activist');
    }

    public function elections(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Election');
    }

    public function events(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Event');
    }

    public function surveys(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Survey');
    }

    public function wards(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Ward');
    }

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\User', 'link_councils_users', 'council_id', 'user_id');
    }

    /*
        Takes a line from the electoral register, splits out the postcode, address and other details for entry into the addresses table
    */
    private function getRoadNameAndNumberFromElectoralRegister(array $line): array
    {
        // Most addresses will have the Address1 as something like "123 Fake Street".
        // In this case, we can easily check for the first character being a number, and the split by first space.
        if (is_numeric(substr($line['Address1'], 0, 1)))
        {
            $arr = explode(' ', $line['Address1'], 2);

            if (isset($arr[1]))
            {
                // Then Address 1 will be something like "123 Fake Street".
                return [
                    'number' => trim($arr[0]),
                    'name'   => trim($arr[1]),
                ];
            } else {
                // Then Address1 will be something like "123" and Address2 will be "Fake Street".
                return [
                    'number' => trim($line['Address1']),
                    'name'   => trim($line['Address2']),
                ];
            }
        } else {
            // This might be something like a nursing home, a named cottage, or it could be flats. Check the Address2 and see if it starts with a number.
            // If so, it's almost certainly flats.
            if (is_numeric(substr($line['Address2'], 0, 1)))
            {
                $arr = explode(' ', $line['Address2'], 2);

                return [
                    'number' => trim($line['Address1'].', '.$arr[0]),
                    'name'   => trim($arr[1]),
                ];
            } else {
                // If neither Address1 or Address2 starts with a number, just use them as name and number.
                return [
                    'number' => trim($line['Address1']),
                    'name'   => trim($line['Address2']),
                ];
            }
        }
    }

    /*
        Returns an array of format:

        array:188 [▼
            "Aspen Drive" => 1
            "Avondale Road" => 2
            "Beeches Avenue" => 3
            ...
        ]
    */
    private function getRoadsAndIDsFromParsedElectoralRegister(array $array): array
    {
        // Pluck all the road names and create them.
        $roadNames = [];
        foreach ($array as $line)
        {
            $roadNames[$line['parsed_address_road']] = $line['ward_id'];
        }

        $roads = [];
        foreach ($roadNames as $road => $wardID)
        {
            $roads[$road] = Road::firstOrCreate([
                'council_id' => $this->id,
                'name'       => $road,
                'ward_id'    => $wardID,
            ])->id;
        }

        return $roads;
    }

    /*
        This is to be used with the upload, and requires an array directly from a CSV upload of the electoral register, which will look like the below.

        16 => array:14 [▼
            "Elector Number Prefix" => "SP1"
            "Elector Number" => "24"
            "Elector Number Suffix" => "0"
            "Elector Markers" => ""
            "Elector DOB" => ""
            "Elector Surname" => "Smith"
            "Elector Forename" => "John"
            "PostCode" => "DE21 1XX"
            "Address1" => "1 Test Road"
            "Address2" => "Spondon"
            "Address3" => "Derby"
            "Address4" => "DE21 1XX"
            "Address5" => ""
            "Address6" => ""
        ]
    */
    public function uploadUsers(array $array, int $wardID): bool
    {
        foreach ($array as $id => $line)
        {
            $temp = $this->getRoadNameAndNumberFromElectoralRegister($line);
            $array[$id]['parsed_address_number'] = $temp['number'];
            $array[$id]['parsed_address_road']   = $temp['name'];
            $array[$id]['ward_prefix']           = substr($line['﻿Elector Number Prefix'], 0, 2);
            $array[$id]['ward_id']               = $wardID;
        }

        $roadsArray     = $this->getRoadsAndIDsFromParsedElectoralRegister($array);

        foreach ($array as $id => $line)
        {
            $array[$id]['address_id'] = Address::firstOrCreate([
                'council_id' => $this->id,
                'ward_id'    => $wardID,
                'road_id'    => $roadsArray[$line['parsed_address_road']],
                'name'       => $line['parsed_address_number'],
                'postcode'   => strtoupper(str_replace(' ', '', $line['PostCode'])),
            ])->id;
        }

        // Note that voters are soft deleted, so this doesn't remove anything from the database.
        Voter::where('ward_id', $wardID)->delete();
        $upsertArray = [];

        foreach ($array as $id => $line)
        {
            $suffix = '';
            if ($line['Elector Number Suffix'])
            {
                $suffix = '/'.trim($line['Elector Number Suffix']);
            }

            $upsertArray[] = [
                'address_id' => $line['address_id'],
                'council_id' => $this->id,
                'road_id'    => $roadsArray[$line['parsed_address_road']],
                'ward_id'    => $wardID,
                'forename'   => trim($line['Elector Forename']),
                'surname'    => trim($line['Elector Surname']),
                'deleted_at' => null,

                'elector_number_full'   => trim($line['﻿Elector Number Prefix']).'-'.trim($line['Elector Number']).$suffix,
                'elector_number_prefix' => trim($line['﻿Elector Number Prefix']),
                'elector_number'        => (int) $line['Elector Number'],
                'elector_number_suffix' => (int) $line['Elector Number Suffix'],
                'elector_markers'       => trim($line['Elector Markers']),
            ];
        }

        $results   = [];

        foreach (array_chunk($upsertArray, 500) as $upsertArrayChunk)
        {
            $results[] = Voter::upsert($upsertArrayChunk, [
                'address_id',
                'council_id',
                'road_id',
                'ward_id',
                'forename',
                'surname',
                'elector_number_prefix',
                'elector_number',
                'elector_number_suffix',
                'elector_markers'
            ], ['deleted_at']);
        }

        return true;
    }
}
