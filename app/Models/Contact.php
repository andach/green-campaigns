<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Contact extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['activist_id', 'event_id', 'user_id', 'date_of_contact', 'enum_contact_type', 'managed_to_contact', 'notes'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function activist(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Activist');
    }

    public function event(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getEventNameAttribute(): string
    {
        if ($this->event)
        {
            return $this->event->name;
        }

        return '';
    }

    public function getFormattedManagedToContactAttribute(): string
    {
        if ($this->managed_to_contact)
        {
            return 'Yes';
        }

        return 'N';
    }
}
