<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Route extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    public $fillable = ['ward_id', 'name'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function addresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Address');
    }

    public function ward(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Ward');
    }

    public function getNumberOfHousesAttribute(): int
    {
        return $this->addresses()->count();
    }

    public function getNumberOfVotersAttribute(): int
    {
        $addressIDs = $this->addresses->pluck('id')->toArray();

        if (count($addressIDs) == 0)
        {
            return 0;
        }

        $countVoters = \DB::select('SELECT count(id) as number_of_voters FROM voters WHERE address_id IN ('.implode(',', $addressIDs).')');

        return $countVoters[0]->number_of_voters;
    }
}
