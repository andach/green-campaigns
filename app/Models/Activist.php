<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Activist extends Model
{
    use HasFactory;
    use LogsActivity;
    use PrimaryKeyUuid;
    use SoftDeletes;

    public $fillable = ['id', 'council_id', 'forename', 'surname', 'email', 'mobile_number', 'phone_number', 'do_not_phone',
        'address', 'city', 'postcode', 'council_ward', 'local_authority', 'constituency',
        'is_member', 'membership_start_date', 'membership_end_date', 'membership_type',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    public function contacts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function council(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Council');
    }

    public function events(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Event', 'link_activists_events', 'activist_id', 'event_id');
    }

    public static function csvHeaders(): array
    {
        return [
            'Forename',
            'Surname',
            'Email',
            'Mobile Number',
            'Phone Number',
            'Do Not Phone',
            'Address',
            'City',
            'Postcode',
            'Council Ward',
            'Local Authority',
            'Constituency',
            'Is a Member?',
            'Membership Start Date',
            'Membership End Date',
            'Membership Type',
        ];
    }

    public function csvRow(): array
    {
        return [
            $this->forename,
            $this->Surname,
            $this->email,
            $this->mobile_number,
            $this->phone_number,
            $this->do_not_phone,
            $this->address,
            $this->city,
            $this->postcode,
            $this->council_ward,
            $this->local_authority,
            $this->constituency,
            $this->is_a_member,
            $this->membership_start_date,
            $this->membership_end_date,
            $this->membership_type,
        ];
    }

    public static function formatPhoneNumber(string $number): string
    {
        $number       = str_replace(' ', '' , str_replace('+', '', $number));
        $elevenDigits = '';

        switch (strlen($number))
        {
            case 10:
                $elevenDigits = '0'.$number;
                break;

            case 11:
                $elevenDigits = $number;
                break;

            case 12:
                $elevenDigits = '0'.substr($number, 2, 10);
                break;
        }

        switch (substr($elevenDigits, 0, 2))
        {
            case '01':
                if (substr($elevenDigits, 3, 1) === "1")
                {
                    return substr($elevenDigits, 0, 4).' '.substr($elevenDigits, 4, 7);
                } else {
                    return substr($elevenDigits, 0, 5).' '.substr($elevenDigits, 5, 6);
                }
                break;

            case '02':
            case '03':
                return substr($elevenDigits, 0, 3).' '.substr($elevenDigits, 3, 4).' '.substr($elevenDigits, 7, 4);
                break;

            case '07':
                return substr($elevenDigits, 0, 5).' '.substr($elevenDigits, 5, 6);
                break;
        }

        return $elevenDigits;
    }

    public function getFormattedCanPhoneAttribute(): string
    {
        if ($this->do_not_phone)
        {
            return 'N';
        }

        return 'Yes';
    }

    public function getFormattedIsMemberAttribute(): string
    {
        if ($this->is_member)
        {
            return 'Yes';
        }

        return 'N';
    }

    public function getFormattedPhoneAttribute(): string
    {
        $returnArr = [];

        if ($this->phone_number)
        {
            $returnArr[] = 'H: '.$this->phone_number;
        }

        if ($this->mobile_number)
        {
            $returnArr[] = 'M: '.$this->mobile_number;
        }

        return implode('<br />', $returnArr);
    }

    public function getFormattedPhoneAndEmailAttribute(): string
    {
        $returnArr = [];

        if ($this->phone_number)
        {
            $returnArr[] = 'H: '.$this->phone_number;
        }

        if ($this->mobile_number)
        {
            $returnArr[] = 'M: '.$this->mobile_number;
        }

        if ($this->email)
        {
            $returnArr[] = 'E: '.$this->email;
        }

        return implode('<br />', $returnArr);
    }

    public function getNameAttribute(): string
    {
        return $this->forename.' '.$this->surname;
    }

    public static function parseHomeNumber(array $row): string
    {
        $numbers = array_filter([
            $row['can2_phone'],
            $row['Home_Phone'],
            $row['Phone Number'],
        ]);

        foreach ($numbers as $number)
        {
            $formattedNumber = self::formatPhoneNumber($number);

            if (substr($formattedNumber, 1, 1) != "7")
            {
                return $formattedNumber;
            }
        }

        return '';
    }

    public static function parseMobileNumber(array $row): string
    {
        $numbers = array_filter([
            $row['can2_phone'],
            $row['Home_Phone'],
            $row['Phone Number'],
        ]);

        foreach ($numbers as $number)
        {
            $formattedNumber = self::formatPhoneNumber($number);

            if (substr($formattedNumber, 1, 1) == "7")
            {
                return $formattedNumber;
            }
        }

        return '';
    }
}
