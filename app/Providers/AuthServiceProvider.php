<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Casework' => 'App\Policies\CaseworkPolicy',
        'App\Models\Council'  => 'App\Policies\CouncilPolicy',
        'App\Models\Road'     => 'App\Policies\RoadPolicy',
        'App\Models\Ward'     => 'App\Policies\WardPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
