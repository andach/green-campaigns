<?php

namespace App\Enums;

use App\Enums\Enum;

class GenerallyVotes extends Enum
{
    private const CON    = 'Tories';
    private const LAB    = 'Labour';
    private const LD     = 'Lib Dems';
    private const GREEN  = 'Greens';
    private const SNP    = 'SNP';
    private const CYMRU  = 'Plaid Cymru';
    private const BREX   = 'Brexit/UKIP/Reform';
    private const ANTI   = 'Anti';
    private const LOCAL  = 'Other Local Party';
    private const OTHER  = 'Other National Party';
    private const NOVOTE = 'Does Not Vote';
}
