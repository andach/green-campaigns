<?php

namespace App\Enums;

use App\Enums\Enum;

class VoteGreenLikelihoodColor extends Enum
{
    private const ZERO  = '#333333';
    private const ONE   = '#6AB023';
    private const TWO   = '#59931D';
    private const THREE = '#477517';
    private const FOUR  = '#29450D';
    private const FIVE  = '#152307';
}
