<?php

namespace App\Enums;

use App\Enums\Enum;

class GenerallyVotesColor extends Enum
{
    private const CON    = '#00558c';
    private const LAB    = '#f93822';
    private const LD     = '#fdbb30';
    private const GREEN  = '#6AB023';
    private const SNP    = '#FFFF00';
    private const CYMRU  = '#E48920';
    private const BREX   = '#772583';
    private const ANTI   = '#000000';
    private const LOCAL  = '#d1ccbd';
    private const OTHER  = '#d1ccbd';
    private const NOVOTE = '#d1ccbd';
}
