<?php

namespace App\Enums;

use App\Enums\Enum;

class Questions extends Enum
{
    private const TEXT   = 'Full free-text answer';
    private const IMPORT = 'List of issues, rated as not/fairly/very important';
    private const VOTES  = 'List of political parties, with vote chance listed as definitely/maybe/no chance';
    private const CHOICE = 'Multiple choice question, with custom responses';
    private const CHECK  = 'Checkbox';
}
