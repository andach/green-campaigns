<?php

namespace App\Enums;

use App\Enums\Enum;

class ContactTypes extends Enum
{
    private const PHONE  = 'Phone';
    private const TEXT   = 'Text/WhatsApp/etc.';
    private const LETTER = 'Letter';
    private const EMAIL  = 'Email';
    private const PERSON = 'In-Person';
    private const OTHER  = 'Other';
}
