<?php

namespace App\Enums;

use App\Enums\Enum;

class VoteGreenLikelihood extends Enum
{
    private const ZERO  = 'N/A - Does Not Vote';
    private const ONE   = '1 - Strong Support';
    private const TWO   = '2 - Probable Green Vote';
    private const THREE = '3 - Possible Green Vote';
    private const FOUR  = '4 - Weak Opposition';
    private const FIVE  = '5 - Strong Opposition';
}
