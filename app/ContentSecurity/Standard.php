<?php

namespace App\ContentSecurity;

use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic;

class Standard extends Basic
{
    public function configure(): void
    {
        parent::configure();

        $this->addDirective(Directive::SCRIPT, 'cdnjs.cloudflare.com');
        $this->addDirective(Directive::STYLE, 'cdnjs.cloudflare.com');
        $this->addDirective(Directive::STYLE, 'fonts.googleapis.com');
        $this->addDirective(Directive::FONT, 'fonts.gstatic.com');
    }
}
