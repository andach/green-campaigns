<?php

namespace Tests\Unit;

use App\Models\Casework;
use App\Models\User;
use Tests\TestCase;

class CaseworkTest extends TestCase
{
    public function testCreate()
    {
        $response = $this->get('/casework/create/1');
        $response->assertStatus(302);
        $response->assertDontSee('Create Casework');

        $this->be(User::find(1));
        $response = $this->get('/casework/create/1');
        $response->assertStatus(200);
        $response->assertSee('Create Casework');

        $this->be(User::find(2));
        $response = $this->get('/casework/create/1');
        $response->assertStatus(200);
        $response->assertSee('Create Casework');

        $this->be(User::find(4));
        $response = $this->get('/casework/create/1');
        $response->assertStatus(403);
        $response->assertDontSee('Create Casework');
    }

    public function testCreatePost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/casework/create/1', [
            'title'   => 'Test Title',
            'comment' => 'Test Comment',
        ]);
        $response->assertSee('The new item of casework has been created');
        $casework = Casework::orderBy('id', 'desc')->first();
        $this->assertEquals(1, $casework->address_id);
        $this->assertEquals(1, $casework->council_id);
        $this->assertEquals(1, $casework->voter_id);
        $this->assertEquals(1, $casework->ward_id);
        $this->assertEquals(2, $casework->user_id);
        $this->assertEquals('Test Title', $casework->name);
        $this->assertEquals(null, $casework->is_closed);
        $this->assertEquals('Test Comment', $casework->comments()->first()->comment);
        $this->assertEquals(2, $casework->comments()->first()->user_id);

        $response = $this->followingRedirects()->post('/casework/create/1', [
            'title'   => 'Test Title',
        ]);
        $response->assertSee('The comment field is required');

        $response = $this->followingRedirects()->post('/casework/create/1', [
            'comment'   => 'Test Comment',
        ]);
        $response->assertSee('The title field is required');
    }

    public function testIndex()
    {
        $response = $this->get('/casework');
        $response->assertStatus(302);
        $response->assertDontSee('All Casework');

        $this->be(User::find(1));
        $response = $this->get('/casework');
        $response->assertStatus(200);
        $response->assertSee('All Casework');
        $response->assertSee('You do not have any casework.');

        $this->be(User::find(2));
        $response = $this->get('/casework');
        $response->assertStatus(200);
        $response->assertSee('All Casework');
        $response->assertSee('Problem with the bins');

        $this->be(User::find(4));
        $response = $this->get('/casework');
        $response->assertStatus(200);
        $response->assertSee('All Casework');
        $response->assertSee('You do not have any casework.');
    }

    public function testShow()
    {
        $response = $this->get('/casework/1');
        $response->assertStatus(302);
        $response->assertDontSee('Casework: Problem with the bins');

        $this->be(User::find(1));
        $response = $this->get('/casework/1');
        $response->assertStatus(200);
        $response->assertSee('Casework: Problem with the bins');

        $this->be(User::find(2));
        $response = $this->get('/casework/1');
        $response->assertStatus(200);
        $response->assertSee('Casework: Problem with the bins');

        $this->be(User::find(4));
        $response = $this->get('/casework/1');
        $response->assertStatus(403);
        $response->assertDontSee('Casework: Problem with the bins');
    }

    public function testShowPost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/casework/1', ['reopen' => 1]);
        $response->assertSee('The casework item has been reopened');
        $casework = Casework::find(1);
        $this->assertEquals($casework->is_closed, 0);

        $response = $this->followingRedirects()->post('/casework/2', ['comment' => 'Test']);
        $response->assertSee('Casework has been updated');
        $casework = Casework::find(2);
        $this->assertEquals($casework->is_closed, 0);
        $this->assertEquals($casework->comments()->orderBy('id', 'desc')->first()->comment, 'Test');

        $response = $this->followingRedirects()->post('/casework/2', ['comment' => 'Test2', 'is_closed' => 1]);
        $response->assertSee('Casework has been updated');
        $casework = Casework::find(2);
        $this->assertEquals($casework->is_closed, 1);
        $this->assertEquals($casework->comments()->orderBy('id', 'desc')->first()->comment, 'Test2');
    }
}
