<?php

namespace Tests\Unit;

use App\Models\Council;
use App\Models\Ward;
use App\Models\User;
use Tests\TestCase;

class CouncilTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/council');
        $response->assertStatus(302);
        $response->assertDontSee('Manage Councils');

        $this->be(User::find(1));
        $response = $this->get('/council');
        $response->assertStatus(200);
        $response->assertSee('Manage Councils');
        $response->assertSee('Derby City Council');
        $response->assertSee('Abbey [AB]');

        $this->be(User::find(2));
        $response = $this->get('/council');
        $response->assertStatus(403);
        $response->assertDontSee('Manage Councils');
    }

    public function testIndexPost()
    {
        $response = $this->followingRedirects()->post('/council', [
            'new_council' => 'Test Council',
        ]);
        $response->assertDontSee('The new council has been added');
        
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/council', [
            'new_council' => 'Test Council',
        ]);
        $response->assertStatus(403);
        $response->assertDontSee('The new council has been added');
        
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/council', [
            'new_council' => 'Test Council',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The new council has been added');
        $council = Council::orderBy('id', 'desc')->first();
        $this->assertEquals($council->name, 'Test Council');
        
        $response = $this->followingRedirects()->post('/council', [
            'council_id'        => '1',
            'new_ward'          => 'Test Ward',
            'electoral_prefix'  => 'TEST',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The new ward has been added');
        $ward = Ward::orderBy('id', 'desc')->first();
        $this->assertEquals($ward->council_id, '1');
        $this->assertEquals($ward->name, 'Test Ward');
        $this->assertEquals($ward->electoral_prefix, 'TEST');
        
        $response = $this->followingRedirects()->post('/council', [
            'delete_ward' => [18],
        ]);
        $response->assertStatus(200);
        $response->assertSee('The selected ward(s) have been deleted');
        $response = $this->get('/council');
        $response->assertDontSee('Nottingham Ward [AA]');
        
        $response = $this->followingRedirects()->post('/council', [
            'delete_council' => [2],
        ]);
        $response->assertStatus(200);
        $response->assertSee('The selected council(s) have been deleted');
        $response = $this->get('/council');
        $response->assertDontSee('Nottingham City Council');
        $response->assertDontSee('Nottingham Ward [AA]');
    }
}
