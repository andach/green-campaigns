<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\User;
use App\Models\Voter;
use Tests\TestCase;

class AddressTest extends TestCase
{
    public function testAddressCreate()
    {
        $response = $this->get('/address/create');
        $response->assertStatus(302);
        $response->assertDontSee('Create Address');

        $this->be(User::find(1));
        $response = $this->get('/address/create');
        $response->assertStatus(200);
        $response->assertSee('Create Address');
        $response->assertSee('Spondon');
        $response->assertSee('Nottingham Ward');

        $this->be(User::find(2));
        $response = $this->get('/address/create');
        $response->assertStatus(200);
        $response->assertSee('Create Address');
        $response->assertSee('Spondon');
        $response->assertDontSee('Nottingham Ward');

        $this->be(User::find(4));
        $response = $this->get('/address/create');
        $response->assertStatus(200);
        $response->assertSee('Create Address');
        $response->assertDontSee('Spondon');
        $response->assertSee('Nottingham Ward');
    }

    public function testAddressCreatePost()
    {
        $this->be(User::find(2));

        $response = $this->followingRedirects()->post('/address/create', [
            'ward_id' => 1,
            'road_id' => 1,
            'name'    => '1',
            'notes'   => 'NOTES HERE'
        ]);
        $response->assertStatus(200);
        $response->assertSee('This address already exists');

        $response = $this->followingRedirects()->post('/address/create', [
            'ward_id' => 1,
            'road_id' => 1,
            'name'    => '1A',
            'notes'   => 'NOTES HERE'
        ]);
        $response->assertStatus(200);
        $response->assertSee('The address has been created');
    }

    public function testAddressEdit()
    {
        $this->be(User::find(2));

        $response = $this->followingRedirects()->post('/address/1/edit', [
            'delete_voters' => [1],
            'forename'      => 'Tom',
            'surname'       => 'Thomas',
            'is_postal'     => 1,
            'notes'         => 'NOTES HERE'
        ]);
        $response->assertStatus(200);
        $response->assertSee('The voters at this address have been updated');

        $voter = Voter::orderBy('id', 'desc')->first();
        $this->assertEquals('Tom', $voter->forename);
        $this->assertEquals('Thomas', $voter->surname);
        $this->assertEquals(1, $voter->is_postal);
        $this->assertEquals('NOTES HERE', $voter->notes);
    }
}
