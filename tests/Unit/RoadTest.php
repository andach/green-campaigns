<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class RoadTest extends TestCase
{
    public function testShow()
    {
        $response = $this->get('/road/1');
        $response->assertStatus(302);
        $response->assertDontSee('Test Road');

        $this->be(User::find(1));
        $response = $this->get('/road/1');
        $response->assertStatus(200);
        $response->assertSee('Test Road');

        $this->be(User::find(2));
        $response = $this->get('/road/1');
        $response->assertStatus(200);
        $response->assertSee('Test Road');

        $this->be(User::find(4));
        $response = $this->get('/road/1');
        $response->assertStatus(403);
        $response->assertDontSee('Test Road');
    }
}
