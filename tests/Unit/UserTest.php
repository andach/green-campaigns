<?php

namespace Tests\Unit;

use App\Models\User;
use Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testCreate()
    {
        $response = $this->get('/user/create');
        $response->assertStatus(302);
        $response->assertDontSee('Create User');

        $this->be(User::find(1));
        $response = $this->get('/user/create');
        $response->assertStatus(200);
        $response->assertSee('Create User');

        $this->be(User::find(2));
        $response = $this->get('/user/create');
        $response->assertStatus(403);
        $response->assertDontSee('Create User');
    }

    public function testCreatePost()
    {
        $response = $this->post('/user/create');
        $response->assertStatus(302);
        $response->assertDontSee('Create User');

        $this->be(User::find(1));
        $response = $this->post('/user/create');
        $response->assertStatus(302);

        $response = $this->followingRedirects()->post('/user/create', []);
        $response->assertStatus(200);
        $response->assertSee('The name field is required');
        $response->assertSee('The email field is required');
        $response->assertSee('The password field is required');

        $response = $this->followingRedirects()->post('/user/create', [
            'name'      => 'a',
            'email'     => 'a',
            'password'  => 'a',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The email must be a valid email address');
        $response->assertSee('The password must be at least 8 characters');
        $response->assertSee('The password confirmation does not match');

        $response = $this->followingRedirects()->post('/user/create', [
            'name'      => 'a',
            'email'     => 'admin@demo.com',
            'password'  => 'a',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The email has already been taken');

        $response = $this->followingRedirects()->post('/user/create', [
            'name'                  => 'a',
            'email'                 => 'a@aol.com',
            'password'              => '12345678',
            'password_confirmation' => '12345678',
            'council_ids'           => [1, 2],
        ]);
        $response->assertStatus(200);
        $response->assertSee('The user has been created');
        $user = User::orderBy('id', 'desc')->first();
        $this->assertEquals($user->name, 'a');
        $this->assertEquals($user->email, 'a@aol.com');

        $response = $this->followingRedirects()->post('/user/create', [
            'name'                  => 'b',
            'email'                 => 'b@aol.com',
            'password'              => '12345678',
            'password_confirmation' => '12345678',
            'council_ids'           => [],
        ]);
        $response->assertStatus(200);
        $response->assertSee('The user has been created');

        $this->be(User::find(2));
        $response = $this->post('/user/create');
        $response->assertStatus(403);
        $response->assertDontSee('Create User');
    }

    public function testEdit()
    {
        $response = $this->get('/user/1/edit');
        $response->assertStatus(302);
        $response->assertDontSee('Update User - Admin');

        $this->be(User::find(1));
        $response = $this->get('/user/1/edit');
        $response->assertStatus(200);
        $response->assertSee('Update User - Admin');

        $this->be(User::find(2));
        $response = $this->get('/user/1/edit');
        $response->assertStatus(403);
        $response->assertDontSee('Update User - Admin');
    }

    public function testEditPost()
    {
        $response = $this->post('/user/1/edit');
        $response->assertStatus(302);
        $response->assertDontSee('Update User - Admin');

        $this->be(User::find(1));
        $response = $this->post('/user/1/edit');
        $response->assertStatus(302);

        $response = $this->followingRedirects()->post('/user/1/edit', []);
        $response->assertStatus(200);
        $response->assertSee('The name field is required');
        $response->assertSee('The email field is required');

        $response = $this->followingRedirects()->post('/user/1/edit', [
            'name'      => 'a',
            'email'     => 'a',
            'password'  => 'a',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The email must be a valid email address');
        $response->assertSee('The password must be at least 8 characters');
        $response->assertSee('The password confirmation does not match');

        $response = $this->followingRedirects()->post('/user/1/edit', [
            'name'                  => 'a',
            'email'                 => 'a@aol.com',
            'password'              => '12345678',
            'password_confirmation' => '12345678',
            'council_ids'           => [1, 2],
        ]);
        $response->assertStatus(200);
        $response->assertSee('The user has been edited');
        $user = User::find(1);
        $this->assertEquals($user->name, 'a');
        $this->assertEquals($user->email, 'a@aol.com');
        $this->assertTrue(Hash::check('12345678', $user->password));

        $response = $this->followingRedirects()->post('/user/1/edit', [
            'name'                  => 'b',
            'email'                 => 'b@aol.com',
            'password'              => '12345678',
            'password_confirmation' => '12345678',
            'council_ids'           => [],
        ]);
        $response->assertStatus(200);
        $response->assertSee('The user has been edited');

        $this->be(User::find(2));
        $response = $this->post('/user/1/edit');
        $response->assertStatus(403);
        $response->assertDontSee('Update User - Admin');
    }

    public function testIndex()
    {
        $response = $this->get('/user');
        $response->assertStatus(302);
        $response->assertDontSee('All Users');

        $this->be(User::find(1));
        $response = $this->get('/user');
        $response->assertStatus(200);
        $response->assertSee('All Users');
        $response->assertSee('admin@demo.com');
        $response->assertSee('derbyadmin@demo.com');
        $response->assertSee('derbyuser@demo.com');
        $response->assertSee('nottingham@demo.com');

        $this->be(User::find(2));
        $response = $this->get('/user');
        $response->assertStatus(403);
        $response->assertDontSee('All Users');
    }

    public function testMyAccount()
    {
        $response = $this->get('/user/my-account');
        $response->assertStatus(302);
        $response->assertDontSee('My Account');

        $this->be(User::find(1));
        $response = $this->get('/user/my-account');
        $response->assertStatus(200);
        $response->assertSee('My Account - Admin');

        $this->be(User::find(2));
        $response = $this->get('/user/my-account');
        $response->assertStatus(200);
        $response->assertSee('My Account - Derby Admin');
    }

    public function testMyAccountPost()
    {
        $response = $this->post('/user/my-account');
        $response->assertStatus(302);
        $response->assertDontSee('Update User - Admin');

        $this->be(User::find(2));
        $response = $this->post('/user/my-account');
        $response->assertStatus(302);

        $response = $this->followingRedirects()->post('/user/my-account', []);
        $response->assertStatus(200);
        $response->assertSee('The name field is required');
        $response->assertSee('The email field is required');

        $response = $this->followingRedirects()->post('/user/my-account', [
            'name'      => 'a',
            'email'     => 'a',
            'password'  => 'a',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The email must be a valid email address');
        $response->assertSee('The password must be at least 8 characters');
        $response->assertSee('The password confirmation does not match');

        $response = $this->followingRedirects()->post('/user/my-account', [
            'name'                  => 'a',
            'email'                 => 'a@aol.com',
            'password'              => '12345678',
            'password_confirmation' => '12345678',
        ]);
        $response->assertStatus(200);
        $response->assertSee('Your account has been updated');

        $user = User::find(2);
        $this->assertEquals($user->name, 'a');
        $this->assertEquals($user->email, 'a@aol.com');
        $this->assertTrue(Hash::check('12345678', $user->password));
    }
}
