<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class WardTest extends TestCase
{
    public function testExport()
    {
        $response = $this->get('/ward/1/export');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->get('/ward/1/export');
        $response->assertStatus(200);

        $this->be(User::find(2));
        $response = $this->get('/ward/1/export');
        $response->assertStatus(200);
    }

    public function testIndex()
    {
        $response = $this->get('/ward');
        $response->assertStatus(302);
        $response->assertDontSee('All Wards');

        $this->be(User::find(1));
        $response = $this->get('/ward');
        $response->assertStatus(200);
        $response->assertSee('All Wards');
        $response->assertSee('Abbey');
        $response->assertSee('Nottingham Ward');

        $this->be(User::find(2));
        $response = $this->get('/ward');
        $response->assertStatus(200);
        $response->assertSee('All Wards');
        $response->assertSee('Abbey');
        $response->assertDontSee('Nottingham Ward');

        $this->be(User::find(4));
        $response = $this->get('/ward');
        $response->assertStatus(200);
        $response->assertSee('All Wards');
        $response->assertDontSee('Abbey');
        $response->assertSee('Nottingham Ward');
    }

    public function testShow()
    {
        $response = $this->get('/ward/1');
        $response->assertStatus(302);
        $response->assertDontSee('Abbey Ward');

        $this->be(User::find(1));
        $response = $this->get('/ward/1');
        $response->assertStatus(200);
        $response->assertSee('Abbey Ward');
        $response = $this->get('/ward/18');
        $response->assertStatus(200);
        $response->assertSee('Nottingham Ward Ward');

        $this->be(User::find(2));
        $response = $this->get('/ward/1');
        $response->assertStatus(200);
        $response->assertSee('Abbey Ward');
        $response = $this->get('/ward/18');
        $response->assertStatus(403);
    }
}
