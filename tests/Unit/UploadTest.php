<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Election;
use App\Models\Voter;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Storage;
use Tests\TestCase;

class UploadTest extends TestCase
{
    public function testAbsentRegister()
    {
        $response = $this->get('/upload/absent-register');
        $response->assertStatus(302);
        $response->assertDontSee('Upload New Absent Register');

        $this->be(User::find(1));
        $response = $this->get('/upload/absent-register');
        $response->assertStatus(200);
        $response->assertSee('Upload New Absent Register');

        $this->be(User::find(2));
        $response = $this->get('/upload/absent-register');
        $response->assertStatus(403);
        $response->assertDontSee('Upload New Absent Register');

        $this->be(User::find(4));
        $response = $this->get('/upload/absent-register');
        $response->assertStatus(403);
        $response->assertDontSee('Upload New Absent Register');
    }

    public function testAbsentRegisterPost()
    {
        Storage::fake('uploads');

        $row1    = 'AB1-1';
        $row2    = 'AB1-2';
        $content = implode("\n", [$row1, $row2]);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/upload/absent-register', [
            'council_id' => 1,
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);
        $response->assertSee('The absent register information has been updated');

        $this->assertEquals([1, 2], voter::where('is_postal', 1)->pluck('id')->toArray());

        $row1    = 'AB1-1';
        $row2    = 'AB1-3';
        $content = implode("\n", [$row1, $row2]);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/upload/absent-register', [
            'council_id' => 1,
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);
        $response->assertSee('The absent register information has been updated');

        $this->assertEquals([1, 3], voter::where('is_postal', 1)->pluck('id')->toArray());
    }

    public function testElectoralRegister()
    {
        $response = $this->get('/upload/electoral-register');
        $response->assertStatus(302);
        $response->assertDontSee('Upload New Electoral Register');

        $this->be(User::find(1));
        $response = $this->get('/upload/electoral-register');
        $response->assertStatus(200);
        $response->assertSee('Upload New Electoral Register');

        $this->be(User::find(2));
        $response = $this->get('/upload/electoral-register');
        $response->assertStatus(403);
        $response->assertDontSee('Upload New Electoral Register');

        $this->be(User::find(4));
        $response = $this->get('/upload/electoral-register');
        $response->assertStatus(403);
        $response->assertDontSee('Upload New Electoral Register');
    }

    public function testElectoralRegisterPost()
    {
        Storage::fake('uploads');

        $header  = '﻿Elector Number Prefix,Elector Number,Elector Number Suffix,Elector Markers,Elector DOB,Elector Surname,Elector Forename,PostCode,Address1,Address2,Address3,Address4,Address5,Address6';
        $row1    = 'AL1,1,0,,,Smith,John,DE1 2AB,1 The Street,Allestree,Derby,DE1 2AB,,';
        $row2    = 'AL1,2,0,,,Smith,Jane,de1 2AB,1 The Street,Allestree,Derby,DE1 2AB,,';
        $row3    = 'AL1,3,0,,,Firstname,Lastname,DE12AB,2 The Street,Allestree,Derby,DE1 2AB,,';
        $row4    = 'AL1,4,0,,,Old,Person,DE1 2AB,Nursing Home,The Street,Allestree,Derby,DE21 7SG,';
        $content = implode("\n", [$header, $row1, $row2, $row3, $row4]);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/upload/electoral-register', [
            'council_id' => 1,
            'ward_id'    => 2,
            'date_of_electoral_register' => '2021-01-01',
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);
        $response->assertSee('The electoral register has been updated');

        $address = Address::find(4);
        $this->assertEquals(1, $address->council_id);
        $this->assertEquals(2, $address->road_id);
        $this->assertEquals(2, $address->ward_id);
        $this->assertEquals(1, $address->name);
        $this->assertEquals('DE12AB', $address->postcode);

        $address = Address::find(5);
        $this->assertEquals(1, $address->council_id);
        $this->assertEquals(2, $address->road_id);
        $this->assertEquals(2, $address->ward_id);
        $this->assertEquals(2, $address->name);
        $this->assertEquals('DE12AB', $address->postcode);

        $address = Address::find(6);
        $this->assertEquals(1, $address->council_id);
        $this->assertEquals(2, $address->road_id);
        $this->assertEquals(2, $address->ward_id);
        $this->assertEquals('Nursing Home', $address->name);
        $this->assertEquals('DE12AB', $address->postcode);

        $voter = Voter::find(7);
        $this->assertEquals(4, $voter->address_id);
        $this->assertEquals(1, $voter->council_id);
        $this->assertEquals(2, $voter->road_id);
        $this->assertEquals(2, $voter->ward_id);
        $this->assertEquals('John', $voter->forename);
        $this->assertEquals('Smith', $voter->surname);
        $this->assertEquals('AL1-1', $voter->elector_number_full);

        $voter = Voter::find(10);
        $this->assertEquals(6, $voter->address_id);
        $this->assertEquals(1, $voter->council_id);
        $this->assertEquals(2, $voter->road_id);
        $this->assertEquals(2, $voter->ward_id);
        $this->assertEquals('Person', $voter->forename);
        $this->assertEquals('Old', $voter->surname);
        $this->assertEquals('AL1-4', $voter->elector_number_full);

        // Now test it fails (correctly) if the headers are wrong.
        $header  = '﻿Elector Number Prefix,Elector Number,Elector Number Suffix,Elector Markers,Elector DOB,Elector Surname,Elector Forename,PostCode,Address1,Address2,Address3,Address4,Address5';
        $row1    = 'AL1,1,0,,,Smith,John,DE1 2AB,1 The Street,Allestree,Derby,DE1 2AB,';
        $row2    = 'AL1,2,0,,,Smith,Jane,DE1 2AB,1 The Street,Allestree,Derby,DE1 2AB,';
        $row3    = 'AL1,3,0,,,Firstname,Lastname,DE1 2AB,2 The Street,Allestree,Derby,DE1 2AB,';
        $row4    = 'AL1,4,0,,,Old,Person,DE1 2AB,Nursing Home,The Street,Allestree,Derby,DE21 7SG';
        $content = implode("\n", [$header, $row1, $row2, $row3, $row4]);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/upload/electoral-register', [
            'council_id' => 1,
            'ward_id'    => 2,
            'date_of_electoral_register' => '2021-01-01',
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);
        $response->assertDontSee('The electoral register has been updated');
        $response->assertSee('The headers of the CSV you provided weren\'t right. Nothing has been uploaded or updated');
    }

    public function testElectoralRegisterPost2()
    {
        // Now test it fails (correctly) if the Address1 data is ill-formatted.
        $header  = '﻿Elector Number Prefix,Elector Number,Elector Number Suffix,Elector Markers,Elector DOB,Elector Surname,Elector Forename,PostCode,Address1,Address2,Address3,Address4,Address5,Address6';
        $row1    = 'AL1,1,0,,,Smith,John,DE1 2AB,1,Allestree,Derby,DE1 2AB,,';
        $row2    = 'AL1,2,0,,,Smith,Jane,DE1 2AB,Floopington,Allestree,Derby,DE1 2AB,,';
        $row3    = 'AL1,3,0,,,Firstname,Lastname,DE1 2AB,2 The Street,Allestree,Derby,DE1 2AB,,';
        $row4    = 'AL1,4,0,,,Old,Person,DE1 2AB,Nursing Home,The Street,Allestree,Derby,DE21 7SG,';
        $content = implode("\n", [$header, $row1, $row2, $row3, $row4]);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/upload/electoral-register', [
            'council_id' => 1,
            'ward_id'    => 2,
            'date_of_electoral_register' => '2021-01-01',
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);
        $response->assertSee('The electoral register has been updated');

        $address = Address::find(4);
        $this->assertEquals(1, $address->council_id);
        $this->assertEquals(2, $address->road_id);
        $this->assertEquals(2, $address->ward_id);
        $this->assertEquals(1, $address->name);

        $address = Address::find(5);
        $this->assertEquals(1, $address->council_id);
        $this->assertEquals(2, $address->road_id);
        $this->assertEquals(2, $address->ward_id);
        $this->assertEquals('Floopington', $address->name);
    }

    public function testIndex()
    {
        $response = $this->get('/upload');
        $response->assertStatus(302);
        $response->assertDontSee('Upload Data to System');

        $this->be(User::find(1));
        $response = $this->get('/upload');
        $response->assertStatus(200);
        $response->assertSee('Upload Data to System');

        $this->be(User::find(2));
        $response = $this->get('/upload');
        $response->assertStatus(403);
        $response->assertDontSee('Upload Data to System');

        $this->be(User::find(4));
        $response = $this->get('/upload');
        $response->assertStatus(403);
        $response->assertDontSee('Upload Data to System');
    }

    public function testMarkedRegister()
    {
        $response = $this->get('/upload/marked-register');
        $response->assertStatus(302);
        $response->assertDontSee('Upload New Marked Register');

        $this->be(User::find(1));
        $response = $this->get('/upload/marked-register');
        $response->assertStatus(200);
        $response->assertSee('Upload New Marked Register');

        $this->be(User::find(2));
        $response = $this->get('/upload/marked-register');
        $response->assertStatus(403);
        $response->assertDontSee('Upload New Marked Register');

        $this->be(User::find(4));
        $response = $this->get('/upload/marked-register');
        $response->assertStatus(403);
        $response->assertDontSee('Upload New Marked Register');
    }

    public function testMarkedRegisterPost()
    {
        Storage::fake('uploads');

        $row1    = 'AB1-1';
        $row2    = 'AB1-2';
        $row3    = 'AB1-5';
        $row4    = 'AB1-6';
        $content = implode("\n", [$row1, $row2, $row3, $row4]);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/upload/marked-register', [
            'election_id' => 1,
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);
        $response->assertSee('The marked register information has been updated');

        $election = Election::find(1);
        $this->assertEquals([1, 2, 5, 6], $election->voters()->pluck('voters.id')->toArray());
    }
}
