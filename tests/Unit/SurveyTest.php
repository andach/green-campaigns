<?php

namespace Tests\Unit;

use App\Models\Survey;
use App\Models\User;
use Tests\TestCase;

class SurveyTest extends TestCase
{
    public function testCreate()
    {
        $response = $this->get('/survey/create');
        $response->assertStatus(302);
        $response->assertDontSee('Create Survey');

        $this->be(User::find(1));
        $response = $this->get('/survey/create');
        $response->assertStatus(200);
        $response->assertSee('Create Survey');
        $response->assertSee('Derby City Council');

        $this->be(User::find(2));
        $response = $this->get('/survey/create');
        $response->assertStatus(200);
        $response->assertSee('Create Survey');
        $response->assertSee('Derby City Council');

        $this->be(User::find(4));
        $response = $this->get('/survey/create');
        $response->assertStatus(200);
        $response->assertSee('Create Survey');
        $response->assertDontSee('Derby City Council');
    }

    public function testCreatePost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/survey/create', [
            'name'       => 'Test Survey',
            'council_id' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertSee('The survey has been created successfully. Now add some questions to it.');

        $survey = Survey::find(2);
        $this->assertEquals('Test Survey', $survey->name);
        $this->assertEquals(1, $survey->council_id);

        $response = $this->get('/survey/2');
        $response->assertStatus(200);
        $response->assertSee('Test Survey');

        $response = $this->get('/survey/2/edit');
        $response->assertStatus(200);
        $response->assertSee('How Important');
    }

    function testEdit()
    {
        $this->be(User::find(2));
        $response = $this->get('/survey/1/edit');
        $response->assertStatus(200);
        $response->assertDontSee('How Important');
    }

    function testExport()
    {
        $this->be(User::find(2));
        $response = $this->get('/survey/1/export');
        $response->assertStatus(200);
    }

    function testResponsePost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/survey/1/response', [
            'date_of_response' => '2022-01-01',
            'ward_id' => 1,
            'address_id' => 0,
            'voter_id' => 0,
        ]);
        $response->assertStatus(200);
        $response->assertSee('The survey response has been logged');
    }
}
