<?php

namespace Tests\Unit;

use App\Models\Canvass;
use App\Models\User;
use Tests\TestCase;

class CanvassTest extends TestCase
{
    public function testAddress()
    {
        $response = $this->get('/canvass/address/1');
        $response->assertStatus(302);
        $response->assertDontSee('Canvassing - 1 Test Road');

        $this->be(User::find(1));
        $response = $this->get('/canvass/address/1');
        $response->assertStatus(200);
        $response->assertSee('Canvassing - 1 Test Road');

        $this->be(User::find(2));
        $response = $this->get('/canvass/address/1');
        $response->assertStatus(200);
        $response->assertSee('Canvassing - 1 Test Road');

        $this->be(User::find(4));
        $response = $this->get('/canvass/address/1');
        $response->assertStatus(403);
        $response->assertDontSee('Canvassing - 1 Test Road');
    }

    public function testAddressPost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/canvass/address/2', [
            'voter_ids'                             => [2, 3],
            'council_enum_generally_votes'          => [2 => 'GREEN', 3 => 'CON'],
            'council_enum_vote_green_likelihood'    => [2 => 'FIVE',  3 => 'ZERO'],
            'parl_enum_generally_votes'             => [2 => 'LAB', 3 => null],
            'parl_enum_vote_green_likelihood'       => [2 => 'THREE',  3 => 'THREE'],
            'next_address'                          => 1,
            'notes'                                 => 'ADD NOTES',
        ]);
        $response->assertStatus(200);
        $response->assertSee('Canvassing - 1 Test Road');
        $response->assertSee('Canvass data has been updated');

        $canvass = Canvass::where('voter_id', 2)->orderBy('id', 'desc')->first();
        $this->assertEquals($canvass->council_enum_generally_votes, 'GREEN');
        $this->assertEquals($canvass->council_enum_vote_green_likelihood, 'FIVE');
        $this->assertEquals($canvass->parl_enum_generally_votes, 'LAB');
        $this->assertEquals($canvass->parl_enum_vote_green_likelihood, 'THREE');
        $this->assertEquals($canvass->user_id, 2);

        $response = $this->get('/road/1');
        $response->assertStatus(200);
        $response->assertSee('ADD NOTES');
    }

    public function testPrint()
    {
        $response = $this->get('/canvass/print/1');
        $response->assertStatus(302);
        $response->assertDontSee('Print Canvass Sheets - Test Road');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(1));
        $response = $this->get('/canvass/print/1');
        $response->assertStatus(200);
        $response->assertSee('Print Canvass Sheets - Test Road');
        $response->assertSee('Adam Adams');
        $response->assertSee('Charles Clarke');

        $this->be(User::find(2));
        $response = $this->get('/canvass/print/1');
        $response->assertStatus(200);
        $response->assertSee('Print Canvass Sheets - Test Road');
        $response->assertSee('Adam Adams');
        $response->assertSee('Charles Clarke');

        $this->be(User::find(4));
        $response = $this->get('/canvass/print/1');
        $response->assertStatus(403);
        $response->assertDontSee('Print Canvass Sheets - Test Road');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');
    }

    public function testPrintAbsentMarked()
    {
        $response = $this->get('/canvass/print/1/absent-marked');
        $response->assertStatus(302);
        $response->assertDontSee('Print Canvass Sheets - Test Road (Absent & Marked Voters Only)');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(1));
        $response = $this->get('/canvass/print/1/absent-marked');
        $response->assertStatus(200);
        $response->assertSee('Print Canvass Sheets - Test Road (Absent & Marked Voters Only)');
        $response->assertSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(2));
        $response = $this->get('/canvass/print/1/absent-marked');
        $response->assertStatus(200);
        $response->assertSee('Print Canvass Sheets - Test Road (Absent & Marked Voters Only)');
        $response->assertSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(4));
        $response = $this->get('/canvass/print/1/absent-marked');
        $response->assertStatus(403);
        $response->assertDontSee('Print Canvass Sheets - Test Road (Absent & Marked Voters Only)');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');
    }

    public function testRoad()
    {
        $response = $this->get('/canvass/road/1');
        $response->assertStatus(302);
        $response->assertDontSee('Canvass - Test Road');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(1));
        $response = $this->get('/canvass/road/1');
        $response->assertStatus(200);
        $response->assertSee('Canvass - Test Road');
        $response->assertSee('Adam Adams');
        $response->assertSee('Charles Clarke');

        $this->be(User::find(2));
        $response = $this->get('/canvass/road/1');
        $response->assertStatus(200);
        $response->assertSee('Canvass - Test Road');
        $response->assertSee('Adam Adams');
        $response->assertSee('Charles Clarke');

        $this->be(User::find(4));
        $response = $this->get('/canvass/road/1');
        $response->assertStatus(403);
        $response->assertDontSee('Canvass - Test Road');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');
    }

    public function testRoadAbsentMarked()
    {
        $response = $this->get('/canvass/road/1/absent-marked');
        $response->assertStatus(302);
        $response->assertDontSee('Canvass - Test Road (Absent & Marked Voters Only)');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(1));
        $response = $this->get('/canvass/road/1/absent-marked');
        $response->assertStatus(200);
        $response->assertSee('Canvass - Test Road (Absent & Marked Voters Only)');
        $response->assertSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(2));
        $response = $this->get('/canvass/road/1/absent-marked');
        $response->assertStatus(200);
        $response->assertSee('Canvass - Test Road (Absent & Marked Voters Only)');
        $response->assertSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');

        $this->be(User::find(4));
        $response = $this->get('/canvass/road/1/absent-marked');
        $response->assertStatus(403);
        $response->assertDontSee('Canvass - Test Road (Absent & Marked Voters Only)');
        $response->assertDontSee('Adam Adams');
        $response->assertDontSee('Charles Clarke');
    }

    public function testRoadPost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/canvass/road/1', [
            'voter_ids'                             => [2, 3],
            'council_enum_generally_votes'          => [2 => 'GREEN', 3 => 'CON'],
            'council_enum_vote_green_likelihood'    => [2 => 'FIVE',  3 => 'ZERO'],
            'parl_enum_generally_votes'             => [2 => 'LAB', 3 => 'LAB'],
            'parl_enum_vote_green_likelihood'       => [2 => 'THREE',  3 => 'THREE'],
        ]);
        $response->assertStatus(200);
        $response->assertSee('Canvass - Test Road');
        $response->assertSee('The canvassing session has been updated');

        $canvass = Canvass::where('voter_id', 2)->orderBy('id', 'desc')->first();
        $this->assertEquals($canvass->council_enum_generally_votes, 'GREEN');
        $this->assertEquals($canvass->council_enum_vote_green_likelihood, 'FIVE');
        $this->assertEquals($canvass->parl_enum_generally_votes, 'LAB');
        $this->assertEquals($canvass->parl_enum_vote_green_likelihood, 'THREE');
        $this->assertEquals($canvass->user_id, 2);
    }
}
