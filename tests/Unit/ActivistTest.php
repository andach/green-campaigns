<?php

namespace Tests\Unit;

use App\Models\Activist;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Storage;
use Tests\TestCase;

class ActivistTest extends TestCase
{
    public function testActivistContactDelete()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/activist/contact-delete', [
            'contact_id' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertSee('The contact record has been deleted');

        $this->be(User::find(4));
        $response = $this->followingRedirects()->post('/activist/contact-delete', [
            'contact_id' => 2,
        ]);
        $response->assertStatus(403);
        $response->assertDontSee('The contact record has been deleted');
    }

//    public function testActivistExport()
//    {
//        $response = $this->get('/activist/1/export');
//        $response->assertStatus(302);
//
//        $this->be(User::find(1));
//        $response = $this->get('/activist/1/export');
//        $response->assertStatus(200);
//
//        $this->be(User::find(2));
//        $response = $this->get('/activist/1/export');
//        $response->assertStatus(200);
//
//        $this->be(User::find(4));
//        $response = $this->get('/activist/1/export');
//        $response->assertStatus(302);
//    }

    public function testActivistIndex()
    {
        $response = $this->get('/activist');
        $response->assertStatus(302);
        $response->assertDontSee('All Activists');

        $this->be(User::find(1));
        $response = $this->get('/activist');
        $response->assertStatus(200);
        $response->assertSee('All Activists');
        $response->assertSee('Derby City Council');
        $response->assertSee('Nottingham City Council');

        $this->be(User::find(2));
        $response = $this->get('/activist');
        $response->assertStatus(200);
        $response->assertSee('All Activists');
        $response->assertSee('Derby City Council');
        $response->assertDontSee('Nottingham City Council');

        $this->be(User::find(4));
        $response = $this->get('/activist');
        $response->assertStatus(200);
        $response->assertSee('All Activists');
        $response->assertDontSee('Derby City Council');
        $response->assertSee('Nottingham City Council');
    }

    public function testActivistShow()
    {
        $this->be(User::find(2));
        $response = $this->get('/activist/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa');
        $response->assertStatus(200);
        $response->assertSee('zoople@gmail.com');
        $response->assertSee('No response');

        $this->be(User::find(4));
        $response = $this->get('/activist/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa');
        $response->assertStatus(403);
        $response->assertDontSee('zoople@gmail.com');
        $response->assertDontSee('No response');
    }

    public function testActivistShowPost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/activist/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', [
            'date_of_contact'    => '2020-01-01',
            'enum_contact_type'  => 'PHONE',
            'managed_to_contact' => true,
            'notes'              => 'They are going to try to make it to the event',
            'is_coming'          => true,
            'is_attending'       => 0,
            'event_id'           => 1,
        ]);
        $response->assertStatus(200);
        $response->assertSee('Information added successfully');

        $contact = Contact::find(3);
        $this->assertEquals('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', $contact->activist_id);
        $this->assertEquals('They are going to try to make it to the event', $contact->notes);
    }

    public function testActivistUpload()
    {
        Storage::fake('uploads');

        $header  = 'first_name,last_name,email,can2_phone,can2_user_address,can2_user_city,can2_user_state,zip_code,uuid,can2_subscription_status,can2_sms_status,Birthdate,do_not_phone,Full Time Student,Home_Phone,Local_Authority,Local_Party,Member,Membership_End_Date,Membership_Join_Date,Membership_Period_Start,Membership_Status,Membership_Type,Mobile_Phone,Parliamentary_Constituency,Phone Number,Regional_Party,Ward';
        $row1    = 'A,AA,a@a.com,7123456789,Flat 1,London,England,AB12DE,c2fbb390-602e-4ddb-9b2e-7ef320155ab1,subscribed,unsubscribed,,TRUE,,2012345678,Derbyshire Dales District Council,Derbyshire Green Party,,13/03/2017,13/03/2016,13/03/2016,,Student Membership,,Derbyshire Dales,,East Midlands Green Party,Ward 1';
        $row2    = 'B,BB,b@b.com,,Flat 2,Bristol,England,AB12DE,f6bf7f8e-ac48-4bdb-bbf3-f0080fd95af7,subscribed,unsubscribed,,,,3012345678,Derbyshire Dales District Council,Derbyshire Green Party,,,,,,,,Derbyshire Dales,7999654321,East Midlands Green Party,Ward 2';
        $row3    = 'C,CC,c@c.com,,Flat 3,Northampton,England,AB12DE,b0e080f6-25bf-4f73-b80b-12f5a26dbf19,subscribed,unsubscribed,,,,1012345678,Derby City Council,Derbyshire Green Party,,,,,,,,Derby North,,East Midlands Green Party,Ward 3';
        $row4    = 'D,DD,d@d.com,,Flat 4,Burton,England,AB12DE,5f893802-c432-489e-8223-36cd9b50d305,subscribed,unsubscribed,16/03/1964,,,1604123456,Chesterfield Borough Council,Derbyshire Green Party,Yes,01/12/2021,12/01/2014,01/12/2020,Current,Reduced Rate Membership,,Chesterfield,,East Midlands Green Party,Ward 4';
        $row5    = 'E,EE,e@e.com,441555123456,Flat 5,Glasgow,England,AB12DE,a4e9c15b-2691-4112-8a73-898d142398f7,subscribed,unsubscribed,01/01/1973,,,7952123456,Derbyshire Dales District Council,Derbyshire Green Party,No,11/06/2021,11/06/2019,11/06/2020,Cancelled,Standard Annual Membership,,Derbyshire Dales,,East Midlands Green Party,Ward 5';
        $content = implode("\n", [$header, $row1, $row2, $row3, $row4, $row5]);

        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/activist/upload', [
            'council_id' => 1,
            'file'       => UploadedFile::fake()->createWithContent('file.csv', $content),
        ]);
        $response->assertStatus(200);

        $activist = Activist::find('c2fbb390-602e-4ddb-9b2e-7ef320155ab1');
        $this->assertEquals('A', $activist->forename);
        $this->assertEquals('AA', $activist->surname);
        $this->assertEquals('07123 456789', $activist->mobile_number);
        $this->assertEquals('020 1234 5678', $activist->phone_number);

        $activist = Activist::find('f6bf7f8e-ac48-4bdb-bbf3-f0080fd95af7');
        $this->assertEquals('B', $activist->forename);
        $this->assertEquals('BB', $activist->surname);
        $this->assertEquals('07999 654321', $activist->mobile_number);
        $this->assertEquals('030 1234 5678', $activist->phone_number);

        $activist = Activist::find('b0e080f6-25bf-4f73-b80b-12f5a26dbf19');
        $this->assertEquals('C', $activist->forename);
        $this->assertEquals('CC', $activist->surname);
        $this->assertEquals('', $activist->mobile_number);
        $this->assertEquals('0101 2345678', $activist->phone_number);
        $this->assertEquals(null, $activist->membership_start_date);
        $this->assertEquals(null, $activist->membership_end_date);

        $activist = Activist::find('a4e9c15b-2691-4112-8a73-898d142398f7');
        $this->assertEquals('E', $activist->forename);
        $this->assertEquals('EE', $activist->surname);
        $this->assertEquals('07952 123456', $activist->mobile_number);
        $this->assertEquals('01555 123456', $activist->phone_number);
        $this->assertEquals('Flat 5', $activist->address);
        $this->assertEquals('Glasgow', $activist->city);
        $this->assertEquals('AB12DE', $activist->postcode);
        $this->assertEquals('Ward 5', $activist->council_ward);
        $this->assertEquals('Derbyshire Dales District Council', $activist->local_authority);
        $this->assertEquals('Derbyshire Dales', $activist->constituency);
        $this->assertEquals(1, $activist->is_member);
        $this->assertEquals('2019-06-11', $activist->membership_start_date);
        $this->assertEquals('2021-06-11', $activist->membership_end_date);
        $this->assertEquals('Standard Annual Membership', $activist->membership_type);
    }
}
