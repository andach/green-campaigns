<?php

namespace Tests\Unit;

use App\Models\Contact;
use App\Models\Event;
use App\Models\User;
use Tests\TestCase;

class EventTest extends TestCase
{
    public function testEventCreate()
    {
        $response = $this->get('/event/create');
        $response->assertStatus(302);
        $response->assertDontSee('Create Event');

        $this->be(User::find(1));
        $response = $this->get('/event/create');
        $response->assertStatus(200);
        $response->assertSee('Create Event');
        $response->assertSee('Derby City Council');
        $response->assertSee('Nottingham City Council');

        $this->be(User::find(2));
        $response = $this->get('/event/create');
        $response->assertStatus(200);
        $response->assertSee('Create Event');
        $response->assertSee('Derby City Council');
        $response->assertDontSee('Nottingham City Council');

        $this->be(User::find(4));
        $response = $this->get('/event/create');
        $response->assertStatus(200);
        $response->assertSee('Create Event');
        $response->assertDontSee('Derby City Council');
        $response->assertSee('Nottingham City Council');
    }

    public function testEventCreatePost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/event/create', [
            'council_id' => 1,
            'date_of_event' => '2021-02-02',
            'name' => 'Test Event',
            'notes' => 'Test Event Notes',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The event has been created');

        $event = Event::orderBy('id', 'desc')->first();
        $this->assertEquals(1, $event->council_id);
        $this->assertEquals('2021-02-02', $event->date_of_event);
        $this->assertEquals('Test Event', $event->name);
        $this->assertEquals('Test Event Notes', $event->notes);
    }

    public function testEventContact()
    {
        $response = $this->get('/event/1/contact/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa');
        $response->assertStatus(302);
        $response->assertDontSee('Contact Zoople Zooplington about Derby City Event');

        $this->be(User::find(1));
        $response = $this->get('/event/1/contact/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa');
        $response->assertStatus(200);
        $response->assertSee('Contact Zoople Zooplington about Derby City Event');

        $this->be(User::find(2));
        $response = $this->get('/event/1/contact/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa');
        $response->assertStatus(200);
        $response->assertSee('Contact Zoople Zooplington about Derby City Event');

        $this->be(User::find(4));
        $response = $this->get('/event/1/contact/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa');
        $response->assertStatus(403);
        $response->assertDontSee('Contact Zoople Zooplington about Derby City Event');
    }

    public function testEventContactNotComing()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/event/1/contact/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/not-coming', [
            'not_coming' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertSee('This person has been marked as not attending');

        $event = Event::find(1);
        $this->assertEquals(0, $event->number_attending);
    }

    public function testEventContactPost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/event/1/contact/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', [
            'date_of_contact'   => '2020-01-01',
            'enum_contact_type' => 'PHONE',
            'managed_to_contact' => true,
            'notes' => 'They are going to try to make it to the event',
            'is_coming' => true,
            'is_attending' => 0,
        ]);
        $response->assertStatus(200);
        $response->assertSee('Information added successfully');

        $contact = Contact::orderBy('id', 'desc')->first();
        $this->assertEquals('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', $contact->activist_id);
        $this->assertEquals(1, $contact->event_id);
        $this->assertEquals(2, $contact->user_id);
        $this->assertEquals('2020-01-01', $contact->date_of_contact);
        $this->assertEquals('PHONE', $contact->enum_contact_type);
        $this->assertEquals(true, $contact->managed_to_contact);
        $this->assertEquals('They are going to try to make it to the event', $contact->notes);

        $event = Event::find(1);
        $this->assertEquals(1, $event->activists()->count());

        $activists = $event->activists()->where('activists.id', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa')->withPivot('is_attending')->first();
        $this->assertEquals(0, $activists->pivot_is_attending);
    }

    public function testEventEdit()
    {
        $response = $this->get('/event/1/edit');
        $response->assertStatus(302);
        $response->assertDontSee('Edit Event - Derby City Event');

        $this->be(User::find(1));
        $response = $this->get('/event/1/edit');
        $response->assertStatus(200);
        $response->assertSee('Edit Event - Derby City Event');

        $this->be(User::find(2));
        $response = $this->get('/event/1/edit');
        $response->assertStatus(200);
        $response->assertSee('Edit Event - Derby City Event');

        $this->be(User::find(4));
        $response = $this->get('/event/1/edit');
        $response->assertStatus(403);
        $response->assertDontSee('Edit Event - Derby City Event');
    }

    public function testEventEditPost()
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/event/1/edit', [
            'date_of_event' => '2021-02-02',
            'name' => 'Test Event',
            'notes' => 'Test Event Notes',
        ]);
        $response->assertSee('The event has been edited');

        $event = Event::find(1);
        $this->assertEquals(1, $event->council_id);
        $this->assertEquals('2021-02-02', $event->date_of_event);
        $this->assertEquals('Test Event', $event->name);
        $this->assertEquals('Test Event Notes', $event->notes);
    }

    public function testEventIndex()
    {
        $response = $this->get('/event');
        $response->assertStatus(302);
        $response->assertDontSee('All Events');

        $this->be(User::find(1));
        $response = $this->get('/event');
        $response->assertStatus(200);
        $response->assertSee('All Events');
        $response->assertSee('Derby City Event');
        $response->assertSee('Nottingham City Event');

        $this->be(User::find(2));
        $response = $this->get('/event');
        $response->assertStatus(200);
        $response->assertSee('All Events');
        $response->assertSee('Derby City Event');
        $response->assertDontSee('Nottingham City Event');

        $this->be(User::find(4));
        $response = $this->get('/event');
        $response->assertStatus(200);
        $response->assertSee('All Events');
        $response->assertDontSee('Derby City Event');
        $response->assertSee('Nottingham City Event');
    }

    public function testEventShow()
    {
        $response = $this->get('/event/1');
        $response->assertStatus(302);
        $response->assertDontSee('Derby City Event');

        $this->be(User::find(1));
        $response = $this->get('/event/1');
        $response->assertStatus(200);
        $response->assertSee('Derby City Event');

        $this->be(User::find(2));
        $response = $this->get('/event/1');
        $response->assertStatus(200);
        $response->assertSee('Derby City Event');

        $this->be(User::find(4));
        $response = $this->get('/event/1');
        $response->assertStatus(403);
        $response->assertDontSee('Derby City Event');
    }
}
