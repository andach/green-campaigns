@extends('template')

@section('title')
    {{ $road->name }}
@endsection

@section('infobox')
    Click on the house number of any house in this road to start canvassing. Alternatively, you can access
    <a href="{{ route('canvass.road', $road->id) }}">printable canvass sheets</a> for the entire road.
@endsection

@section('content')

    <div class="card">
        <div class="card-header">All Houses in this Road</div>
        <div class="card-body">
            <p>Part of <a href="{{ route('ward.show', $road->ward_id) }}">{{ $road->ward->name }} Ward</a></p>
            <div class="row row-header">
                <div class="col-3">House Name/Number</div>
                <div class="col-4">Voters</div>
                <div class="col-2">Number of Voters</div>
                <div class="col-3">Route</div>
            </div>

            <div class="row">
                @foreach ($addresses as $address)
                    <div class="col-3"><a href="{{ route('canvass.address', $address->id) }}">{{ $address->name }}</a></div>
                    <div class="col-4">
                        @foreach ($address->voters as $voter)
                            {{ $voter->name }}
                            @if (!$loop->last)
                                <br />
                            @endif
                        @endforeach
                    </div>
                    <div class="col-2">{{ $address->number_of_voters }}</div>
                    <div class="col-3"></div>

                    @if ($address->notes)
                        <div class="col-12">Notes: {{ $address->notes }}</div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
