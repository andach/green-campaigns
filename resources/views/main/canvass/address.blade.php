@extends('template')

@section('title')
    Canvassing - {{ $address->name }} {{ $address->road->name }}
@endsection

@section('content')
    <p><a href="{{ route('address.edit', $address->id) }}">Update Voters at this Address</a></p>

    {{ Form::open(['route' => ['canvass.address-post', $address->id], 'method' => 'post']) }}

    <div class="alert alert-info">
        Use this page with a smartphone/tablet while out walking. Enter voting intention details here, then click the button at the bottom to bring you to
        the next house. This page automatically saves information as soon as you move to the next house. Links to log casework will open in a new tab.
    </div>

    <div class="card">
        <div class="card-header">Canvass this Address</div>
        <div class="card-body">
            <p>This house is on <a href="{{ route('canvass.road', $address->road_id) }}">{{ $address->road->name }}</a>.</p>

            <div class="row row-header">
                <div class="col-4 col-md-4">Voter</div>
                <div class="col-2 col-md-2">Last Vote (Council)</div>
                <div class="col-2 col-md-2">Green Vote Chance (Council)</div>
                <div class="col-2 col-md-2">Last Vote (MP)</div>
                <div class="col-2 col-md-2">Green Vote Chance (MP)</div>
            </div>

            <div class="row">
                @foreach ($address->voters as $voter)
                    {{ Form::hidden('voter_ids[]', $voter->id) }}
                    <div class="col-4 col-md-4">{{ $voter->name }}<br />
                        <span style="font-size: .8em">
                            <a href="{{ route('casework.create', $voter->id) }}" target="_blank">Log Casework</a>

                            @foreach ($voter->elections as $election)
                                <br />Voted: {{ $election->name }}
                            @endforeach
                        </span>
                    </div>

                    <div class="col-2 col-md-2">
                        {{ Form::select('council_enum_generally_votes['.$voter->id.']', $enumGenerallyVotes, null, ['placeholder' => '', 'class' => 'form-control']) }}

                        @if ($voter['council_enum_generally_votes_latest'])
                            <p style="margin-bottom: 0">Prev: {{ $enumGenerallyVotes[$voter['council_enum_generally_votes_latest']] }}</p>
                        @endif
                    </div>
                    <div class="col-2 col-md-2">
                        {{ Form::select('council_enum_vote_green_likelihood['.$voter->id.']', $enumVoteGreenLikelihood, null, ['placeholder' => '', 'class' => 'form-control']) }}

                        @if ($voter['council_enum_vote_green_likelihood_latest'])
                            <p style="margin-bottom: 0">Prev: {{ $enumVoteGreenLikelihood[$voter['council_enum_vote_green_likelihood_latest']] }}</p>
                        @endif
                    </div>
                    <div class="col-2 col-md-2">
                        {{ Form::select('parl_enum_generally_votes['.$voter->id.']', $enumGenerallyVotes, null, ['placeholder' => '', 'class' => 'form-control']) }}

                        @if ($voter['parl_enum_generally_votes_latest'])
                            <p style="margin-bottom: 0">Prev: {{ $enumGenerallyVotes[$voter['parl_enum_generally_votes_latest']] }}</p>
                        @endif
                    </div>
                    <div class="col-2 col-md-2">
                        {{ Form::select('parl_enum_vote_green_likelihood['.$voter->id.']', $enumVoteGreenLikelihood, null, ['placeholder' => '', 'class' => 'form-control']) }}

                        @if ($voter['parl_enum_vote_green_likelihood_latest'])
                            <p style="margin-bottom: 0">Prev: {{ $enumVoteGreenLikelihood[$voter['parl_enum_vote_green_likelihood_latest']] }}</p>
                        @endif
                    </div>

                    @if (!$loop->last)
                        <div class="col-12"><hr /></div>
                    @endif
                @endforeach

                <div class="col-12">&nbsp;</div>

                @if ($mostRecentSurveyResponse)
                    <div class="col-12">
                        Voters at this address have responded to a survey on {{ $mostRecentSurveyResponse->date_of_response}}.
                        <a href="{{ route('survey-response.show', $mostRecentSurveyResponse->id) }}">See their full response</a>
                    </div>
                @endif

                @foreach ($nextAddresses as $nextAddress)
                    @if ($nextAddress)
                        <div class="col-6 col-md-3">
                            <button type="submit" name="next_address" value="{{ $nextAddress->id }}" class="form-control btn btn-success btn-block">Save and go to number {{ $nextAddress->name }}</button>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">Notes for this Address</div>
        <div class="card-body">
            {{ Form::textarea('notes', $address->notes, ['class' => 'form-control', 'rows' => 3]) }}
        </div>
    </div>

    <div class="card">
        <div class="card-header">Manually choose next Address</div>
        <div class="card-body">
            {{ Form::bsSelect('Manually Choose Next House', 'next_address_manual', $allAddresses, null, ['placeholder' => '']) }}
            {{ Form::bsSubmit('Save and go to Manually Chosen House') }}
        </div>
    </div>

    {{ Form::close() }}
@endsection
