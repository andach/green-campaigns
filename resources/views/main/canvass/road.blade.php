@extends('template')

@section('title')
    Canvass - {{ $road->name }}@if ($absentMarked) (Absent &amp; Marked Voters Only) @endif
@endsection

@section('content')
    <p><a href="{{ route('address.create') }}">Add an Address</a></p>
    @if ($absentMarked)
        <p><a href="{{ route('canvass.road', $road->id) }}">View all Voters in this Road</a></p>
    @else
        <p><a href="{{ route('canvass.road-absent-marked', $road->id) }}">View just absent & marked Voters in this Road</a></p>
    @endif

    {{ Form::open(['route' => ['canvass.road-post', $road->id], 'method' => 'post']) }}

    <div class="alert alert-info d-print-none">
        This page is here to enter the data from printed out canvass sheets. You may find that this particular page prints oddly.
        If so, <a href="{{ route('canvass.print', $road->id) }}" target="_blank">click here</a> to access a page specifically designed for printing off,
        or <a href="{{ route('canvass.print-absent-marked', $road->id) }}">this link</a> to see just absent & marked voters.
    </div>

    <div class="card">
        <div class="card-header">Canvass this Road</div>
        <div class="card-body">
            <div class="row row-header">
                <div class="col-2">House Name/Number</div>
                <div class="col-2">Voter</div>
                <div class="col-2">Last Vote (Council)</div>
                <div class="col-2">Green Vote Chance (Council)</div>
                <div class="col-2">Last Vote (MP)</div>
                <div class="col-2">Green Vote Chance (MP)</div>
            </div>

            <div class="row">
                @foreach ($addresses as $address)
                    @foreach ($address->voters as $voter)
                        {{ Form::hidden('voter_ids[]', $voter->id) }}
                        <div class="col-2"><a href="{{ route('canvass.address', $address->id) }}">{{ $address->name }}</a></div>
                        <div class="col-2">{{ $voter->name }}<span class="d-print-none"><br />
                                <a href="{{ route('casework.create', $voter->id) }}">Log Casework</a></span></div>

                        <div class="col-2">
                            {{ Form::select('council_enum_generally_votes['.$voter->id.']', $enumGenerallyVotes, null, ['placeholder' => '', 'class' => 'form-control']) }}

                            @if ($voter['council_enum_generally_votes_latest'])
                                <p style="margin-bottom: 0">Prev: {{ $enumGenerallyVotes[$voter['council_enum_generally_votes_latest']] }}</p>
                            @endif
                        </div>
                        <div class="col-2">
                            {{ Form::select('council_enum_vote_green_likelihood['.$voter->id.']', $enumVoteGreenLikelihood, null, ['placeholder' => '', 'class' => 'form-control']) }}

                            @if ($voter['council_enum_vote_green_likelihood_latest'])
                                <p style="margin-bottom: 0">Prev: {{ $enumVoteGreenLikelihood[$voter['council_enum_vote_green_likelihood_latest']] }}</p>
                            @endif
                        </div>
                        <div class="col-2">
                            {{ Form::select('parl_enum_generally_votes['.$voter->id.']', $enumGenerallyVotes, null, ['placeholder' => '', 'class' => 'form-control']) }}

                            @if ($voter['parl_enum_generally_votes_latest'])
                                <p style="margin-bottom: 0">Prev: {{ $enumGenerallyVotes[$voter['parl_enum_generally_votes_latest']] }}</p>
                            @endif
                        </div>
                        <div class="col-2">
                            {{ Form::select('parl_enum_vote_green_likelihood['.$voter->id.']', $enumVoteGreenLikelihood, null, ['placeholder' => '', 'class' => 'form-control']) }}

                            @if ($voter['parl_enum_vote_green_likelihood_latest'])
                                <p style="margin-bottom: 0">Prev: {{ $enumVoteGreenLikelihood[$voter['parl_enum_vote_green_likelihood_latest']] }}</p>
                            @endif
                        </div>

                        @if (!$loop->parent->last || !$loop->last)
                            <div class="col-12"><hr style="margin: .5rem 0" /></div>
                        @endif
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Record Canvassing Data') }}

    {{ Form::close() }}
@endsection
