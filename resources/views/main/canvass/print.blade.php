@extends('print')

@section('title')
    Print Canvass Sheets - {{ $road->name }}@if ($absentMarked) (Absent &amp; Marked Voters Only) @endif
@endsection

@section('content')
    <div class="row row-header">
        <div class="col-2">House Name/Number</div>
        <div class="col-2">Voter</div>
        <div class="col-2">Last Vote (Council)</div>
        <div class="col-2">Green Vote Chance (Council)</div>
        <div class="col-2">Last Vote (MP)</div>
        <div class="col-2">Green Vote Chance (MP)</div>
    </div>

    <div class="row">
        @foreach ($addresses as $address)
            @foreach ($address->voters as $voter)
                <div class="col-1">&nbsp;</div>
                <div class="col-1">{{ $address->name }}</div>
                <div class="col-2">{{ $voter->name }}</div>

                <div class="col-2">
                    {{ Form::select('council_enum_generally_votes['.$voter->id.']', $enumGenerallyVotes, null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'margin-bottom: 0px']) }}

                    @if ($voter['council_enum_generally_votes_latest'])
                        <p style="margin-bottom: 0; font-size: smaller">Prev: {{ $enumGenerallyVotes[$voter['council_enum_generally_votes_latest']] }}</p>
                    @endif
                </div>
                <div class="col-2">
                    {{ Form::select('council_enum_vote_green_likelihood['.$voter->id.']', $enumVoteGreenLikelihood, null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'margin-bottom: 0px']) }}

                    @if ($voter['council_enum_vote_green_likelihood_latest'])
                        <p style="margin-bottom: 0; font-size: smaller">Prev: {{ $enumVoteGreenLikelihood[$voter['council_enum_vote_green_likelihood_latest']] }}</p>
                    @endif
                </div>
                <div class="col-2">
                    {{ Form::select('parl_enum_generally_votes['.$voter->id.']', $enumGenerallyVotes, null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'margin-bottom: 0px']) }}

                    @if ($voter['parl_enum_generally_votes_latest'])
                        <p style="margin-bottom: 0; font-size: smaller">Prev: {{ $enumGenerallyVotes[$voter['parl_enum_generally_votes_latest']] }}</p>
                    @endif
                </div>
                <div class="col-2">
                    {{ Form::select('parl_enum_vote_green_likelihood['.$voter->id.']', $enumVoteGreenLikelihood, null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'margin-bottom: 0px']) }}

                    @if ($voter['parl_enum_vote_green_likelihood_latest'])
                        <p style="margin-bottom: 0; font-size: smaller">Prev: {{ $enumVoteGreenLikelihood[$voter['parl_enum_vote_green_likelihood_latest']] }}</p>
                    @endif
                </div>
            @endforeach
        @endforeach
    </div>
@endsection
