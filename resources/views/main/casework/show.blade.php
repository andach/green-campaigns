@extends('template')

@section('title')
    Casework: {{ $casework->name }}
@endsection

@section('content')
    {{ Form::open(['route' => ['casework.show-post', $casework->id], 'method' => 'post']) }}

    <div class="card">
        <div class="card-header">Casework for {{ $casework->voter->name }}</div>
        <div class="card-body">
            @foreach ($casework->comments as $comment)
                <div class="card">
                    <div class="card-body">{{ $comment->comment }}</div>
                </div>
            @endforeach

            @if ($casework->is_closed)
                <div class="alert alert-danger">This item of casework is now closed.</div>
            @endif
        </div>
    </div>

    @if ($casework->is_closed)
        {{ Form::hidden('reopen', 1) }}
        {{ Form::bsSubmit('Reopen Casework to add Comments') }}
    @else
        <div class="card">
            <div class="card-header">Add Comment or Attachment</div>
            <div class="card-body">
                {{ Form::bsText('Comment', 'comment') }}
                {{ Form::bsCheckbox('Mark Work as Closed/Complete?', 'is_closed', 1) }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Casework') }}
    @endif

    {{ Form::close() }}
@endsection
