@extends('template')

@section('title')
    Create Casework
@endsection

@section('content')
    {{ Form::open(['route' => ['casework.create', $voter->id], 'method' => 'post']) }}

    <div class="card">
        <div class="card-header">New item of Casework for {{ $voter->name }} at {{ $voter->address->name }} {{ $voter->address->road->name }}</div>
        <div class="card-body">
            {{ Form::bsText('Title', 'title') }}
            {{ Form::bsTextarea('Comments and Notes', 'comment') }}
        </div>
    </div>

    {{ Form::bsSubmit('Create Casework Item') }}

    {{ Form::close() }}
@endsection
