@extends('template')

@section('title')
    My Casework
@endsection

@section('content')
    <div class="card">
        <div class="card-header">All Casework</div>
        <div class="card-body">
            @if ($caseworks->count())
                <div class="row row-header">
                    <div class="col-2">Address</div>
                    <div class="col-2">Voter</div>
                    <div class="col-4">Summary</div>
                    <div class="col-2">Opened On</div>
                    <div class="col-2">Status</div>
                </div>

                <div class="row">
                    @foreach ($caseworks as $casework)
                        <div class="col-2">{{ $casework->address->name }} {{ $casework->address->road->name }}</div>
                        <div class="col-2">{{ $casework->voter->name }}</div>
                        <div class="col-4"><a href="{{ route('casework.show', $casework->id) }}">{{ $casework->name }}</a></div>
                        <div class="col-2">{{ $casework->created_at }}</div>
                        <div class="col-2">{{ $casework->status_text }}</div>
                    @endforeach
                </div>
            @else
                You do not have any casework.
            @endif
        </div>
    </div>
@endsection
