@extends('template')

@section('title')
    {{ $ward->name }} Ward
@endsection

@section('content')
    <p><a href="{{ route('ward.export', $ward->id) }}">Export Ward to CSV</a></p>
    <p><a href="{{ route('address.create') }}">Add an Address</a></p>

    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header">Council Voting Intention</div>
                <div class="card-body"><canvas id="councilVote" width="400" height="400"></canvas></div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Council Green Vote Chance</div>
                <div class="card-body"><canvas id="councilGreen" width="400" height="400"></canvas></div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Parliament Voting Intention</div>
                <div class="card-body"><canvas id="parlVote" width="400" height="400"></canvas></div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Parliament Green Vote Chance</div>
                <div class="card-body"><canvas id="parlGreen" width="400" height="400"></canvas></div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">All Roads in this Ward</div>
        <div class="card-body">
            <div class="row row-header">
                <div class="col-4">Road</div>
                <div class="col-1">Houses</div>
                <div class="col-1">Voters</div>
                <div class="col-6">Delivery Route</div>
            </div>

            @foreach ($roads as $road)
                <div class="row">
                    <div class="col-4"><a href="{{ route('road.show', $road->id) }}">{{ $road->name }}</a></div>
                    <div class="col-1">{{ $road->number_of_houses }}</div>
                    <div class="col-1">{{ $road->number_of_voters }}</div>
                    <div class="col-6">{{ $road->delivery_route_names }}</div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('javascript')
    @include('component.chart.js.pie', ['htmlID' => 'councilVote', 'data' => $ward->canvassNumbersForChart('council_enum_generally_votes_latest'), 'colors' => $colorsVotes])
    @include('component.chart.js.pie', ['htmlID' => 'councilGreen', 'data' => $ward->canvassNumbersForChart('council_enum_vote_green_likelihood_latest'), 'colors' => $colorsGreen])
    @include('component.chart.js.pie', ['htmlID' => 'parlVote', 'data' => $ward->canvassNumbersForChart('parl_enum_generally_votes_latest'), 'colors' => $colorsVotes])
    @include('component.chart.js.pie', ['htmlID' => 'parlGreen', 'data' => $ward->canvassNumbersForChart('parl_enum_vote_green_likelihood_latest'), 'colors' => $colorsGreen])
@endsection
