@extends('template')

@section('title')
    All Wards
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Full List of Wards</div>
        <div class="card-body">
            <div class="row row-header">
                <div class="col-3">Council</div>
                <div class="col-3">Ward</div>
                <div class="col-3">Number of Houses / Voters</div>
                <div class="col-3">Delivery Route</div>
            </div>

            @foreach ($wards as $ward)
                <div class="row">
                    <div class="col-3">{{ $ward->council->name }}</div>
                    <div class="col-3"><a href="{{ route('ward.show', $ward->id) }}">{{ $ward->name }}</a></div>
                    <div class="col-3">{{ $ward->addresses_count }} / {{ $ward->voters_count }}</div>
                    <div class="col-3">?</div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
