@extends('template')

@section('title')
    Update User - {{ $user->name }}
@endsection

@section('content')
    {{ Form::open(['route' => ['user.edit', $user->id], 'method' => 'post']) }}
        <div class="card">
            <div class="card-header">Create User Form</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name', $user->name) }}
                {{ Form::bsText('Email', 'email', $user->email) }}
                {{ Form::bsPassword('Password', 'password') }}
                {{ Form::bsPassword('Confirm Password', 'password_confirmation') }}
                {{ Form::bsSelect('Access Councils', 'council_ids[]', $councils, $user->councils->pluck('id'), ['placeholder' => '', 'multiple' => 'multiple']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Update User') }}
    {{ Form::close() }}
@endsection
