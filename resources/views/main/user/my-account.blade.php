@extends('template')

@section('title')
    My Account - {{ $user->name }}
@endsection

@section('content')
    {{ Form::model($user, ['route' => ['user.my-account', $user->id], 'method' => 'post']) }}
    <div class="card">
        <div class="card-header">Create User Form</div>
        <div class="card-body">
            {{ Form::bsText('Name', 'name', $user->name) }}
            {{ Form::bsText('Email', 'email', $user->email) }}
            {{ Form::bsPassword('Password', 'password') }}
            {{ Form::bsPassword('Confirm Password', 'password_confirmation') }}
        </div>
    </div>

    {{ Form::bsSubmit('Update Your Account') }}
    {{ Form::close() }}
@endsection
