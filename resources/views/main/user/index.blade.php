@extends('template')

@section('title')
    All Users
@endsection

@section('content')
    <div class="card">
        <div class="card-header">All Users</div>
        <div class="card-body">
            <p><a href="{{ route('user.create') }}">Create a new user</a></p>

            <div class="row row-header">
                <div class="col-4">Name</div>
                <div class="col-4">Email</div>
                <div class="col-4">Created At</div>
            </div>

            <div class="row">
                @foreach ($users as $user)
                    <div class="col-4"><a href="{{ route('user.edit', $user->id) }}">{{ $user->name }}</a></div>
                    <div class="col-4">{{ $user->email }}</div>
                    <div class="col-4">{{ $user->created_at }}</div>
                @endforeach

            </div>
        </div>
    </div>
@endsection