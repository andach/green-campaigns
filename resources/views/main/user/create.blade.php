@extends('template')

@section('title')
    Create User
@endsection

@section('content')
    {{ Form::open(['route' => 'user.create', 'method' => 'post']) }}
    <div class="card">
        <div class="card-header">Create User Form</div>
        <div class="card-body">
            {{ Form::bsText('Name', 'name') }}
            {{ Form::bsText('Email', 'email') }}
            {{ Form::bsPassword('Password', 'password') }}
            {{ Form::bsPassword('Confirm Password', 'password_confirmation') }}
            {{ Form::bsSelect('Access Councils', 'council_ids[]', $councils, null, ['multiple' => 'multiple']) }}
        </div>
    </div>

    {{ Form::bsSubmit('Create User') }}
    {{ Form::close() }}
@endsection