@extends('template')

@section('title')
    Create Address
@endsection

@section('content')
    {{ Form::open(['route' => 'address.create-post', 'method' => 'post']) }}

    <div class="card">
        <div class="card-header">Create Address</div>
        <div class="card-body">
            {{ Form::select('ward_id', $wards, null, ['placeholder' => '', 'class' => 'form-control', 'id' => 'ward_id']) }}
            {{ Form::select('road_id', [], null, ['placeholder' => '--please select a ward first--', 'class' => 'form-control', 'id' => 'road_id']) }}
            {{ Form::bsText('Name/Number of House', 'name') }}
            {{ Form::bsTextarea('Notes', 'notes') }}
        </div>
    </div>

    {{ Form::bsSubmit('Add new Address') }}

    {{ Form::close() }}
@endsection


@section('javascript')
    <script nonce="{{ csp_nonce() }}">
        $(document).ready(function() {

            $("#ward_id").change(function() {
                var wardID = $(this).val();

                $.ajax({
                    url: '/ajax/ward-to-roads/'+wardID,
                    type: 'get',
                    data: {id:wardID},
                    dataType: 'json',

                    success:function(response) {
                        $('#road_id').empty();
                        $('#address_id').empty();
                        $('#voter_id').empty();
                        $('#road_id').append($('<option>', {value:0, text:''}));
                        $.each( response, function(k, v) {
                            $('#road_id').append($('<option>', {value:k, text:v}));
                        });
                    }
                });
            });

        });
    </script>
@endsection
