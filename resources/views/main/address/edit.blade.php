@extends('template')

@section('title')
    Update Voters
@endsection

@section('content')
    {{ Form::open(['route' => ['address.edit-post', $address->id], 'method' => 'post']) }}

    <div class="alert alert-info">
        This page is used when someone is no longer on the electoral register for any reason, or to manually add new voters.
    </div>

    @if ($address->voters->count())
        <div class="card">
            <div class="card-header">Existing Voters</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">Delete?</div>
                    <div class="col-9">Name</div>

                    @foreach ($address->voters as $voter)
                        {{ Form::bsCheckboxReverse($voter->name, 'delete_voters[]', $voter->id) }}
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="card">
        <div class="card-header">Add New Voter</div>
        <div class="card-body">
            {{ Form::bsText('Forename', 'forename') }}
            {{ Form::bsText('Surname', 'surname') }}
            {{ Form::bsCheckbox('Is a Postal Voter', 'is_postal', 1) }}
            {{ Form::bsText('Notes', 'notes') }}
        </div>
    </div>

    {{ Form::bsSubmit('Update Voters at this Address') }}

    {{ Form::close() }}
@endsection
