@extends('template')

@section('title')
    Upload New Marked Register
@endsection

@section('infobox')
    This upload requires a CSV file of voter IDs. Only the first column will be used, all other columns will be ignored.
@endsection

@section('content')
    <div class="alert alert-danger" role="alert">Upload only ONE council at a time.</div>

    {{ Form::open(['route' => 'upload.marked-register-post', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="card">
            <div class="card-header">Upload Marked Register</div>
            <div class="card-body">
                {{ Form::bsSelect('Election', 'election_id', $elections, null) }}
                {{ Form::bsFile('File', 'file') }}
            </div>
        </div>

        {{ Form::bsSubmit('Upload Marked Register') }}
    {{ Form::close() }}
@endsection
