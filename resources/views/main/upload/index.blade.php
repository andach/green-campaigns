@extends('template')

@section('title')
    Upload Data to System
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Upload Information</div>
        <div class="card-body">
            <p><a href="{{ route('upload.absent-register') }}">Absent Register</a> (of postal voters)</p>
            <p><a href="{{ route('upload.electoral-register') }}">Electoral Register</a></p>
            <p><a href="{{ route('upload.marked-register') }}">Marked Register</a> (of those who have voted in a given election)</p>
        </div>
    </div>
@endsection
