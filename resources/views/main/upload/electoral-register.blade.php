@extends('template')

@section('title')
    Upload New Electoral Register
@endsection

@section('content')
    <div class="alert alert-danger" role="alert">Upload only ONE ward.</div>

    {{ Form::open(['route' => 'upload.electoral-register-post', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="card">
            <div class="card-header">Upload Ward</div>
            <div class="card-body">
                {{ Form::bsSelect('Council', 'council_id', $councils, null) }}
                {{ Form::bsSelect('Ward', 'ward_id', $wards, null) }}
                {{ Form::bsDate('Date of Electoral Register', 'date_of_electoral_register') }}
                {{ Form::bsFile('File', 'file') }}
            </div>
        </div>

        {{ Form::bsSubmit('Upload Electoral Register') }}
    {{ Form::close() }}
@endsection
