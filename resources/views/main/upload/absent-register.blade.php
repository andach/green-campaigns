@extends('template')

@section('title')
    Upload New Absent Register
@endsection

@section('infobox')
    This page expects to receive information for either a full council or a ward. If a council or ward is selected, then the absent voter information for
    these entities will be completely deleted, and replaced with the information entered. If not, the absent voter information will be added, which will
    generally not be what you want.
@endsection

@section('content')
    {{ Form::open(['route' => 'upload.absent-register-post', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="card">
            <div class="card-header">Upload Ward</div>
            <div class="card-body">
                {{ Form::bsSelect('Council', 'council_id', $councils, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Ward', 'ward_id', $wards, null, ['placeholder' => '']) }}
                {{ Form::bsFile('File', 'file') }}
            </div>
        </div>

        {{ Form::bsSubmit('Upload Absent Register') }}
    {{ Form::close() }}
@endsection
