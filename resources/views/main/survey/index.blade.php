@extends('template')

@section('title')
    All Surveys
@endsection

@section('content')
    <p><a href="{{ route('survey.create') }}">Create a new Survey</a></p>

    <div class="card">
        <div class="card-header">Full List of Surveys</div>
        <div class="card-body">
            <div class="row row-header">
                <div class="col-3">Council</div>
                <div class="col-3">Created by User</div>
                <div class="col-3">Name</div>
                <div class="col-3">Number of Responses</div>
            </div>

            @foreach ($surveys as $survey)
                <div class="row">
                    <div class="col-3">{{ $survey->council->name }}</div>
                    <div class="col-3">{{ $survey->user->name }}</div>
                    <div class="col-3">
                        <a href="{{ route('survey.show', $survey->id) }}">{{ $survey->name }}</a>
                        (<a href="{{ route('survey.edit', $survey->id) }}">Edit</a>)
                        (<a href="{{ route('survey.export', $survey->id) }}">Export</a>)
                    </div>
                    <div class="col-3">{{ count($survey->responses) }} <a href="{{ route('survey.responses', $survey->id) }}">View Responses</a>    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
