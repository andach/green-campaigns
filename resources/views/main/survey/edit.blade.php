@extends('template')

@section('title')
    Edit Survey
@endsection

@section('content')
    {{ Form::open(['route' => ['survey.edit-post', $survey->id], 'method' => 'post']) }}

    <div class="card">
        <div class="card-header">Survey Name</div>
        <div class="card-body">
            {{ Form::bsText('Name', 'name', $survey->name) }}
        </div>
    </div>

    @if ($survey->canEdit())
        <div class="card">
            <div class="card-header">How Important are the Following to You?</div>
            <div class="card-body">
                {{ Form::bsTextarea('Add Questions (one per line)', 'add_issues') }}

                @foreach ($survey->issues as $issue)
                    {{ Form::bsCheckbox($issue->name, 'delete_issues[]', $issue->id) }}
                @endforeach
            </div>
            <div class="card-footer">
                You only need to enter a summary of the issue, not the full text as written on the survey itself. The
                software will automatically let you enter whether the issues are not/fairly/very important on a
                subsequent screen.
            </div>
        </div>

        <div class="card">
            <div class="card-header">Current Questions on the Survey</div>
            <div class="card-body">
                @if (!count($survey->questions))
                    <p>No questions have been added to this survey yet</p>
                @else
                    @foreach ($survey->questions as $question)
                        <div class="card">
                            <div class="card-header">{{ $question->name }}</div>
                            <div class="card-body">
                                @foreach ($question->options as $option)
                                    {{ Form::bsCheckboxReverse($option->name, 'delete_options[]', $option->id) }}
                                @endforeach

                                {{ Form::bsText('Add an option to this question', 'add_option_to_question['.$question->id.']') }}
                            </div>
                            <div class="card-footer">
                                {{ Form::bsCheckboxReverse('Delete this Question', 'delete_questions[]', $question->id) }}
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="card">
            <div class="card-header">Add Question to Survey "{{ $survey->name }}"</div>
            <div class="card-body">
                {{ Form::bsText('Question', 'new_question_name') }}
                {{ Form::bsTextarea('Options (one per line)', 'new_question_options') }}
            </div>
        </div>
    @endif

    {{ Form::bsSubmit('Update Survey') }}

    {{ Form::close() }}
@endsection
