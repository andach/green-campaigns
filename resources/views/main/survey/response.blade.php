@extends('template')

@section('title')
    Record Response to Survey - {{ $survey->name }}
@endsection

@section('content')
    {{ Form::open(['route' => ['survey.response-post', $survey->id], 'method' => 'post']) }}

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">Address and Voter</div>
                <div class="card-body">
                    {{ Form::bsDate('Date of Response', 'date_of_response') }}
                    <div class="row">
                        <div class="col-12">{{ Form::select('ward_id', $wards, null, ['placeholder' => '', 'class' => 'form-control', 'id' => 'ward_id']) }}</div>
                        <div class="col-12">{{ Form::select('road_id', [], null, ['placeholder' => '--please select a ward first--', 'class' => 'form-control', 'id' => 'road_id']) }}</div>
                        <div class="col-12">{{ Form::select('address_id', [], null, ['placeholder' => '--please select a road first--', 'class' => 'form-control', 'id' => 'address_id']) }}</div>
                        <div class="col-12">{{ Form::select('voter_id', [], null, ['placeholder' => '--please select an address first--', 'class' => 'form-control', 'id' => 'voter_id']) }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">How Important are the Following to You?</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Issue</th>
                                <th>Not Important</th>
                                <th>Fairly Important</th>
                                <th>Very Important</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($survey->issues as $issue)
                                <tr>
                                    <th>{{ $issue->name }}</th>
                                    <th>{{ Form::radio('issue['.$issue->id.']', 0) }}</th>
                                    <th>{{ Form::radio('issue['.$issue->id.']', 1) }}</th>
                                    <th>{{ Form::radio('issue['.$issue->id.']', 2) }}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Other Issues Classed as Important (or general notes)</div>
                <div class="card-body">
                    {{ Form::textarea('other_issues', null, ['class' => 'form-control', 'rows' => 3]) }}
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Voting Intention</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Party</th>
                                <th>Definitely</th>
                                <th>Maybe</th>
                                <th>Never</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($parties as $partyID => $partyName)
                                <tr>
                                    <th>{{ $partyName }}</th>
                                    <th>{{ Form::radio('party['.$partyID.']', 2) }}</th>
                                    <th>{{ Form::radio('party['.$partyID.']', 1) }}</th>
                                    <th>{{ Form::radio('party['.$partyID.']', 0) }}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">Questions on the Survey</div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($survey->questions as $question)
                        <tr>
                            <td>{{ $question->name }}</td>
                            <td>
                                @foreach ($question->options as $option)
                                    <p>{{ Form::radio('option_id['.$question->id.']', $option->id) }} - {{ $option->name }}</p>
                                @endforeach
                                {{ Form::text('additional_text['.$question->id.']', null, ['placeholder' => 'Additional Info (if relevant)', 'class' => 'form-control']) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Gave Contact Details?</div>
                <div class="card-body">
                    {{ Form::bsText('Name', 'signup_name') }}
                    {{ Form::bsText('Email', 'signup_email') }}
                    {{ Form::bsText('Phone', 'signup_phone') }}
                </div>
                <div class="card-footer">Only fill this in if the respondant gave their contact details voluntarily. DO NOT fill this in if you just know them, or they did not give explicit and clear details for their information to be collected.</div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Volunteering?</div>
                <div class="card-body">
                    {{ Form::bsCheckbox('Deliver Newsletters?', 'volunteer_deliver', 1) }}
                    {{ Form::bsCheckbox('Vote for Us?', 'volunteer_vote', 1) }}
                    {{ Form::bsCheckbox('Put up a Poster?', 'volunteer_poster', 1) }}
                    {{ Form::bsCheckbox('Anything Else?', 'volunteer_other', 1) }}
                    {{ Form::text('volunteer_text', null, ['placeholder' => 'Notes about Volunteering', 'class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Record Survey Response') }}

    {{ Form::close() }}
@endsection

@section('javascript')
<script nonce="{{ csp_nonce() }}">
    $(document).ready(function() {

        $("#ward_id").change(function() {
            var wardID = $(this).val();

            $.ajax({
                url: '/ajax/ward-to-roads/'+wardID,
                type: 'get',
                data: {id:wardID},
                dataType: 'json',

                success:function(response) {
                    $('#road_id').empty();
                    $('#address_id').empty();
                    $('#voter_id').empty();
                    $('#road_id').append($('<option>', {value:0, text:''}));
                    $.each( response, function(k, v) {
                        $('#road_id').append($('<option>', {value:k, text:v}));
                    });
                }
            });
        });

        $("#road_id").change(function() {
            var roadID = $(this).val();

            $.ajax({
                url: '/ajax/road-to-addresses/'+roadID,
                type: 'get',
                data: {id:roadID},
                dataType: 'json',

                success:function(response) {
                    $('#address_id').empty();
                    $('#voter_id').empty();
                    $('#address_id').append($('<option>', {value:0, text:''}));
                    $.each( response, function(k, v) {
                        $('#address_id').append($('<option>', {value:k, text:v}));
                    });
                }
            });
        });

        $("#address_id").change(function() {
            var addressID = $(this).val();

            $.ajax({
                url: '/ajax/address-to-voters/'+addressID,
                type: 'get',
                data: {id:addressID},
                dataType: 'json',

                success:function(response) {
                    $('#voter_id').empty();
                    $('#voter_id').append($('<option>', {value:0, text:''}));
                    $.each( response, function(k, v) {
                        $('#voter_id').append($('<option>', {value:k, text:v}));
                    });
                }
            });
        });

    });
    /*
    var xhttp = new XMLHttpRequest();
    if (!data.isNew) {
        xhttp.open("GET", "/location/ajax/update/"+this.key+"/"+data.input.val(), true);
        xhttp.send();
    } else {
        parentID = this.parent.key;
        if (isNaN(parentID)) {
            parentID = 0;
        }

        xhttp.open("GET", "/location/ajax/create/"+parentID+"/"+data.input.val(), true);
        xhttp.send();
    }
    */
</script>
@endsection
