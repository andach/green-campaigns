@extends('template')

@section('title')
    Create Survey
@endsection

@section('content')
    {{ Form::open(['route' => 'survey.create-post', 'method' => 'post']) }}

    <div class="card">
        <div class="card-header">New Survey</div>
        <div class="card-body">
            {{ Form::bsText('Name', 'name') }}
            {{ Form::bsSelect('Council', 'council_id', $councils) }}
        </div>
    </div>

    {{ Form::bsSubmit('Create this Survey') }}

    {{ Form::close() }}
@endsection
