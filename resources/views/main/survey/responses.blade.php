@extends('template')

@section('title')
    All Response to Survey "{{ $survey->name }}"
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Survey Responses</div>
        <div class="card-body">
            <div class="row">
                <div class="col-1">Ward</div>
                <div class="col-2">Address</div>
                <div class="col-2">Voter</div>
                <div class="col-1">Date</div>
                <div class="col-2">Signed Up?</div>
                <div class="col-2">Volunteered?</div>
                <div class="col-2">&nbsp;</div>

                @foreach ($survey->responses as $response)
                    <div class="col-1">{{ $response->ward_name }}</div>
                    <div class="col-2">{{ $response->address_name }}</div>
                    <div class="col-2">{{ $response->voter_name }}</div>
                    <div class="col-1">{{ $response->date_of_response }}</div>
                    <div class="col-2">{{ $response->hasSignedUp() ? 'Yes' : 'No' }}</div>
                    <div class="col-2">{{ $response->hasVolunteered() ? 'Yes' : 'No' }}</div>
                    <div class="col-2"><a href="{{ route('survey-response.show', $response->id) }}">Full Response</a></div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
