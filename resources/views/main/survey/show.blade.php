@extends('template')

@section('title')
    Show Survey - {{ $survey->name }}
@endsection

@section('content')
    <p><a href="{{ route('survey.response', $survey->id) }}">Record Response to this Survey</a></p>
    <p><a href="{{ route('survey.export', $survey->id) }}">Export Survey to CSV</a></p>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">Response Summary</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6"><canvas id="surveysByMonth" width="400" height="400"></canvas></div>
                        <div class="col-6"><canvas id="issuesSummary" width="400" height="400"></canvas></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-5">
            <div class="card">
                <div class="card-header">How Important are the Following to You?</div>
                <div class="card-body">{!! implode('<br />', $survey->issues->pluck('name')->toArray()) !!}</div>
            </div>
        </div>

        <div class="col-12 col-md-7">
            <div class="card">
                <div class="card-header">Questions on the Survey</div>
                <div class="card-body">
                    @if (!count($survey->questions))
                        <p>No questions have been added to this survey yet</p>
                    @else
                        @foreach ($survey->questions as $question)
                            <p>
                                <b>{{ $question->name }}</b>
                                <br />
                                Options: {{ implode(', ', $question->options->pluck('name')->toArray()) }}
                            </p>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
@include('component.chart.js.bar', [
    'htmlID' => 'surveysByMonth',
    'title' => 'Number of Responses Filled out by Month',
    'data' => $survey->chartJSItemsByMonth(),
    // 'colors' => $colorsVotes,
])

@include('component.chart.js.bar-stacked', [
    'htmlID' => 'issuesSummary',
    'data' => $survey->chartJSIssues(),
    'labels' => $survey->chartJSIssuesLabels(),
    'title' => 'Responses to main Issues',
    'colors' => [
        'Not Important'    => '#dc3545',
        'Fairly Important' => '#4d4d4d',
        'Very Important'   => '#6AB023',
    ],
])
@endsection
