@extends('template')

@section('title')
    All Activists
@endsection

@section('content')
    {{ Form::open(['route' => 'activist.index', 'method' => 'get']) }}

        <div class="card">
            <div class="card-header">Search</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        {{ Form::bsText('Forename', 'forename') }}
                        {{ Form::bsText('Surname', 'surname') }}
                        {{ Form::bsSelect('Is a Member', 'is_member', [1 => 'Yes', 0 => 'No'], null, ['placeholder' => '']) }}
                    </div>
                    <div class="col-6">
                        {{ Form::bsText('Council Ward', 'council_ward') }}
                        {{ Form::bsText('Local Authority', 'local_authority') }}
                        {{ Form::bsText('Constituency', 'constituency') }}
                    </div>
                </div>

                {{ Form::bsSubmit('Search Activists') }}
            </div>
        </div>

    {{ Form::close() }}

    <div class="card">
        <div class="card-header">Activists</div>
        <div class="card-body">
            <div class="row row-header">
                <div class="col-2">Name</div>
                <div class="col-3">Contact</div>
                <div class="col-1">Member?</div>
                <div class="col-2">Membership End</div>
                <div class="col-1">Can Phone?</div>
                <div class="col-2">Local Authority</div>
                <div class="col-1">Ward</div>
            </div>

            <div class="row">
                @foreach ($activists as $activist)
                    <div class="col-2"><a href="{{ route('activist.show', $activist->id) }}">{{ $activist->name }}</a></div>
                    <div class="col-3">{!! $activist->formatted_phone_and_email !!}</div>
                    <div class="col-1">{{ $activist->formatted_is_member }}</div>
                    <div class="col-2">{{ $activist->membership_end_date }}</div>
                    <div class="col-1">{{ $activist->formatted_can_phone }}</div>
                    <div class="col-2">{{ $activist->local_authority }}</div>
                    <div class="col-1">{{ $activist->council_ward }}</div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::open(['route' => 'activist.upload', 'method' => 'post', 'files' => true]) }}

        <div class="card">
            <div class="card-header">Upload Replacement Activist List</div>
            <div class="card-body">
                {{ Form::bsSelect('Council', 'council_id', $councils->pluck('name', 'id')->toArray()) }}
                {{ Form::bsFile('CSV Upload', 'file') }}
                {{ Form::bsSubmit('Upload new Activists') }}
            </div>
            <div class="card-footer">This requires a direct export in CSV form from Action Network.</div>
        </div>

    {{ Form::close() }}
@endsection
