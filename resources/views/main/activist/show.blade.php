@extends('template')

@section('title')
    Activist - {{ $activist->name }}
@endsection

@section('content')
    <p><a href="{{ route('activist.index') }}">Back to all activists</a></p>

    <div class="card">
        <div class="card-header">Activist Details</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Name</div>
                <div class="col-10">{{ $activist->name }}</div>

                <div class="col-2">Email</div>
                <div class="col-10">{{ $activist->email }}</div>

                <div class="col-2">Phone</div>
                <div class="col-10">{{ $activist->phone_number }}@if ($activist->do_not_phone) (do not phone)@endif</div>

                <div class="col-2">Mobile</div>
                <div class="col-10">{{ $activist->mobile_number }}@if ($activist->do_not_phone) (do not phone)@endif</div>

                <div class="col-2">Address</div>
                <div class="col-10">{{ $activist->address }}<br />{{ $activist->city }}<br />{{ $activist->postcode }}</div>

                <div class="col-2">Ward</div>
                <div class="col-10">{{ $activist->council_ward }}</div>

                <div class="col-2">Local Authority</div>
                <div class="col-10">{{ $activist->local_authority }}</div>

                <div class="col-2">Constituency</div>
                <div class="col-10">{{ $activist->constituency }}</div>

                <div class="col-2">Membership Details</div>
                <div class="col-10">
                    @if ($activist->membership_type)
                        {{ $activist->membership_type }}

                        @if ($activist->is_member)
                            (Current Member, expiring {{ $activist->membership_end_date }})
                        @else
                            (Former Member, expired {{ $activist->membership_end_date }})
                        @endif
                    @else
                        N/A
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">Contact History</div>
        <div class="card-body">
            @if (count($activist->contacts))
                <div class="row">
                    <div class="col-2">Date</div>
                    <div class="col-2">Contacted By</div>
                    <div class="col-2">About Event?</div>
                    <div class="col-2">Method</div>
                    <div class="col-4">Notes</div>

                    @foreach ($activist->contacts as $contact)
                        <div class="col-2">{{ $contact->date_of_contact }}</div>
                        <div class="col-2">{{ $contact->user->name }}</div>
                        <div class="col-2">{{ $contact->event_name }}</div>
                        <div class="col-2">
                            {{ $contactTypes[$contact->enum_contact_type] }}

                            @if (!$contact->managed_to_contact)
                                (Unsuccessful)
                            @endif
                        </div>
                        <div class="col-3">{{ $contact->notes }}</div>
                        <div class="col-1">
                            {{ Form::open(['route' => ['activist.contact-delete', $contact->id], 'method' => 'post']) }}
                                {{ Form::hidden('contact_id', $contact->id) }}
                                {{ Form::submit('Delete', ['class' => 'form-control btn btn-danger']) }}
                            {{ Form::close() }}
                        </div>
                    @endforeach
                </div>
            @else
                <p>There is no contact history for this member.</p>
            @endif
        </div>
    </div>

    {{ Form::open(['route' => ['activist.show-post', $activist->id], 'method' => 'post']) }}
    <div class="card">
        <div class="card-header">Record new Contact</div>
        <div class="card-body">
            {{ Form::hidden('managed_to_contact', 0) }}
            {{ Form::hidden('is_attending', 0) }}
            {{ Form::bsDate('Date', 'date_of_contact', date('Y-m-d')) }}
            {{ Form::bsSelect('Method of Contact', 'enum_contact_type', $contactTypes) }}
            {{ Form::bsCheckbox('Successful Contact?', 'managed_to_contact', 1, 1) }}
            {{ Form::bsText('Notes', 'notes') }}
            {{ Form::bsSelect('Specifically about an Event?', 'event_id', $events, null, ['placeholder' => '']) }}
            {{ Form::bsCheckbox('Are they Attending? (if relevant)', 'is_attending', 1) }}
            {{ Form::bsSubmit('Record Attempt to Contact') }}
        </div>
    </div>
@endsection
