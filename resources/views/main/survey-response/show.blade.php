@extends('template')

@section('title')
    Show Response to Survey "{{ $response->survey->name }}"
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Issues</div>
                <div class="card-body">
                    <div class="row row-header">
                        <div class="col-7">Issue</div>
                        <div class="col-5">Response</div>
                    </div>

                    <div class="row">
                        @foreach ($issues as $issueName => $issueResponse)
                            <div class="col-7">{{ $issueName }}</div>
                            <div class="col-5">{{ $issueResponse }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">Parties</div>
                <div class="card-body">
                    <div class="row row-header">
                        <div class="col-7">Party</div>
                        <div class="col-5">Response</div>
                    </div>

                    <div class="row">
                        @foreach ($parties as $partyName => $partyResponse)
                            <div class="col-7">{{ $partyName }}</div>
                            <div class="col-5">{{ $partyResponse }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-header">Answers to Individual Questions</div>
                <div class="card-body">
                    <div class="row row-header">
                        <div class="col-4">Question</div>
                        <div class="col-2">Option Chosen</div>
                        <div class="col-6">Additional Text or Info</div>
                    </div>

                    <div class="row">
                        @foreach ($response->answers as $answer)
                            <div class="col-4">{{ $answer->question->name }}</div>
                            <div class="col-2">{{ $answer->option->name }}</div>
                            <div class="col-6">{{ $answer->additional_text }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
