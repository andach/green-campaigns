@extends('template')

@section('title')
    Manage Councils
@endsection

@section('content')
    {{ Form::open(['route' => 'council.index-post', 'method' => 'post']) }}

    <div class="card card-dark">
        <div class="card-header">Current Councils in System</div>
        <div class="card-body">
            <div class="row row-header">
                <div class="col-6">Name</div>
                <div class="col-6">Wards</div>
            </div>
            
            <div class="row">
                @foreach ($councils as $council)
                    <div class="col-6">
                        {{ Form::bsCheckbox($council->name, 'delete_council[]', $council->id) }}
                    </div>

                    <div class="col-6">
                        @foreach ($council->wards as $ward)
                            {{ Form::bsCheckbox($ward->name.' ['.$ward->electoral_prefix.']', 'delete_ward[]', $ward->id) }}
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            There are {{ count($councils) }} councils in the system at the moment.
        </div>
    </div>


    <div class="card card-dark">
        <div class="card-header">Add a Council</div>
        <div class="card-body">
            {{ Form::bsText('Council Name', 'new_council') }}
        </div>
    </div>


    <div class="card card-dark">
        <div class="card-header">Add a Ward to a Council</div>
        <div class="card-body">
            {{ Form::bsSelect('Council', 'council_id', $councils->pluck('name', 'id')->toArray()) }}
            {{ Form::bsText('Ward Name', 'new_ward') }}
            {{ Form::bsText('Electoral Prefix', 'electoral_prefix') }}
        </div>
    </div>

        {{ Form::bsSubmit('Update Councils and Wards') }}

    {{ Form::close() }}
@endsection