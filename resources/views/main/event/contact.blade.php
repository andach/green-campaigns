@extends('template')

@section('title')
    Contact {{ $activist->name }} about {{ $event->name }}
@endsection

@section('content')
    <p><a href="{{ route('event.index') }}">Back to list of events</a></p>
    <p><a href="{{ route('event.show', $event->id) }}">Back to this event</a></p>

    <div class="row">
        <div class="col-12 col-md-4">
            <div class="card">
                <div class="card-header">Contact Details</div>
                <div class="card-body">
                    @if ($isAttending)
                        <p>{{ $activist->name }} is coming to this event.</p>
                    @else
                        <p>{{ $activist->name}} is not attending.</p>
                    @endif

                    <p>
                        M: {{ $activist->mobile_number }}
                        H: {{ $activist->phone_number }}
                        E: {{ $activist->email }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">Previous Attempts to Contact</div>
                <div class="card-body">
                    <div class="row">
                        @if (count($contacts))
                            <div class="col-3">Date/Time</div>
                            <div class="col-2">Method</div>
                            <div class="col-2">Managed to Contact?</div>
                            <div class="col-5">Notes</div>
                            @foreach ($contacts as $contact)
                                <div class="col-3">{{ $contact->date_of_contact }}</div>
                                <div class="col-2">{{ $contactTypes[$contact->enum_contact_type] }}</div>
                                <div class="col-2">{{ $contact->formatted_managed_to_contact }}</div>
                                <div class="col-5">{{ $contact->notes }}</div>
                            @endforeach
                        @else
                            <div class="col-12">This person has not been contacted yet about this event.</div>
                        @endif
                    </div>

                    @if ($isAttending)
                        {{ Form::open(['route' => ['event.contact-not-coming', ['id' => $event->id, 'activistID' => $activist->id]], 'method' => 'post']) }}
                        {{ Form::hidden('not_coming', 1) }}
                        {{ Form::bsSubmit('Mark this person as not attending') }}
                        {{ Form::close() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{ Form::open(['route' => ['event.contact', ['id' => $event->id, 'activistID' => $activist->id]], 'method' => 'post']) }}
        <div class="card">
            <div class="card-header">Record Contact Attempt</div>
            <div class="card-body">
                {{ Form::hidden('managed_to_contact', 0) }}
                {{ Form::hidden('is_attending', 0) }}
                {{ Form::bsDate('Date', 'date_of_contact', date('Y-m-d')) }}
                {{ Form::bsSelect('Method of Contact', 'enum_contact_type', $contactTypes) }}
                {{ Form::bsCheckbox('Successful Contact?', 'managed_to_contact', 1) }}
                {{ Form::bsCheckbox('Are they Attending?', 'is_attending', 1) }}
                {{ Form::bsText('Notes', 'notes') }}
                {{ Form::bsSubmit('Record Attempt to Contact') }}
            </div>
        </div>
    {{ Form::close() }}
@endsection
