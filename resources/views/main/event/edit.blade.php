@extends('template')

@section('title')
    Edit Event - {{ $event->name }}
@endsection

@section('content')
    <p><a href="{{ route('event.index') }}">Back to list of events</a></p>

    {{ Form::model($event, ['route' => ['event.edit-post', $event->id], 'method' => 'post']) }}

        <div class="card">
            <div class="card-header">Event Details</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsDate('Date of Event', 'date_of_event') }}
                {{ Form::bsText('Notes', 'notes') }}
                {{ Form::bsSubmit('Edit Event') }}
            </div>
        </div>

    {{ Form::close() }}
@endsection
