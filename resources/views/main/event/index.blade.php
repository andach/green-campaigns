@extends('template')

@section('title')
    All Events
@endsection

@section('content')
    <p><a href="{{ route('event.create') }}">Create an event</a></p>

    @foreach ($councils as $council)
        <div class="card">
            <div class="card-header">{{ $council->name }} Events</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-2">Name</div>
                    <div class="col-2">Created By</div>
                    <div class="col-2">Date</div>
                    <div class="col-2">Number Attending (&amp; Attended)</div>
                    <div class="col-3">Notes</div>
                    <div class="col-1">Edit?</div>
                    @foreach ($council->events as $event)
                        <div class="col-2"><a href="{{ route('event.show', $event->id) }}">{{ $event->name }}</a></div>
                        <div class="col-2">{{ $event->user->name }}</div>
                        <div class="col-2">{{ $event->date_of_event }}</div>
                        <div class="col-2">{{ $event->number_attending }} ({{ $event->number_attended }})</div>
                        <div class="col-3">{{ $event->notes }}</div>
                        <div class="col-1"><a href="{{ route('event.edit', $event->id) }}">Edit</a></div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
@endsection
