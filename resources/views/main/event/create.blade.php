@extends('template')

@section('title')
    Create Event
@endsection

@section('content')
    <p><a href="{{ route('event.index') }}">Back to list of events</a></p>

    {{ Form::open(['route' => 'event.create-post', 'method' => 'post']) }}
        <div class="card">
            <div class="card-header">Create an Event</div>
            <div class="card-body">
                {{ Form::bsSelect('Council', 'council_id', $councils) }}
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsDate('Date of Event', 'date_of_event') }}
                {{ Form::bsText('Notes', 'notes') }}
                {{ Form::bsSubmit('Create Event') }}
            </div>
        </div>
    {{ Form::close() }}
@endsection
