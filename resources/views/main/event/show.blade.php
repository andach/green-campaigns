@extends('template')

@section('title')
    {{ $event->name }}
@endsection

@section('content')
    <p><a href="{{ route('event.index') }}">Back to list of events</a></p>

    {{ Form::open(['route' => ['event.show', $event->id], 'method' => 'get']) }}

    <div class="card">
        <div class="card-header">Search Activists</div>
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    {{ Form::bsText('Forename', 'forename') }}
                    {{ Form::bsText('Surname', 'surname') }}
                    {{ Form::bsSelect('Is a Member', 'is_member', [1 => 'Yes', 0 => 'No'], null, ['placeholder' => '']) }}
                </div>
                <div class="col-6">
                    {{ Form::bsText('Council Ward', 'council_ward') }}
                    {{ Form::bsText('Local Authority', 'local_authority') }}
                    {{ Form::bsText('Constituency', 'constituency') }}
                </div>
            </div>

            {{ Form::bsSubmit('Search Activists') }}
        </div>
    </div>

    {{ Form::close() }}

    <div class="card">
        <div class="card-header">Event Details</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Name</div>
                <div class="col-10">{{ $event->name }}</div>

                <div class="col-2">Created By</div>
                <div class="col-10">{{ $event->user->name }}</div>

                <div class="col-2">Date</div>
                <div class="col-10">{{ $event->date_of_event }}</div>

                <div class="col-2">Notes</div>
                <div class="col-10">{{ $event->notes }}</div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">All Available Activists</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Name</div>
                <div class="col-2">Phone</div>
                <div class="col-1">Member?</div>
                <div class="col-2">Membership End</div>
                <div class="col-1">Can Phone?</div>
                <div class="col-2">Local Authority</div>
                <div class="col-2">Contacted?</div>
                @foreach ($activists as $activist)
                    <div class="col-2"><a href="{{ route('event.contact', ['id' => $event->id, 'activistID' => $activist->id]) }}">{{ $activist->name }}</a></div>
                    <div class="col-2">{!! $activist->formatted_phone !!}</div>
                    <div class="col-1">{{ $activist->formatted_is_member }}</div>
                    <div class="col-2">{{ $activist->membership_end_date }}</div>
                    <div class="col-1">{{ $activist->formatted_can_phone }}</div>
                    <div class="col-2">{{ $activist->local_authority }}</div>
                    <div class="col-2">
                        @if (in_array($activist->id, $attending))
                            <b>Attending</b><br />
                        @elseif (in_array($activist->id, $contacted))
                            Contacted
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
