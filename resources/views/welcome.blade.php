@extends('template')

@section('content')
    @if (!Auth::check())
        <div class="alert alert-danger" role="alert">
            This is a piece of software only for use by the Derbyshire Green Party.
            Unauthorised access to this system is a criminal offence.
            Detailed and secure logs are taken of all access attempts made to this server and will be shared with the police if required.
        </div>
    @else
        <div class="row">
            <div class="col-12"><h2>Change Log</h2></div>

            <div class="col-2">Date</div>
            <div class="col-10">Updates</div>

            <div class="col-2">2021-10-21</div>
            <div class="col-10">
                Added the ability to export surveys and wards to CSV.<br />
                Added a drop-down to manually choose the next address when canvassing on foot.<br />
                Added notes to addresses on the canvass page.<br />
                Added a way to manually remove and add voters.
            </div>

            <div class="col-12">&nbsp;</div>

            <div class="col-12"><h2>Upcoming Changes and Fixes</h2></div>
            <div class="col-12">
                <ul>
                    <li>Upload the absent and marked register for Mackworth, then get a "marked only" printable canvass sheet, and canvassing "marked only" addresses.</li>
                </ul>
            </div>
        </div>
    @endif
@endsection
