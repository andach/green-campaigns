@extends('template')

@section('title')
    Help - Elector Markers
@endsection

@section('content')
    <p><a href="{{ route('help') }}">Back to the main help page</a></p>
    <p><hr /></p>

    <div class="card">
        <div class="card-header">Guide to abbreviations and further reading</div>
        <div class="card-body">
            <table class="table table-striped table-hover">
                <caption class="heading"></caption>
                <tbody>
                    <tr>
                        <th scope="col">Abbreviation</th>
                        <th scope="col">Meaning</th>
                    </tr>
                    <tr>
                        <td>†</td>
                        <td>Not entitled to vote in local (later, metropolitan borough, urban district, or parish) elections</td>
                    </tr>
                    <tr>
                        <td>††</td>
                        <td>Not entitled to vote in rural district elections</td>
                    </tr>
                    <tr>
                        <td>[date]</td>
                        <td>Entitled to vote from that date</td>
                    </tr>
                    <tr>
                        <td>A<br />
                        </td>
                        <td>Absent voter (serving in the armed forces)</td>
                    </tr>
                    <tr>
                        <td>BP(Bw)<br />
                        </td>
                        <td>Business premises qualification (women)</td>
                    </tr>
                    <tr>
                        <td>D (Dw)</td>
                        <td>Spouse’s occupation qualification</td>
                    </tr>
                    <tr>
                        <td>E  </td>
                        <td>Not entitled to vote in local or parliamentary elections</td>
                    </tr>
                    <tr>
                        <td>F </td>
                        <td>Not entitled to vote in local elections</td>
                    </tr>
                    <tr>
                        <td>G</td>
                        <td>Citizen of Europe, entitled to vote in local elections only</td>
                    </tr>
                    <tr>
                        <td>HO</td>
                        <td>Husband’s occupational qualification</td>
                    </tr>
                    <tr>
                        <td>J</td>
                        <td>Eligible for jury service</td>
                    </tr>
                    <tr>
                        <td>JS</td>
                        <td>Special juror (as defined by the Jurors’ Act 1870 i.e. esquire or above, bankers, merchants, and anyone owning property of more than a certain value)</td>
                    </tr>
                    <tr>
                        <td>K</td>
                        <td>Citizen of Europe, entitled to vote in local and European elections only</td>
                    </tr>
                    <tr>
                        <td>L</td>
                        <td>Not entitled to vote in parliamentary elections</td>
                    </tr>
                    <tr>
                        <td>M</td>
                        <td>Merchant seamen</td>
                    </tr>
                    <tr>
                        <td>N</td>
                        <td>To be included in the next register as a voter</td>
                    </tr>
                    <tr>
                        <td>NM</td>
                        <td>Naval or military voter</td>
                    </tr>
                    <tr>
                        <td>O (Ow)</td>
                        <td>Occupational qualification (women)</td>
                    </tr>
                    <tr>
                        <td>P</td>
                        <td>Proxy voter</td>
                    </tr>
                    <tr>
                        <td>R (Rw)</td>
                        <td>Residential qualification (women)</td>
                    </tr>
                    <tr>
                        <td>S</td>
                        <td>Service (military) voter</td>
                    </tr>
                    <tr>
                        <td>U</td>
                        <td>Citizen of Europe, entitled to vote in European elections only</td>
                    </tr>
                    <tr>
                        <td>X</td>
                        <td>Not entitled to vote in Parliamentary elections</td>
                    </tr>
                    <tr>
                        <td>Y</td>
                        <td>Entitled to vote in the following year</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer">This page serves as a reference to the "Elector Markers" in the electoral register.</div>
    </div>
@endsection
