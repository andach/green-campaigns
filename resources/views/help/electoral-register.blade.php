@extends('template')

@section('title')
    Help - Getting Started
@endsection

@section('content')
    <p><a href="{{ route('help') }}">Back to the main help page</a></p>
    <p><hr /></p>

    <h2>How to Use This</h2>
    <p>To get started, you'll need to have your council and electoral register set up. For the moment, this can only be done by Andreas - to ask that this be
        done, please get in touch at <a href="mailto:andreas@useaquestion.co.uk">andreas@useaquestion.co.uk</a>.</p>

@endsection
