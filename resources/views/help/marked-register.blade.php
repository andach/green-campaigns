@extends('template')

@section('title')
    Help - Marked Register
@endsection

@section('content')
    <p><a href="{{ route('help') }}">Back to the main help page</a></p>
    <p><hr /></p>

    <div class="card">
        <div class="card-header">Preparation of the Marked Register</div>
        <div class="card-body">
            <h2>What does the System Require?</h2>
            <p>To upload the marked register, you need to prepare a CSV file and email this to <a href="andreas@useaquestion.co.uk">andreas@useaquestion.co.uk</a>
                This file needs to contain a single column of full elector numbers (which will look like AA1-123). That's it.</p>

            <h2>How to Prepare the Marked Register</h2>
            <p>You should request your marked register from the council after the election. The returning officer or democracy team will be able to help
                you with how to request this. </p>
            <p>The marked register is a literal scanned copy of the paper pages from the polling stations, one PDF file for each electoral area.
                To indicate that someone has voted, there will be a <a href="/img/help/marked-register-example.png">horizontal line next to their
                name</a>. Absent (i.e. postal) voters will be indicated with an "A" and their name will appear crossed out. DO NOT include
                these in the marked register - they are accounted for elsewhere.</p>
            <p>Unfortunately, there is no way to prepare the marked register other than to manually record each voter number. Using Excel
                you can <a href="/img/help/example-marked-register.png">speed up this process</a> by having one column which just contains the
                electoral area, one column in which you type the voter numbers, and a third column using the formula
                <code>CONCAT(A1,"-",B1)</code>. </p>
            <p><a href="/download/example-marked-register.xlsx">Download a sample excel file here</a></p>

        </div>
        <div class="card-footer">Please note any data in images shown in these help topics has been faked or anonymised to comply with GDPR.</div>
    </div>

@endsection
