@extends('template')

@section('title')
    Help - Getting Started
@endsection

@section('content')
    <p><a href="{{ route('help') }}">Back to the main help page</a></p>
    <p><hr /></p>

    <div class="card">
        <div class="card-header">Introduction to the System</div>
        <div class="card-body">
            <h2>How to Use This</h2>
            <p>To get started, you'll need to have your council and electoral register set up. For the moment, this can only be done by Andreas - to ask that this be
                done, please get in touch at <a href="mailto:andreas@useaquestion.co.uk">andreas@useaquestion.co.uk</a>.</p>
            <p>This is currently in development and may have some bugs.</p>

            <h2>Data has been Added - Now What?</h2>
            <p>You can either canvass using printed canvass sheets, which allow you to enter and update an entire road at a time, or by taking a phone/tablet out
                when canvassing, entering information as it is collected.</p>
            <p>Either way, to to the <a href="{{ route('ward.index') }}">list of wards</a> - which shows all wards in councils you have acceess to. For GDPR reasons,
                you are not able to access data out of assigned local councils. Select the ward to get a list of every road in it, and you'll see
                <a href="/img/help/ward-page.png">a page like this</a>. If this is your first time, the pie charts at the top won't show anything useful.</p>
            <p>Then, select the road(s) that you want to canvass. This will give you a list of information, and you can either click on the "printable canvass sheets"
                link at the top of this page, or click on any house name/number to enter data on the move. The links are
                <a href="/img/help/road-page.png">circled in this picture</a>.</p>

            <h2>Canvassing Using Printed Sheets</h2>
            <p>The printed canvass sheets page <a href="/img/help/printable-canvass-sheets.png">looks like this</a>. You can easily print this portrait on A4 paper
                and take these out with you on a clipboard. As you can see, the sheets will contain the previous voting information (if entered), which will help
                your canvassers at the doors. Record voting intention by writing it down, then when you're back at the PC, enter this all in one page.</p>
            <p>Remember to securely dispose of the printed canvass sheets once you've finished entering them into the system!</p>

            <h2>Canvassing on the Move</h2>
            <p>If you clicked on a house number, you'll see <a href="/img/help/direct-canvass-page.png">the direct canvassing page</a>. Here you can enter canvass
                data, which will be immediately saved as soon as you move to the next house using the buttons at the bottom. If no-one is in, then you can just
                leave these boxes blank, and nothing will be updated or changed. </p>
            <p>The "next houses" link might not work with named houses, which can include care homes or odd houses on the end of streets. It might also
                work a little oddly with flats or houses like 1A and 1B. For this reason, while the system is in development, I recommend you also go out with
                printed canvass sheets, just so you're not messing around with a tablet on the move.</p>
            <p>By default, the "next" houses will be two houses "up" and two houses "down" in numerical order. So if you're canvassing number 150, the boxes at the
                bottom will allow you to save the data and move to 148, 149, 151 and 152. </p>
        </div>
    </div>
@endsection
