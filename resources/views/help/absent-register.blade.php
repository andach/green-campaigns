@extends('template')

@section('title')
    Help - Absent Register
@endsection

@section('content')
    <p><a href="{{ route('help') }}">Back to the main help page</a></p>
    <p><hr /></p>

    <div class="card">
        <div class="card-header">Preparation of the Absent Register</div>
        <div class="card-body">
            <h2>What does the System Require?</h2>
            <p>To upload the absent register, you need to prepare a CSV file and email this to <a href="andreas@useaquestion.co.uk">andreas@useaquestion.co.uk</a>
                This file needs to contain a single column of full elector numbers (which will look like AA1-123). That's it.</p>
            <p>If you are confident the simple process of removing duplicates in Excel columns, this will be very easy. The file you provide should
                <a href="/img/help/absent-register-output.png">look like this</a> (although obviously with a lot more data in it!).</p>

            <h2>How to Prepare the Absent Register</h2>
            <p>You should request the absent register from your local council after the elections have happened. There is a charge for this service, and the
                register should be provided in Excel and PDF form. To prepare the register for upload, you only care about the excel document.
                <a href="/img/help/absent-voter-1.png">It should look something like this.</a></p>
            <p>If this file has only one tab of data in it, delete everything except the first column, then
                <a href="/img/help/absent-voter-2.png">remove duplicates</a> from it. In the Excel ribbon, select "Data", then "remove duplicates" at
                the top (the image shows where this is). Then just save the file as a CSV and you're done. </p>

            <h2>What if the electoral register is over two sheets?</h2>
            <p>In this case, just remove duplicates from the first column in each sheet, paste them all into a single column in a new tab, and save
                this tab as a CSV file.</p>
        </div>
        <div class="card-footer">Please note any data in images shown in these help topics has been faked or anonymised to comply with GDPR.</div>
    </div>

@endsection
