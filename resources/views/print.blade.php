
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Green Party</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bebas+Neue">
    <link rel="stylesheet" href="/css/app.css">
</head>

<body style="padding-top: 0">

    <h1>@yield('title', 'Derbyshire Green Party Canvassing Application')</h1>

    @hasSection('infobox')
        <div class="alert alert-info">
            @yield('infobox')
        </div>
    @endif

    @yield('content')
</body>
</html>
