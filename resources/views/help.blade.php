@extends('template')

@section('title')
    Help
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Help Documents and References</div>
        <div class="card-body">
            <h2>Start Here</h2>
            <p><a href="{{ route('help.getting-started') }}">Getting Started?</a></p>
            <h2>Preparing the registers</h2>
            <p><a href="{{ route('help.absent-register') }}">Absent Register</a></p>
            <p><a href="{{ route('help.electoral-register') }}">Electoral Register</a></p>
            <p><a href="{{ route('help.marked-register') }}">Marked Register</a></p>
            <h2>General Reference</h2>
            <p><a href="{{ route('help.elector-markers') }}">Elector Markers</a></p>
            <p><hr /></p>
            <p>This software is being written by Andreas Christodoulou, a Green Party member from the Derbyshire Party.</p>
            <p>For any queries or comments, please email <a href="mailto:andreas@useaquestion.co.uk">andreas@useaquestion.co.uk</a>.</p>
        </div>
    </div>
@endsection
