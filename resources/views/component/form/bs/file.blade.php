@php
    $class = 'form-control';
    if ($errors->has($name))
    {
        $class = 'form-control is-invalid';
    }

    $labelColumns   = $attributes['labelCols'] ?? 'col-12 col-md-3';
    $elementColumns = $attributes['elementCols'] ?? 'col-12 col-md-9';
    $helpBlockName  = $name.'HelpBlock';
@endphp

<div class="form-group row">
    {{ Form::label($title, null, ['class' => $labelColumns.' col-form-label']) }}
    <div class="{{ $elementColumns }}">
        {{ Form::file($name, array_merge(['class' => $class], $attributes)) }}

        @if (isset($attributes['helpBlock']))
            <small id="{{ $helpBlockName }}" class="form-text">
                {!! $attributes['helpBlock'] !!}
            </small>
        @endif

        @if ($errors->has($name))
            <div class="invalid-feedback">
                Error: {{ $errors->first($name) }}
            </div>
        @endif
    </div>
</div>
