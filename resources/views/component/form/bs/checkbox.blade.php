@php
    $class = 'form-check-input';
    if ($errors->has($name))
    {
        $class = 'form-check-input is-invalid';
    }

    $labelColumns   = $attributes['labelCols'] ?? 'col-12 col-md-3';
    $elementColumns = $attributes['elementCols'] ?? 'col-12 col-md-9';
    $helpBlockName  = $name.'HelpBlock';
@endphp

<div class="form-group row">
    {{ Form::label($name, $title, ['class' => $labelColumns.' col-form-label']) }}
    <div class="{{ $elementColumns }}">
        @if (old($name))
            {{ Form::checkbox($name, old($name), $checked, array_merge(['class' => $class], $attributes)) }}
        @else
            {{ Form::checkbox($name, $value, $checked, array_merge(['class' => $class], $attributes)) }}
        @endif

        @if (isset($attributes['helpBlock']))
            <small id="{{ $helpBlockName }}" class="form-text">
                {!! $attributes['helpBlock'] !!}
            </small>
        @endif

        @if ($errors->has($name))
            <div class="invalid-feedback">
                Error: {{ $errors->first($name) }}
            </div>
        @endif
    </div>
</div>
