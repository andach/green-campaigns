<script nonce="{{ csp_nonce() }}">
    var ctx = document.getElementById('{{ $htmlID }}').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',

        options: {
            plugins: {
                title: {
                    display: true,
                    text: '{{ $title }}'
                },
            },
            responsive: true,
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true,
                    ticks: {
                        stepSize: 1
                    }
                }
            }
        },

        data: {
            labels: [
                '{!! implode("','", $labels) !!}'
            ],

            datasets: [
                @foreach ($data as $key => $subArray)
                    {
                        label: '{{ $key }}',
                        data: [{{ implode(',', $subArray)}}],
                        backgroundColor: '{{ $colors[$key] }}',
                        hoverOffset: 4
                    },
                @endforeach
            ],
        }
    });
</script>
