<script nonce="{{ csp_nonce() }}">
    var ctx = document.getElementById('{{ $htmlID }}').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [
                '{!! implode("','", array_keys($data)) !!}'
            ],
            datasets: [{
                label: 'Data',
                data: [{{ implode(',', $data)}}],
                backgroundColor: [
                    '{!! implode("','", array_merge($data, array_intersect_key($colors, $data))) !!}'
                ],
                hoverOffset: 4
            }]
        }
    });
</script>
