<script nonce="{{ csp_nonce() }}">
    var ctx = document.getElementById('{{ $htmlID }}').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',

        options: {
            plugins: {
                title: {
                    display: true,
                    text: '{{ $title }}'
                },
            },
            responsive: true,
            scales: {
                y: {
                    ticks: {
                        stepSize: 1
                    }
                }
            }
        },

        data: {
            labels: [
                '{!! implode("','", array_keys($data)) !!}'
            ],

            datasets: [{
                label: 'Data',
                data: [{{ implode(',', $data)}}],
                hoverOffset: 4
            }]

        }
    });
</script>
