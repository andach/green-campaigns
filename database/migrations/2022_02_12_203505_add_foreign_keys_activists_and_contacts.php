<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysActivistsAndContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activists', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('activist_id')->references('id')->on('activists');
            $table->bigInteger('event_id')->unsigned()->change();
            $table->foreign('event_id')->references('id')->on('events');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('link_activists_events', function (Blueprint $table) {
            $table->foreign('activist_id')->references('id')->on('activists');
            $table->bigInteger('event_id')->unsigned()->change();
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Have to do these in separate steps because the foreign key stops modification of the column itself
        Schema::table('activists', function (Blueprint $table) {
            $table->dropForeign('activists_council_id_foreign');
        });
        Schema::table('activists', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign('contacts_activist_id_foreign');
            $table->dropForeign('contacts_event_id_foreign');
            $table->dropForeign('contacts_user_id_foreign');
        });
        Schema::table('contacts', function (Blueprint $table) {
            $table->integer('event_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign('events_council_id_foreign');
            $table->dropForeign('events_user_id_foreign');
        });
        Schema::table('events', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('link_activists_events', function (Blueprint $table) {
            $table->dropForeign('link_activists_events_activist_id_foreign');
            $table->dropForeign('link_activists_events_event_id_foreign');
        });
        Schema::table('link_activists_events', function (Blueprint $table) {
            $table->integer('event_id')->signed()->change();
        });
    }
}
