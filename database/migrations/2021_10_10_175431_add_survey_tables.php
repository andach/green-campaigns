<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSurveyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->integer('user_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_issues', function (Blueprint $table) {
            $table->id();
            $table->integer('survey_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_questions', function (Blueprint $table) {
            $table->id();
            $table->integer('survey_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_questions_options', function (Blueprint $table) {
            $table->id();
            $table->integer('question_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_responses', function (Blueprint $table) {
            $table->id();
            $table->integer('address_id')->nullable();
            $table->integer('survey_id');
            $table->integer('ward_id');
            $table->integer('voter_id')->nullable();
            $table->date('date_of_response');
            $table->text('other_issues')->nullable();
            $table->string('signup_name')->nullable();
            $table->string('signup_email')->nullable();
            $table->string('signup_phone')->nullable();
            $table->boolean('volunteer_deliver')->nullable();
            $table->boolean('volunteer_vote')->nullable();
            $table->boolean('volunteer_poster')->nullable();
            $table->boolean('volunteer_other')->nullable();
            $table->text('volunteer_text')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_responses_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('question_id');
            $table->integer('response_id');
            $table->integer('option_id')->nullable();
            $table->string('additional_text')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_responses_issues', function (Blueprint $table) {
            $table->id();
            $table->integer('issue_id');
            $table->integer('response_id');
            $table->integer('answer');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('surveys_responses_parties', function (Blueprint $table) {
            $table->id();
            $table->integer('response_id');
            $table->string('enum_political_party');
            $table->integer('answer');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
        Schema::dropIfExists('surveys_issues');
        Schema::dropIfExists('surveys_questions');
        Schema::dropIfExists('surveys_questions_options');
        Schema::dropIfExists('surveys_responses');
        Schema::dropIfExists('surveys_responses_answers');
        Schema::dropIfExists('surveys_responses_issues');
        Schema::dropIfExists('surveys_responses_issues_other');
        Schema::dropIfExists('surveys_responses_parties');
    }
}
