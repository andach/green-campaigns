<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->integer('road_id');
            $table->integer('route_id')->nullable();
            $table->integer('ward_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('canvasses', function (Blueprint $table) {
            $table->id();
            $table->integer('address_id');
            $table->integer('council_id');
            $table->integer('voter_id');
            $table->integer('ward_id');
            $table->integer('user_id');
            $table->string('council_enum_generally_votes')->nullable();
            $table->string('council_enum_vote_green_likelihood')->nullable();
            $table->string('parl_enum_generally_votes')->nullable();
            $table->string('parl_enum_vote_green_likelihood')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('caseworks', function (Blueprint $table) {
            $table->id();
            $table->integer('address_id');
            $table->integer('council_id');
            $table->integer('voter_id');
            $table->integer('ward_id');
            $table->integer('user_id');
            $table->string('name');
            $table->boolean('is_closed')->nullable();;
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('caseworks_comments', function (Blueprint $table) {
            $table->id();
            $table->integer('casework_id');
            $table->integer('user_id');
            $table->string('attachment')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('councils', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('elections', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('link_addresses_councils', function (Blueprint $table) {
            $table->id();
            $table->integer('address_id');
            $table->integer('council_id');
            $table->timestamps();
        });

        Schema::create('link_councils_users', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('link_elections_voters', function (Blueprint $table) {
            $table->id();
            $table->integer('election_id');
            $table->integer('voter_id');
            $table->timestamps();
        });

        Schema::create('roads', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->integer('route_id')->nullable();
            $table->integer('ward_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('routes', function (Blueprint $table) {
            $table->id();
            $table->integer('ward_id');
            $table->string('name');
            $table->timestamps();
			$table->softDeletes();
        });

        Schema::create('voters', function (Blueprint $table) {
            $table->id();
            $table->integer('address_id');
            $table->integer('council_id');
            $table->integer('road_id');
            $table->integer('route_id')->nullable();
            $table->integer('ward_id');
            $table->date('date_of_electoral_register')->nullable();
            $table->string('forename')->nullable();
            $table->string('surname')->nullable();
            $table->string('is_postal')->nullable();
            $table->string('council_enum_generally_votes_latest')->nullable();
            $table->string('council_enum_vote_green_likelihood_latest')->nullable();
            $table->string('parl_enum_generally_votes_latest')->nullable();
            $table->string('parl_enum_vote_green_likelihood_latest')->nullable();
            $table->string('elector_number_full')->nullable();
            $table->string('elector_number_prefix')->nullable();
            $table->integer('elector_number')->nullable();
            $table->integer('elector_number_suffix')->nullable();
            $table->string('elector_markers')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['address_id', 'council_id', 'road_id', 'ward_id', 'forename', 'surname', 'elector_number_prefix', 'elector_number', 'elector_number_suffix', 'elector_markers'], 'dummy_index');
        });

        Schema::create('wards', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->string('name');
            $table->string('electoral_prefix');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('users')->insert([
            'name'              => 'Admin',
            'email'             => 'admin@demo.com',
            'password'          => Hash::make('password'),
            'is_super_admin'    => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('canvasses');
        Schema::dropIfExists('caseworks');
        Schema::dropIfExists('caseworks_comments');
        Schema::dropIfExists('councils');
        Schema::dropIfExists('elections');
        Schema::dropIfExists('link_addresses_councils');
        Schema::dropIfExists('link_councils_users');
        Schema::dropIfExists('link_elections_voters');
        Schema::dropIfExists('roads');
        Schema::dropIfExists('routes');
        Schema::dropIfExists('voters');
        Schema::dropIfExists('wards');
    }
}
