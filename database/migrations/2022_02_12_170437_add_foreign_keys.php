<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
        });

        Schema::table('canvasses', function (Blueprint $table) {
            $table->bigInteger('address_id')->unsigned()->change();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('voter_id')->unsigned()->change();
            $table->foreign('voter_id')->references('id')->on('voters');
            $table->bigInteger('ward_id')->unsigned()->change();
            $table->foreign('ward_id')->references('id')->on('wards');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('caseworks', function (Blueprint $table) {
            $table->bigInteger('address_id')->unsigned()->change();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('voter_id')->unsigned()->change();
            $table->foreign('voter_id')->references('id')->on('voters');
            $table->bigInteger('ward_id')->unsigned()->change();
            $table->foreign('ward_id')->references('id')->on('wards');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('caseworks_comments', function (Blueprint $table) {
            $table->bigInteger('casework_id')->unsigned()->change();
            $table->foreign('casework_id')->references('id')->on('caseworks');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('elections', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
        });

        Schema::table('link_addresses_councils', function (Blueprint $table) {
            $table->bigInteger('address_id')->unsigned()->change();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
        });

        Schema::table('link_councils_users', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('link_elections_voters', function (Blueprint $table) {
            $table->bigInteger('election_id')->unsigned()->change();
            $table->foreign('election_id')->references('id')->on('elections');
            $table->bigInteger('voter_id')->unsigned()->change();
            $table->foreign('voter_id')->references('id')->on('voters');
        });

        Schema::table('roads', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('route_id')->unsigned()->change();
            $table->foreign('route_id')->references('id')->on('routes');
            $table->bigInteger('ward_id')->unsigned()->change();
            $table->foreign('ward_id')->references('id')->on('wards');
        });

        Schema::table('routes', function (Blueprint $table) {
            $table->bigInteger('ward_id')->unsigned()->change();
            $table->foreign('ward_id')->references('id')->on('wards');
        });

        Schema::table('voters', function (Blueprint $table) {
            $table->bigInteger('address_id')->unsigned()->change();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('road_id')->unsigned()->change();
            $table->foreign('road_id')->references('id')->on('roads');
            $table->bigInteger('route_id')->unsigned()->change();
            $table->foreign('route_id')->references('id')->on('routes');
            $table->bigInteger('ward_id')->unsigned()->change();
            $table->foreign('ward_id')->references('id')->on('wards');
        });

        Schema::table('wards', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Have to do these in separate steps because the foreign key stops modification of the column itself
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign('addresses_council_id_foreign');
        });
        Schema::table('addresses', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
        });

        Schema::table('canvasses', function (Blueprint $table) {
            $table->dropForeign('canvasses_address_id_foreign');
            $table->dropForeign('canvasses_council_id_foreign');
            $table->dropForeign('canvasses_voter_id_foreign');
            $table->dropForeign('canvasses_ward_id_foreign');
            $table->dropForeign('canvasses_user_id_foreign');
        });
        Schema::table('canvasses', function (Blueprint $table) {
            $table->integer('address_id')->signed()->change();
            $table->integer('council_id')->signed()->change();
            $table->integer('voter_id')->signed()->change();
            $table->integer('ward_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('caseworks', function (Blueprint $table) {
            $table->dropForeign('caseworks_address_id_foreign');
            $table->dropForeign('caseworks_council_id_foreign');
            $table->dropForeign('caseworks_voter_id_foreign');
            $table->dropForeign('caseworks_ward_id_foreign');
            $table->dropForeign('caseworks_user_id_foreign');
        });
        Schema::table('caseworks', function (Blueprint $table) {
            $table->integer('address_id')->signed()->change();
            $table->integer('council_id')->signed()->change();
            $table->integer('voter_id')->signed()->change();
            $table->integer('ward_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('caseworks_comments', function (Blueprint $table) {
            $table->dropForeign('caseworks_comments_casework_id_foreign');
            $table->dropForeign('caseworks_comments_user_id_foreign');
        });
        Schema::table('caseworks_comments', function (Blueprint $table) {
            $table->integer('casework_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('elections', function (Blueprint $table) {
            $table->dropForeign('elections_council_id_foreign');
        });
        Schema::table('elections', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
        });

        Schema::table('link_addresses_councils', function (Blueprint $table) {
            $table->dropForeign('link_addresses_councils_address_id_foreign');
            $table->dropForeign('link_addresses_councils_council_id_foreign');
        });
        Schema::table('link_addresses_councils', function (Blueprint $table) {
            $table->integer('address_id')->signed()->change();
            $table->integer('council_id')->signed()->change();
        });

        Schema::table('link_councils_users', function (Blueprint $table) {
            $table->dropForeign('link_councils_users_council_id_foreign');
            $table->dropForeign('link_councils_users_user_id_foreign');
        });
        Schema::table('link_councils_users', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('link_elections_voters', function (Blueprint $table) {
            $table->dropForeign('link_elections_voters_election_id_foreign');
            $table->dropForeign('link_elections_voters_voter_id_foreign');
        });
        Schema::table('link_elections_voters', function (Blueprint $table) {
            $table->integer('election_id')->signed()->change();
            $table->integer('voter_id')->signed()->change();
        });

        Schema::table('roads', function (Blueprint $table) {
            $table->dropForeign('roads_council_id_foreign');
            $table->dropForeign('roads_route_id_foreign');
            $table->dropForeign('roads_ward_id_foreign');
        });
        Schema::table('roads', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
            $table->integer('route_id')->signed()->change();
            $table->integer('ward_id')->signed()->change();
        });

        Schema::table('routes', function (Blueprint $table) {
            $table->dropForeign('routes_ward_id_foreign');
        });
        Schema::table('routes', function (Blueprint $table) {
            $table->integer('ward_id')->signed()->change();
        });

        Schema::table('voters', function (Blueprint $table) {
            $table->dropForeign('voters_address_id_foreign');
            $table->dropForeign('voters_council_id_foreign');
            $table->dropForeign('voters_road_id_foreign');
            $table->dropForeign('voters_route_id_foreign');
            $table->dropForeign('voters_ward_id_foreign');
        });
        Schema::table('voters', function (Blueprint $table) {
            $table->integer('address_id')->signed()->change();
            $table->integer('council_id')->signed()->change();
            $table->integer('road_id')->signed()->change();
            $table->integer('route_id')->signed()->change();
            $table->integer('ward_id')->signed()->change();
        });

        Schema::table('wards', function (Blueprint $table) {
            $table->dropForeign('wards_council_id_foreign');
        });
        Schema::table('wards', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
        });
    }
}
