<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivistsAndContactsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activists', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('council_id');
            $table->string('forename');
            $table->string('surname');
            $table->string('email');
            $table->string('mobile_number');
            $table->string('phone_number');
            $table->boolean('do_not_phone');
            $table->string('address');
            $table->string('city');
            $table->string('postcode');
            $table->string('council_ward');
            $table->string('local_authority');
            $table->string('constituency');
            $table->boolean('is_member');
            $table->date('membership_start_date')->nullable();
            $table->date('membership_end_date')->nullable();
            $table->string('membership_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->uuid('activist_id');
            $table->integer('event_id')->nullable();
            $table->integer('user_id');
            $table->date('date_of_contact');
            $table->string('enum_contact_type');
            $table->boolean('managed_to_contact');
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->integer('council_id');
            $table->integer('user_id');
            $table->date('date_of_event');
            $table->string('name');
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('link_activists_events', function (Blueprint $table) {
            $table->id();
            $table->uuid('activist_id');
            $table->integer('event_id');
            $table->boolean('is_attending');
            $table->boolean('did_attend')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activists');
        Schema::dropIfExists('contacts');
        Schema::dropIfExists('events');
        Schema::dropIfExists('link_activists_events');
    }
}
