<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysSurveys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->change();
            $table->foreign('council_id')->references('id')->on('councils');
            $table->bigInteger('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('surveys_issues', function (Blueprint $table) {
            $table->bigInteger('survey_id')->unsigned()->change();
            $table->foreign('survey_id')->references('id')->on('surveys');
        });

        Schema::table('surveys_questions', function (Blueprint $table) {
            $table->bigInteger('survey_id')->unsigned()->change();
            $table->foreign('survey_id')->references('id')->on('surveys');
        });

        Schema::table('surveys_questions_options', function (Blueprint $table) {
            $table->bigInteger('question_id')->unsigned()->change();
            $table->foreign('question_id')->references('id')->on('surveys_questions');
        });

        Schema::table('surveys_responses', function (Blueprint $table) {
            $table->bigInteger('address_id')->unsigned()->change();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->bigInteger('survey_id')->unsigned()->change();
            $table->foreign('survey_id')->references('id')->on('surveys');
            $table->bigInteger('ward_id')->unsigned()->change();
            $table->foreign('ward_id')->references('id')->on('wards');
            $table->bigInteger('voter_id')->unsigned()->change();
            $table->foreign('voter_id')->references('id')->on('voters');
        });

        Schema::table('surveys_responses_answers', function (Blueprint $table) {
            $table->bigInteger('question_id')->unsigned()->change();
            $table->foreign('question_id')->references('id')->on('surveys_questions');
            $table->bigInteger('response_id')->unsigned()->change();
            $table->foreign('response_id')->references('id')->on('surveys_responses');
            $table->bigInteger('option_id')->unsigned()->change();
            $table->foreign('option_id')->references('id')->on('surveys_questions_options');
        });

        Schema::table('surveys_responses_issues', function (Blueprint $table) {
            $table->bigInteger('issue_id')->unsigned()->change();
            $table->foreign('issue_id')->references('id')->on('surveys_issues');
            $table->bigInteger('response_id')->unsigned()->change();
            $table->foreign('response_id')->references('id')->on('surveys_responses');
        });

        Schema::table('surveys_responses_parties', function (Blueprint $table) {
            $table->bigInteger('response_id')->unsigned()->change();
            $table->foreign('response_id')->references('id')->on('surveys_responses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Have to do these in separate steps because the foreign key stops modification of the column itself
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropForeign('surveys_council_id_foreign');
            $table->dropForeign('surveys_user_id_foreign');
        });
        Schema::table('surveys', function (Blueprint $table) {
            $table->integer('council_id')->signed()->change();
            $table->integer('user_id')->signed()->change();
        });

        Schema::table('surveys_issues', function (Blueprint $table) {
            $table->dropForeign('surveys_issues_survey_id_foreign');
        });
        Schema::table('surveys_issues', function (Blueprint $table) {
            $table->integer('survey_id')->signed()->change();
        });

        Schema::table('surveys_questions', function (Blueprint $table) {
            $table->dropForeign('surveys_questions_survey_id_foreign');
        });
        Schema::table('surveys_questions', function (Blueprint $table) {
            $table->integer('survey_id')->signed()->change();
        });

        Schema::table('surveys_questions_options', function (Blueprint $table) {
            $table->dropForeign('surveys_questions_options_question_id_foreign');
        });
        Schema::table('surveys_questions_options', function (Blueprint $table) {
            $table->integer('question_id')->signed()->change();
        });

        Schema::table('surveys_responses', function (Blueprint $table) {
            $table->dropForeign('surveys_responses_address_id_foreign');
            $table->dropForeign('surveys_responses_survey_id_foreign');
            $table->dropForeign('surveys_responses_ward_id_foreign');
            $table->dropForeign('surveys_responses_voter_id_foreign');
        });
        Schema::table('surveys_responses', function (Blueprint $table) {
            $table->integer('address_id')->signed()->change();
            $table->integer('survey_id')->signed()->change();
            $table->integer('ward_id')->signed()->change();
            $table->integer('voter_id')->signed()->change();
        });

        Schema::table('surveys_responses_answers', function (Blueprint $table) {
            $table->dropForeign('surveys_responses_answers_question_id_foreign');
            $table->dropForeign('surveys_responses_answers_response_id_foreign');
            $table->dropForeign('surveys_responses_answers_option_id_foreign');
        });
        Schema::table('surveys_responses_answers', function (Blueprint $table) {
            $table->integer('question_id')->signed()->change();
            $table->integer('response_id')->signed()->change();
            $table->integer('option_id')->signed()->change();
        });

        Schema::table('surveys_responses_issues', function (Blueprint $table) {
            $table->dropForeign('surveys_responses_issues_issue_id_foreign');
            $table->dropForeign('surveys_responses_issues_response_id_foreign');
        });
        Schema::table('surveys_responses_issues', function (Blueprint $table) {
            $table->integer('issue_id')->signed()->change();
            $table->integer('response_id')->signed()->change();
        });

        Schema::table('surveys_responses_parties', function (Blueprint $table) {
            $table->dropForeign('surveys_responses_parties_response_id_foreign');
        });
        Schema::table('surveys_responses_parties', function (Blueprint $table) {
            $table->integer('response_id')->signed()->change();
        });
    }
}
