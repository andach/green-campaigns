<?php

namespace Database\Seeders;

use DB;
use Hash;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /***************************
         *  USERS
         **************************/

        DB::table('users')->insert(['name' => 'Derby Admin', 'email' => 'derbyadmin@demo.com', 'password' => Hash::make('password')]);
        DB::table('users')->insert(['name' => 'Derby User', 'email' => 'derbyuser@demo.com', 'password' => Hash::make('password')]);
        DB::table('users')->insert(['name' => 'Nottingham', 'email' => 'nottingham@demo.com', 'password' => Hash::make('password')]);

        DB::table('councils')->insert(['name' => 'Derby City Council']);
        DB::table('councils')->insert(['name' => 'Nottingham City Council']);

        DB::table('wards')->insert([
            ['council_id' => 1, 'electoral_prefix' => 'AB', 'name' => 'Abbey'],
            ['council_id' => 1, 'electoral_prefix' => 'AL', 'name' => 'Allestree'],
            ['council_id' => 1, 'electoral_prefix' => 'AV', 'name' => 'Alvaston'],
            ['council_id' => 1, 'electoral_prefix' => 'AR', 'name' => 'Arboretum'],
            ['council_id' => 1, 'electoral_prefix' => 'BL', 'name' => 'Blagreaves'],

            ['council_id' => 1, 'electoral_prefix' => 'BO', 'name' => 'Boulton'],
            ['council_id' => 1, 'electoral_prefix' => 'CA', 'name' => 'Chaddesden'],
            ['council_id' => 1, 'electoral_prefix' => 'CE', 'name' => 'Chellaston'],
            ['council_id' => 1, 'electoral_prefix' => 'DA', 'name' => 'Darley'],
            ['council_id' => 1, 'electoral_prefix' => 'DE', 'name' => 'Derwent'],

            ['council_id' => 1, 'electoral_prefix' => 'LI', 'name' => 'Littleover'],
            ['council_id' => 1, 'electoral_prefix' => 'MA', 'name' => 'Mackworth'],
            ['council_id' => 1, 'electoral_prefix' => 'MI', 'name' => 'Mickleover'],
            ['council_id' => 1, 'electoral_prefix' => 'NO', 'name' => 'Normanton'],
            ['council_id' => 1, 'electoral_prefix' => 'OA', 'name' => 'Oakwood'],

            ['council_id' => 1, 'electoral_prefix' => 'SI', 'name' => 'Sinfin'],
            ['council_id' => 1, 'electoral_prefix' => 'SP', 'name' => 'Spondon'],
            ['council_id' => 2, 'electoral_prefix' => 'AA', 'name' => 'Nottingham Ward'],
        ]);

        DB::table('roads')->insert(['council_id' => 1, 'ward_id' => 1, 'name' => 'Test Road']);

        DB::table('link_councils_users')->insert(['council_id' => 1, 'user_id' => 2]);
        DB::table('link_councils_users')->insert(['council_id' => 1, 'user_id' => 3]);
        DB::table('link_councils_users')->insert(['council_id' => 2, 'user_id' => 4]);

        /***************************
         *  DIMENSIONS
         **************************/

        DB::table('addresses')->insert([
            ['road_id' => 1, 'ward_id' => 1, 'name' => '1', 'postcode' => 'AB12CD', 'council_id' => 1],
            ['road_id' => 1, 'ward_id' => 1, 'name' => '2', 'postcode' => 'AB12CD', 'council_id' => 1],
            ['road_id' => 1, 'ward_id' => 1, 'name' => '3', 'postcode' => 'AB12CD', 'council_id' => 1],
        ]);

        DB::table('elections')->insert([
            ['council_id' => 1, 'name' => 'Derby City Council Election 2020', 'year' => 2020],
            ['council_id' => 1, 'name' => 'Derby City Council Election 2021', 'year' => 2021],
        ]);

        DB::table('link_addresses_councils')->insert([
            ['address_id' => 1, 'council_id' => 1],
            ['address_id' => 2, 'council_id' => 1],
            ['address_id' => 3, 'council_id' => 1],
        ]);

        /***************************
         *  VOTERS
         **************************/

        DB::table('voters')->insert([
            ['address_id' => 1, 'council_id' => 1, 'road_id' => 1, 'ward_id' => 1, 'forename' => 'Adam', 'surname' => 'Adams', 'is_postal' => 1, 'council_enum_generally_votes_latest' => 'GREEN', 'council_enum_vote_green_likelihood_latest' => 'FIVE', 'parl_enum_generally_votes_latest' => 'LAB', 'parl_enum_vote_green_likelihood_latest' => 'THREE', 'elector_number_full' => 'AB1-1'],
            ['address_id' => 2, 'council_id' => 1, 'road_id' => 1, 'ward_id' => 1, 'forename' => 'Bob', 'surname' => 'Boblington', 'is_postal' => 1, 'council_enum_generally_votes_latest' => 'CON', 'council_enum_vote_green_likelihood_latest' => 'ZERO', 'parl_enum_generally_votes_latest' => 'CON', 'parl_enum_vote_green_likelihood_latest' => 'ZERO', 'elector_number_full' => 'AB1-2'],
        ]);

        DB::table('voters')->insert([
            ['address_id' => 2, 'council_id' => 1, 'road_id' => 1, 'ward_id' => 1, 'forename' => 'Bobina', 'surname' => 'Boblington', 'elector_number_full' => 'AB1-3'],
            ['address_id' => 3, 'council_id' => 1, 'road_id' => 1, 'ward_id' => 1, 'forename' => 'Christy', 'surname' => 'Clarke', 'elector_number_full' => 'AB1-4'],
            ['address_id' => 3, 'council_id' => 1, 'road_id' => 1, 'ward_id' => 1, 'forename' => 'Charles', 'surname' => 'Clarke', 'elector_number_full' => 'AB1-5'],
            ['address_id' => 3, 'council_id' => 1, 'road_id' => 1, 'ward_id' => 1, 'forename' => 'Cindy', 'surname' => 'Clarke', 'elector_number_full' => 'AB1-6'],
        ]);

        DB::table('canvasses')->insert([
            ['address_id' => 1, 'council_id' => 1, 'voter_id' => 1, 'ward_id' => 1, 'user_id' => 1, 'council_enum_generally_votes' => 'GREEN', 'council_enum_vote_green_likelihood' => 'FIVE', 'parl_enum_generally_votes' => 'LAB', 'parl_enum_vote_green_likelihood' => 'THREE'],
            ['address_id' => 2, 'council_id' => 1, 'voter_id' => 2, 'ward_id' => 1, 'user_id' => 1, 'council_enum_generally_votes' => 'CON', 'council_enum_vote_green_likelihood' => 'ZERO', 'parl_enum_generally_votes' => 'CON', 'parl_enum_vote_green_likelihood' => 'ZERO'],
        ]);

        DB::table('caseworks')->insert([
            ['address_id' => 1, 'council_id' => 1, 'voter_id' => 1, 'ward_id' => 1, 'user_id' => 2, 'name' => 'Problem with the bins', 'is_closed' => 1],
            ['address_id' => 1, 'council_id' => 1, 'voter_id' => 1, 'ward_id' => 1, 'user_id' => 2, 'name' => 'Anti social youths', 'is_closed' => null],
        ]);

        DB::table('caseworks_comments')->insert([
            ['casework_id' => 1, 'user_id' => 2, 'comment' => 'This is now fixed'],
        ]);

        DB::table('link_elections_voters')->insert([
            ['election_id' => 1, 'voter_id' => 1],
            ['election_id' => 2, 'voter_id' => 1],
            ['election_id' => 2, 'voter_id' => 3],
        ]);

        /***************************
         *  SURVEYS
         **************************/

        DB::table('surveys')->insert([
            ['council_id' => 1, 'user_id' => 1, 'name' => 'Test Survey'],
        ]);

        DB::table('surveys_issues')->insert([
            ['survey_id' => 1, 'name' => 'Creating jobs'],
            ['survey_id' => 1, 'name' => 'Improving care'],
            ['survey_id' => 1, 'name' => 'Tackling local crime'],
            ['survey_id' => 1, 'name' => 'Reducing litter'],
            ['survey_id' => 1, 'name' => 'Improving waste'],
            ['survey_id' => 1, 'name' => 'Protecting parks'],
            ['survey_id' => 1, 'name' => 'More affordable housing'],
            ['survey_id' => 1, 'name' => 'Safer streets'],
        ]);

        DB::table('surveys_questions')->insert([
            ['survey_id' => 1, 'name' => 'How do you rate the council?'],
            ['survey_id' => 1, 'name' => 'Do you think council tax is value for money?'],
            ['survey_id' => 1, 'name' => 'Can you name your local councillors?'],
        ]);

        DB::table('surveys_questions_options')->insert([
            ['question_id' => 1, 'name' => 'Good'],
            ['question_id' => 1, 'name' => 'Average'],
            ['question_id' => 1, 'name' => 'Poor'],
            ['question_id' => 2, 'name' => 'Yes'],
            ['question_id' => 2, 'name' => 'No'],
            ['question_id' => 3, 'name' => 'Yes'],
            ['question_id' => 3, 'name' => 'No'],
        ]);

        DB::table('surveys_responses')->insert([
            [
                'address_id' => 1,
                'survey_id' => 1,
                'ward_id' => 1,
                'voter_id' => 1,
                'date_of_response' => '2021-10-01',
                'signup_name' => 'Mister LovesTheGreens',
                'signup_email' => 'lovegreens@green.com',
                'signup_phone' => '09876 123 456',
                'volunteer_deliver' => 1,
            ],
        ]);
        DB::table('surveys_responses')->insert([
            [
                'address_id' => 2,
                'survey_id' => 1,
                'ward_id' => 1,
                'date_of_response' => '2021-10-01',
            ],
        ]);

        DB::table('surveys_responses_answers')->insert([
            ['question_id' => 1, 'response_id' => 1, 'option_id' => 1, 'additional_text' => 'I LOVE THE COUNCIL'],
            ['question_id' => 2, 'response_id' => 1, 'option_id' => 4, 'additional_text' => ''],
            ['question_id' => 3, 'response_id' => 1, 'option_id' => 6, 'additional_text' => ''],
            ['question_id' => 1, 'response_id' => 2, 'option_id' => 2, 'additional_text' => ''],
            ['question_id' => 2, 'response_id' => 2, 'option_id' => 4, 'additional_text' => ''],
        ]);

        DB::table('surveys_responses_issues')->insert([
            ['issue_id' => 1, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 2, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 3, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 4, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 5, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 6, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 7, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 8, 'response_id' => 1, 'answer' => 1],
            ['issue_id' => 1, 'response_id' => 2, 'answer' => 2],
            ['issue_id' => 2, 'response_id' => 2, 'answer' => 1],
            ['issue_id' => 3, 'response_id' => 2, 'answer' => 2],
            ['issue_id' => 4, 'response_id' => 2, 'answer' => 0],
            ['issue_id' => 5, 'response_id' => 2, 'answer' => 1],
            ['issue_id' => 6, 'response_id' => 2, 'answer' => 2],
            ['issue_id' => 7, 'response_id' => 2, 'answer' => 0],
            // Note that the second response didn't give an answer for the last issue, so this is null
        ]);

        DB::table('surveys_responses_parties')->insert([
            ['response_id' => 1, 'answer' => 2, 'enum_political_party' => 'CON'],
            ['response_id' => 1, 'answer' => 0, 'enum_political_party' => 'LAB'],
            ['response_id' => 1, 'answer' => 0, 'enum_political_party' => 'LD'],
            ['response_id' => 1, 'answer' => 0, 'enum_political_party' => 'GREEN'],
            ['response_id' => 2, 'answer' => 2, 'enum_political_party' => 'GREEN'],
        ]);

        /***************************
         *  ACTIVISTS AND EVENTS
         **************************/
        DB::table('activists')->insert([
            [
                'id' => 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa',
                'council_id' => 1,
                'forename' => 'Zoople',
                'surname' => 'Zooplington',
                'email' => 'zoople@gmail.com',
                'mobile_number' => '07999123456',
                'phone_number' => '01234123456',
                'do_not_phone' => false,
                'address' => '1 Fake Street',
                'city' => 'Fake City',
                'postcode' => 'FA1 2KE',
                'council_ward' => 'Fake Ward',
                'local_authority' => 'Fake Authority',
                'constituency' => 'Fake Constituency',
                'is_member' => true,
                'membership_start_date' => '2021-01-01',
                'membership_end_date' => '2021-12-31',
                'membership_type' => 'Standard',
            ],
            [
                'id' => 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb',
                'council_id' => 2,
                'forename' => 'Floople',
                'surname' => 'Flooplington',
                'email' => 'Floople@gmail.com',
                'mobile_number' => '07999999999',
                'phone_number' => '02034999999',
                'do_not_phone' => false,
                'address' => '2 Fake Street',
                'city' => 'Fake City',
                'postcode' => 'FA1 2KE',
                'council_ward' => 'Fake Ward',
                'local_authority' => 'Fake Authority',
                'constituency' => 'Fake Constituency',
                'is_member' => true,
                'membership_start_date' => '2021-01-01',
                'membership_end_date' => '2021-12-31',
                'membership_type' => 'Standard',
            ],
        ]);

        DB::table('events')->insert([
            ['council_id' => 1, 'user_id' => 1, 'date_of_event' => '2021-01-01', 'name' => 'Derby City Event', 'notes' => 'Event 1 Notes'],
            ['council_id' => 2, 'user_id' => 1, 'date_of_event' => '2021-01-01', 'name' => 'Nottingham City Event', 'notes' => 'Event 2 Notes'],
        ]);

        DB::table('contacts')->insert([
            ['activist_id' => 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'event_id' => 1, 'user_id' => 1, 'date_of_contact' => '2020-01-01', 'enum_contact_type' => 'PHONE', 'managed_to_contact' => false, 'notes' => 'No response'],
            ['activist_id' => 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'event_id' => 1, 'user_id' => 1, 'date_of_contact' => '2020-01-02', 'enum_contact_type' => 'PHONE', 'managed_to_contact' => true, 'notes' => 'Coming to the event'],
        ]);

        DB::table('link_activists_events')->insert([
            ['activist_id' => 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'event_id' => 1, 'is_attending' => 1, 'did_attend' => 1],
        ]);
    }
}
